/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     11.12.2014 11:20:56                          */
/*==============================================================*/


create sequence G_ID_GAS_FUEL_TYPE;

create sequence G_ID_GAS_STATION;

create sequence G_ID_GAS_STATION_BRAND;

create sequence G_ID_GAS_STATION_FUEL_TYPE;

create sequence G_ID_GAS_STATION_NETWORK;

create sequence G_ID_GAS_STATION_SERVICE;

create sequence G_ID_GAS_STATION_SERVICE_TYPE;

create sequence G_ID_GIS_OBJECT_TYPE;

/*==============================================================*/
/* Table: GAS_FUEL_TYPE                                         */
/*==============================================================*/
create table GAS_FUEL_TYPE (
   ID_GAS_FUEL_TYPE     INT4                 not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           VARCHAR              not null,
   LASTDATE             VARCHAR              not null
);

alter table GAS_FUEL_TYPE
   add constraint PK_GAS_FUEL_TYPE primary key (ID_GAS_FUEL_TYPE);

/*==============================================================*/
/* Index: SI_GAS_FUEL_TYPE                                      */
/*==============================================================*/
create unique index SI_GAS_FUEL_TYPE on GAS_FUEL_TYPE (
NAME
);

/*==============================================================*/
/* Table: GAS_SERVICE_TYPE                                      */
/*==============================================================*/
create table GAS_SERVICE_TYPE (
   ID_GAS_SERVICE_TYPE  INT4                 not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table GAS_SERVICE_TYPE
   add constraint PK_GAS_SERVICE_TYPE primary key (ID_GAS_SERVICE_TYPE);

/*==============================================================*/
/* Index: SI_GAS_SERVICE_TYPE                                   */
/*==============================================================*/
create unique index SI_GAS_SERVICE_TYPE on GAS_SERVICE_TYPE (
NAME
);

/*==============================================================*/
/* Table: GAS_STATION                                           */
/*==============================================================*/
create table GAS_STATION (
   ID_GAS_STATION       INT4                 not null,
   ID_GAS_STATION_BRAND INT4                 not null,
   ID_GAS_STATION_NETWORK INT4                 not null,
   NAME                 VARCHAR              not null,
   ADDRESS              VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table GAS_STATION
   add constraint PK_GAS_STATION primary key (ID_GAS_STATION);

/*==============================================================*/
/* Index: FI_GAS_STATION_GAS_STATION_BRAN                       */
/*==============================================================*/
create  index FI_GAS_STATION_GAS_STATION_BRAN on GAS_STATION (
ID_GAS_STATION_BRAND
);

/*==============================================================*/
/* Index: FI_GAS_STATION_GAS_STATION_NETW                       */
/*==============================================================*/
create  index FI_GAS_STATION_GAS_STATION_NETW on GAS_STATION (
ID_GAS_STATION_NETWORK
);

/*==============================================================*/
/* Table: GAS_STATION_BRAND                                     */
/*==============================================================*/
create table GAS_STATION_BRAND (
   ID_GAS_STATION_BRAND INT4                 not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table GAS_STATION_BRAND
   add constraint PK_GAS_STATION_BRAND primary key (ID_GAS_STATION_BRAND);

/*==============================================================*/
/* Index: SI_GAS_STATION_BRAND                                  */
/*==============================================================*/
create unique index SI_GAS_STATION_BRAND on GAS_STATION_BRAND (
NAME
);

/*==============================================================*/
/* Table: GAS_STATION_FUEL_TYPE                                 */
/*==============================================================*/
create table GAS_STATION_FUEL_TYPE (
   ID_GAS_STATION_FUEL_TYPE INT4                 not null,
   ID_GAS_STATION       INT4                 not null,
   ID_GAS_FUEL_TYPE     INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table GAS_STATION_FUEL_TYPE
   add constraint PK_GAS_STATION_FUEL_TYPE primary key (ID_GAS_STATION_FUEL_TYPE);

/*==============================================================*/
/* Index: SI_GAS_STATION_FUEL_TYPE                              */
/*==============================================================*/
create unique index SI_GAS_STATION_FUEL_TYPE on GAS_STATION_FUEL_TYPE (
ID_GAS_STATION_FUEL_TYPE,
ID_GAS_STATION
);

/*==============================================================*/
/* Index: FI_GAS_STATION_FUEL_TYPE_STATIO                       */
/*==============================================================*/
create  index FI_GAS_STATION_FUEL_TYPE_STATIO on GAS_STATION_FUEL_TYPE (
ID_GAS_STATION
);

/*==============================================================*/
/* Index: FI_GAS_STATION_FUEL_TYPE_FUEL_T                       */
/*==============================================================*/
create  index FI_GAS_STATION_FUEL_TYPE_FUEL_T on GAS_STATION_FUEL_TYPE (
ID_GAS_FUEL_TYPE
);

/*==============================================================*/
/* Table: GAS_STATION_NETWORK                                   */
/*==============================================================*/
create table GAS_STATION_NETWORK (
   ID_GAS_STATION_NETWORK INT4                 not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table GAS_STATION_NETWORK
   add constraint PK_GAS_STATION_NETWORK primary key (ID_GAS_STATION_NETWORK);

/*==============================================================*/
/* Index: SI_GAS_STATION_NETWORK                                */
/*==============================================================*/
create unique index SI_GAS_STATION_NETWORK on GAS_STATION_NETWORK (
NAME
);

/*==============================================================*/
/* Table: GAS_STATION_SERVICE_TYPE                              */
/*==============================================================*/
create table GAS_STATION_SERVICE_TYPE (
   ID_GAS_STATION_SERVICE_TYPE INT4                 not null,
   ID_GAS_STATION       INT4                 not null,
   ID_GAS_SERVICE_TYPE  INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table GAS_STATION_SERVICE_TYPE
   add constraint PK_GAS_STATION_SERVICE_TYPE primary key (ID_GAS_STATION_SERVICE_TYPE);

/*==============================================================*/
/* Index: SI_GAS_STATION_SERVICE_TYPE                           */
/*==============================================================*/
create unique index SI_GAS_STATION_SERVICE_TYPE on GAS_STATION_SERVICE_TYPE (
ID_GAS_STATION,
ID_GAS_SERVICE_TYPE
);

/*==============================================================*/
/* Index: FI_GAS_STATION_SERVICE_TYPE_STA                       */
/*==============================================================*/
create  index FI_GAS_STATION_SERVICE_TYPE_STA on GAS_STATION_SERVICE_TYPE (
ID_GAS_STATION
);

/*==============================================================*/
/* Index: FI_GAS_STATION_SERVICE_TYPE_SER                       */
/*==============================================================*/
create  index FI_GAS_STATION_SERVICE_TYPE_SER on GAS_STATION_SERVICE_TYPE (
ID_GAS_SERVICE_TYPE
);

alter table GAS_STATION
   add constraint FK_GAS_STATION_GAS_STATION_BRAND foreign key (ID_GAS_STATION_BRAND)
      references GAS_STATION_BRAND (ID_GAS_STATION_BRAND)
      on delete restrict on update restrict;

alter table GAS_STATION
   add constraint FK_GAS_STATION_GAS_STATION_NETWORK foreign key (ID_GAS_STATION_NETWORK)
      references GAS_STATION_NETWORK (ID_GAS_STATION_NETWORK)
      on delete restrict on update restrict;

alter table GAS_STATION_FUEL_TYPE
   add constraint FK_GAS_STATION_FUEL_TYPE_GAS_STATION foreign key (ID_GAS_STATION)
      references GAS_STATION (ID_GAS_STATION)
      on delete restrict on update restrict;

alter table GAS_STATION_FUEL_TYPE
   add constraint FK_GAS_STATION_FUEL_TYPE_GAS_FUEL_TYPE foreign key (ID_GAS_FUEL_TYPE)
      references GAS_FUEL_TYPE (ID_GAS_FUEL_TYPE)
      on delete restrict on update restrict;

alter table GAS_STATION_SERVICE_TYPE
   add constraint FK_GAS_STATION_SERVICE_TYPE_GAS_STATION foreign key (ID_GAS_STATION)
      references GAS_STATION (ID_GAS_STATION)
      on delete restrict on update restrict;

alter table GAS_STATION_SERVICE_TYPE
   add constraint FK_GAS_STATION_SERVICE_TYPE_GAS_SERVICE_TYPE foreign key (ID_GAS_SERVICE_TYPE)
      references GAS_SERVICE_TYPE (ID_GAS_SERVICE_TYPE)
      on delete restrict on update restrict;


-- trigger function for INSERT
create or replace function F_TIB_GAS_FUEL_TYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_GAS_FUEL_TYPE is null) then
    new.id_GAS_FUEL_TYPE := nextval('G_ID_GAS_FUEL_TYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_GAS_FUEL_TYPE 
before insert
on GAS_FUEL_TYPE  
for each row
execute procedure F_TIB_GAS_FUEL_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_GAS_FUEL_TYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_GAS_FUEL_TYPE 
before update
on GAS_FUEL_TYPE 
for each row
execute procedure F_TUB_GAS_FUEL_TYPE();


-- trigger function for INSERT
create or replace function F_TIB_GAS_SERVICE_TYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_GAS_SERVICE_TYPE is null) then
    new.id_GAS_SERVICE_TYPE := nextval('G_ID_GAS_SERVICE_TYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_GAS_SERVICE_TYPE 
before insert
on GAS_SERVICE_TYPE  
for each row
execute procedure F_TIB_GAS_SERVICE_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_GAS_SERVICE_TYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_GAS_SERVICE_TYPE 
before update
on GAS_SERVICE_TYPE 
for each row
execute procedure F_TUB_GAS_SERVICE_TYPE();


-- trigger function for INSERT
create or replace function F_TIB_GAS_STATION() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_GAS_STATION is null) then
    new.id_GAS_STATION := nextval('G_ID_GAS_STATION');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_GAS_STATION 
before insert
on GAS_STATION  
for each row
execute procedure F_TIB_GAS_STATION();


-- trigger function for UPDATE
create or replace function F_TUB_GAS_STATION() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_GAS_STATION 
before update
on GAS_STATION 
for each row
execute procedure F_TUB_GAS_STATION();


-- trigger function for INSERT
create or replace function F_TIB_GAS_STATION_BRAND() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_GAS_STATION_BRAND is null) then
    new.id_GAS_STATION_BRAND := nextval('G_ID_GAS_STATION_BRAND');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_GAS_STATION_BRAND 
before insert
on GAS_STATION_BRAND  
for each row
execute procedure F_TIB_GAS_STATION_BRAND();


-- trigger function for UPDATE
create or replace function F_TUB_GAS_STATION_BRAND() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_GAS_STATION_BRAND 
before update
on GAS_STATION_BRAND 
for each row
execute procedure F_TUB_GAS_STATION_BRAND();


-- trigger function for INSERT
create or replace function F_TIB_GAS_STATION_FUEL_TYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_GAS_STATION_FUEL_TYPE is null) then
    new.id_GAS_STATION_FUEL_TYPE := nextval('G_ID_GAS_STATION_FUEL_TYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_GAS_STATION_FUEL_TYPE 
before insert
on GAS_STATION_FUEL_TYPE  
for each row
execute procedure F_TIB_GAS_STATION_FUEL_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_GAS_STATION_FUEL_TYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_GAS_STATION_FUEL_TYPE 
before update
on GAS_STATION_FUEL_TYPE 
for each row
execute procedure F_TUB_GAS_STATION_FUEL_TYPE();


-- trigger function for INSERT
create or replace function F_TIB_GAS_STATION_NETWORK() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_GAS_STATION_NETWORK is null) then
    new.id_GAS_STATION_NETWORK := nextval('G_ID_GAS_STATION_NETWORK');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_GAS_STATION_NETWORK 
before insert
on GAS_STATION_NETWORK  
for each row
execute procedure F_TIB_GAS_STATION_NETWORK();


-- trigger function for UPDATE
create or replace function F_TUB_GAS_STATION_NETWORK() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_GAS_STATION_NETWORK 
before update
on GAS_STATION_NETWORK 
for each row
execute procedure F_TUB_GAS_STATION_NETWORK();


-- trigger function for INSERT
create or replace function F_TIB_GAS_STATION_SERVICE_TYPE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_GAS_STATION_SERVICE_TYPE is null) then
    new.id_GAS_STATION_SERVICE_TYPE := nextval('G_ID_GAS_STATION_SERVICE_TYPE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_GAS_STATION_SERVICE_TYPE 
before insert
on GAS_STATION_SERVICE_TYPE  
for each row
execute procedure F_TIB_GAS_STATION_SERVICE_TYPE();


-- trigger function for UPDATE
create or replace function F_TUB_GAS_STATION_SERVICE_TYPE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_GAS_STATION_SERVICE_TYPE 
before update
on GAS_STATION_SERVICE_TYPE 
for each row
execute procedure F_TUB_GAS_STATION_SERVICE_TYPE();

