-- GISDB - база геоданных

-- mkdir -p /pgstore03/gisdb/gisdb_tbs_default
-- chown postgres:postgres /pgstore03/gisdb -R
-- chmod 700 /pgstore03/gisdb -R

--
-- /opt/PostgreSQL/9.3/bin/psql -p 5434 -U postgres

-- владелец БД
CREATE ROLE gisdbadmin WITH PASSWORD 'pass';
alter role gisdbadmin login;
UPDATE pg_authid SET rolcatupdate=false WHERE rolname='gisdbadmin';
COMMENT ON ROLE gisdbadmin IS 'Владелец GISDB';

-- табличное пространство
create tablespace gisdb_tbs_default owner gisdbadmin location '/pgstore03/gisdb/gisdb_tbs_default';


-- база
CREATE DATABASE gisdb WITH ENCODING='UTF8' OWNER=gisdbadmin CONNECTION LIMIT=-1 TABLESPACE=gisdb_tbs_default LC_COLLATE = 'ru_RU.utf8' LC_CTYPE = 'ru_RU.utf8' template template_postgis;


-- объекты

-- процедуры и функции из ./procedures


-- NN in PostGIS 2.0
select s.*
from gas_station s 
order by s.location <-> st_geomfromtext('POINT(38.961441 45.059997)')
limit 10



-- OLD OLD OLD 


-- Connect as MONITORING and create tables:
create table geocity (
  id_geocity int not null primary key,
  status varchar,
  name varchar not null,
  district varchar,
  region varchar,
  square double precision,
  state varchar,
  population varchar,
  createdate timestamp default current_timestamp not null
);
select AddGeometryColumn('geocity', 'shape', 4326, 'POLYGON', 2);


create table geoobject (
  id_geoobject int not null primary key,
  name varchar not null,
  createdate timestamp default current_timestamp not null
);
select AddGeometryColumn('geoobject', 'shape', 4326, 'GEOMETRY', 2);


create table georeference (
  id_georeference int not null primary key,
  id_geocity int not null,
  id_geoobject int not null,
  createdate timestamp default current_timestamp not null
);
--
alter table georeference add constraint fk_georeference_geocity foreign key (id_geocity) references geocity (id_geocity);
alter table georeference add constraint fk_georeference_geoobject foreign key (id_geoobject) references geoobject (id_geoobject);
create index fi_georeference_geocity on georeference (id_geocity);
create index fi_georeference_geoobject on georeference (id_geoobject);


create table geoitem (
  id_geoitem int not null primary key,
  grmn_type varchar not null,
  street varchar not null,
  numb int not null,
  liter varchar,
  createdate timestamp default current_timestamp not null
);
select AddGeometryColumn('geoitem', 'shape', 4326, 'GEOMETRY', 2);


create table geoitemref (
  id_geoitemref int not null primary key,
  id_geocity int not null,
  id_geoitem int not null,
  createdate timestamp default current_timestamp not null
);
alter table geoitemref add constraint fk_geoitemref_geocity foreign key (id_geocity) references geocity (id_geocity);
alter table geoitemref add constraint fk_geoitemref_geoitem foreign key (id_geoitem) references geoitem (id_geoitem);
create index fi_geoitemref_geocity on geoitemref (id_geocity);
create index fi_geoitemref_geoitem on geoitemref (id_geoitem);



-- после загрузки данных
vacuum analyze geocity
vacuum analyze geoobject
vacuum analyze georeference
vacuum analyze geoitem
vacuum analyze geoitemref



--
create or replace function get_nearest_data (
  ageom_wkt in varchar, -- геометрия в WKT
  acity_max_distance in int, -- максимальное расстояние поиска города (метров)
  astreet_max_distance in int, -- максимальное расстояние поиска улицы (метров)
  aregion out varchar, 
  acity out varchar, 
  astreet out varchar, 
  adistance out int
)
returns record
as
$body$
declare
  fnn_gid int;
  fnn_dist numeric(16,5);
  fname varchar;
  fstatename varchar;
begin
  -----------------------------------------------------
  -- Возвращает ближайший к геометрии геообъект
  -----------------------------------------------------
  -- ищем ближайший город
  SELECT (pgis_fn_nn(
    ST_GeomFromText(ageom_wkt, 4326), -- геометрия
    acity_max_distance, 
    1, -- numnn (кол-во результатов)
    1000, -- maxslices
    'geocity', -- таблица с геометрией
    'true', -- некое условие которое можно применить к набору во время поиска
    'id_geocity', -- уникальное поле в таблице с геометрией
    'shape')).*  -- поле с геометрией
  into fnn_gid, fnn_dist;
  select name, state into acity, aregion from geocity where id_geocity = fnn_gid;
  -- если попали в город - ищем улицу
  if (fnn_dist = 0) then
    SELECT (pgis_fn_nn(
      ST_GeomFromText(ageom_wkt, 4326), -- геометрия
      astreet_max_distance, 
      1, 
      1000,
      'geoobject',
      'true', 
      'id_geoobject',
      'shape')).* 
    into fnn_gid, fnn_dist;
    -- проверяем, соблюдается ли погрешность дистанции (в данном случае берем максимальную погрешность, составляющую 200 метров) до улицы
    if (fnn_dist <= astreet_max_distance) then
      select name into astreet from geoobject where id_geoobject = fnn_gid;
      adistance := round(fnn_dist);
    else
      astreet   := null;
      adistance := 0;
    end if;
  else
    adistance := round(fnn_dist);
  end if;
  return;
end
$body$
language 'plpgsql' immutable;
-- select aregion, acity, astreet, adistance from get_nearest_data('POINT(38.566667 45.2)', 100000, 200)


------------------
SELECT (pgis_fn_nn(
    ST_GeomFromText('POINT(38.566667 45.2)', 4326), -- geom
    1000000, -- distguess (как я понимаю это максимальное расстояние поиска, т.е. не дальше чем; метров)
    1, -- numnn (кол-во результатов)
    1000, -- maxslices
    'geocity', -- таблица с геометрией
    'true', -- некое условие которое можно применить к набору во время поиска
    'id_geocity', -- уникальное поле в таблице с геометрией
    'shape')).*  -- поле с геометрией



