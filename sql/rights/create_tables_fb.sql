/*==============================================================*/
/* DBMS name:      InterBase 6.x                                */
/* Created on:     28.11.2014 12:11:14                          */
/*==============================================================*/


create generator G_ID_STEM_RIGHT_APIKEY;

create generator G_ID_STEM_RIGHT_RIGHT;

create generator G_ID_STEM_RIGHT_ROLE;

create generator G_ID_STEM_RIGHT_ROLERIGHT;

create generator G_ID_STEM_RIGHT_USER;

create generator G_ID_STEM_RIGHT_USERROLE;

/*==============================================================*/
/* Table: STEM_RIGHT_APIKEY                                     */
/*==============================================================*/
create table STEM_RIGHT_APIKEY (
ID_STEM_RIGHT_APIKEY SMALLINT                       not null,
STRDATA              VARCHAR(256)                   not null,
DESCRIPTION          VARCHAR(2048),
CREATEDATE           DATE,
LASTDATE             DATE,
constraint PK_STEM_RIGHT_APIKEY primary key (ID_STEM_RIGHT_APIKEY)
);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_APIKEY                                  */
/*==============================================================*/
create unique asc index SI_STEM_RIGHT_APIKEY on STEM_RIGHT_APIKEY (
STRDATA
);

/*==============================================================*/
/* Table: STEM_RIGHT_RIGHT                                      */
/*==============================================================*/
create table STEM_RIGHT_RIGHT (
ID_STEM_RIGHT_RIGHT  SMALLINT                       not null,
METHOD               VARCHAR(256)                   not null,
DESCRIPTION          VARCHAR(2048),
CREATEDATE           DATE,
LASTDATE             DATE,
constraint PK_STEM_RIGHT_RIGHT primary key (ID_STEM_RIGHT_RIGHT)
);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_RIGHT                                   */
/*==============================================================*/
create unique asc index SI_STEM_RIGHT_RIGHT on STEM_RIGHT_RIGHT (
METHOD
);

/*==============================================================*/
/* Table: STEM_RIGHT_ROLE                                       */
/*==============================================================*/
create table STEM_RIGHT_ROLE (
ID_STEM_RIGHT_ROLE   SMALLINT                       not null,
NAME                 VARCHAR(256)                   not null,
CREATEDATE           DATE,
LASTDATE             DATE,
DESCRIPTION          VARCHAR(2048),
constraint PK_STEM_RIGHT_ROLE primary key (ID_STEM_RIGHT_ROLE)
);

insert into stem_right_role (id_stem_right_role, name, description) values (1, 'login', '���� � �������');
insert into stem_right_role (id_stem_right_role, name, description) values (2, 'admin', '�������������');
set GENERATOR g_id_stem_right_role to 100;

/*==============================================================*/
/* Index: SI_STEM_RIGHT_ROLE                                    */
/*==============================================================*/
create unique asc index SI_STEM_RIGHT_ROLE on STEM_RIGHT_ROLE (
NAME
);

/*==============================================================*/
/* Table: STEM_RIGHT_ROLERIGHT                                  */
/*==============================================================*/
create table STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_ROLERIGHT SMALLINT                       not null,
ID_STEM_RIGHT_ROLE   INTEGER                        not null,
ID_STEM_RIGHT_RIGHT  INTEGER                        not null,
CREATEDATE           DATE,
LASTDATE             DATE,
constraint PK_STEM_RIGHT_ROLERIGHT primary key (ID_STEM_RIGHT_ROLERIGHT)
);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_ROLERIGHT_ROLE                          */
/*==============================================================*/
create asc index FI_STEM_RIGHT_ROLERIGHT_ROLE on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_ROLE
);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_ROLERIGHT_RIGHT                         */
/*==============================================================*/
create asc index FI_STEM_RIGHT_ROLERIGHT_RIGHT on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_RIGHT
);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_ROLERIGHT                               */
/*==============================================================*/
create unique asc index SI_STEM_RIGHT_ROLERIGHT on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_ROLE,
ID_STEM_RIGHT_RIGHT
);

/*==============================================================*/
/* Table: STEM_RIGHT_USER                                       */
/*==============================================================*/
create table STEM_RIGHT_USER (
ID_STEM_RIGHT_USER   SMALLINT                       not null,
GUUID                VARCHAR(256)                   not null,
CREATEDATE           DATE,
LASTDATE             DATE,
constraint PK_STEM_RIGHT_USER primary key (ID_STEM_RIGHT_USER)
);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_USER                                    */
/*==============================================================*/
create unique asc index SI_STEM_RIGHT_USER on STEM_RIGHT_USER (
GUUID
);

/*==============================================================*/
/* Table: STEM_RIGHT_USERROLE                                   */
/*==============================================================*/
create table STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_USERROLE SMALLINT                       not null,
ID_STEM_RIGHT_ROLE   INTEGER                        not null,
ID_STEM_RIGHT_USER   INTEGER                        not null,
CREATEDATE           DATE,
LASTDATE             DATE,
constraint PK_STEM_RIGHT_USERROLE primary key (ID_STEM_RIGHT_USERROLE)
);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_USERROLE_USER                           */
/*==============================================================*/
create asc index FI_STEM_RIGHT_USERROLE_USER on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_USER
);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_USERROLE_ROLE                           */
/*==============================================================*/
create asc index FI_STEM_RIGHT_USERROLE_ROLE on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_ROLE
);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_USERROLE                                */
/*==============================================================*/
create unique asc index SI_STEM_RIGHT_USERROLE on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_ROLE,
ID_STEM_RIGHT_USER
);

alter table STEM_RIGHT_ROLERIGHT
   add constraint FK_STEM_RIG_REFERENCE_STEM_RI0 foreign key (ID_STEM_RIGHT_RIGHT)
      references STEM_RIGHT_RIGHT (ID_STEM_RIGHT_RIGHT);

alter table STEM_RIGHT_ROLERIGHT
   add constraint FK_STEM_RIG_REFERENCE_STEM_RI1 foreign key (ID_STEM_RIGHT_ROLE)
      references STEM_RIGHT_ROLE (ID_STEM_RIGHT_ROLE);

alter table STEM_RIGHT_USERROLE
   add constraint FK_STEM_RIG_REFERENCE_STEM_RI2 foreign key (ID_STEM_RIGHT_ROLE)
      references STEM_RIGHT_ROLE (ID_STEM_RIGHT_ROLE);

alter table STEM_RIGHT_USERROLE
   add constraint FK_STEM_RIG_REFERENCE_STEM_RI3 foreign key (ID_STEM_RIGHT_USER)
      references STEM_RIGHT_USER (ID_STEM_RIGHT_USER);


create trigger TIB_STEM_RIGHT_APIKEY for STEM_RIGHT_APIKEY
before insert as
begin
if (new.id_STEM_RIGHT_APIKEY is null)
    then new.id_STEM_RIGHT_APIKEY = gen_id(G_ID_STEM_RIGHT_APIKEY, 1);
new.lastdate = current_timestamp;
new.createdate = current_timestamp;    
end;


create trigger TUB_STEM_RIGHT_APIKEY for STEM_RIGHT_APIKEY
  before update
as
begin
  new.lastdate = current_timestamp;
end;


create trigger TIB_STEM_RIGHT_RIGHT for STEM_RIGHT_RIGHT
before insert as
begin
if (new.id_STEM_RIGHT_RIGHT is null)
    then new.id_STEM_RIGHT_RIGHT = gen_id(G_ID_STEM_RIGHT_RIGHT, 1);
new.lastdate = current_timestamp;
new.createdate = current_timestamp;    
end;


create trigger TUB_STEM_RIGHT_RIGHT for STEM_RIGHT_RIGHT
  before update
as
begin
  new.lastdate = current_timestamp;
end;


create trigger TIB_STEM_RIGHT_ROLE for STEM_RIGHT_ROLE
before insert as
begin
if (new.id_STEM_RIGHT_ROLE is null)
    then new.id_STEM_RIGHT_ROLE = gen_id(G_ID_STEM_RIGHT_ROLE, 1);
new.lastdate = current_timestamp;
new.createdate = current_timestamp;    
end;


create trigger TUB_STEM_RIGHT_ROLE for STEM_RIGHT_ROLE
  before update
as
begin
  new.lastdate = current_timestamp;
end;


create trigger TIB_STEM_RIGHT_ROLERIGHT for STEM_RIGHT_ROLERIGHT
before insert as
begin
if (new.id_STEM_RIGHT_ROLERIGHT is null)
    then new.id_STEM_RIGHT_ROLERIGHT = gen_id(G_ID_STEM_RIGHT_ROLERIGHT, 1);
new.lastdate = current_timestamp;
new.createdate = current_timestamp;    
end;


create trigger TUB_STEM_RIGHT_ROLERIGHT for STEM_RIGHT_ROLERIGHT
  before update
as
begin
  new.lastdate = current_timestamp;
end;


create trigger TIB_STEM_RIGHT_USER for STEM_RIGHT_USER
before insert as
begin
if (new.id_STEM_RIGHT_USER is null)
    then new.id_STEM_RIGHT_USER = gen_id(G_ID_STEM_RIGHT_USER, 1);
new.lastdate = current_timestamp;
new.createdate = current_timestamp;    
end;


create trigger TUB_STEM_RIGHT_USER for STEM_RIGHT_USER
  before update
as
begin
  new.lastdate = current_timestamp;
end;


create trigger TIB_STEM_RIGHT_USERROLE for STEM_RIGHT_USERROLE
before insert as
begin
if (new.id_STEM_RIGHT_USERROLE is null)
    then new.id_STEM_RIGHT_USERROLE = gen_id(G_ID_STEM_RIGHT_USERROLE, 1);
new.lastdate = current_timestamp;
new.createdate = current_timestamp;    
end;


create trigger TUB_STEM_RIGHT_SYSUSERROLE for STEM_RIGHT_USERROLE
  before update
as
begin
  new.lastdate = current_timestamp;
end;

