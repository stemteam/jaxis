/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     27.11.2014 12:47:33                          */
/*==============================================================*/


/*==============================================================*/
/* Table: STEM_RIGHT_APIKEY                                     */
/*==============================================================*/
create table STEM_RIGHT_APIKEY (
   ID_STEM_RIGHT_APIKEY int                  identity,
   STRDATA              VARCHAR(256)         not null,
   DESCRIPTION          VARCHAR(4096)        null,
   CREATEDATE           datetime             null default CURRENT_TIMESTAMP,
   LASTDATE             datetime             null default CURRENT_TIMESTAMP
)
go

alter table STEM_RIGHT_APIKEY
   add constraint PK_STEM_RIGHT_APIKEY primary key nonclustered (ID_STEM_RIGHT_APIKEY)
go

/*==============================================================*/
/* Index: SI_STEM_RIGHT_APIKEY                                  */
/*==============================================================*/
create unique index SI_STEM_RIGHT_APIKEY on STEM_RIGHT_APIKEY (
STRDATA ASC
)
go

/*==============================================================*/
/* Table: STEM_RIGHT_RIGHT                                      */
/*==============================================================*/
create table STEM_RIGHT_RIGHT (
   ID_STEM_RIGHT_RIGHT  int                  identity,
   METHOD               VARCHAR(256)         not null,
   DESCRIPTION          VARCHAR(4096)        not null,
   CREATEDATE           datetime             not null default CURRENT_TIMESTAMP,
   LASTDATE             datetime             not null default CURRENT_TIMESTAMP
)
go

alter table STEM_RIGHT_RIGHT
   add constraint PK_STEM_RIGHT_RIGHT primary key nonclustered (ID_STEM_RIGHT_RIGHT)
go

/*==============================================================*/
/* Index: SI_STEM_RIGHT_RIGHT                                   */
/*==============================================================*/
create unique index SI_STEM_RIGHT_RIGHT on STEM_RIGHT_RIGHT (
METHOD ASC
)
go

/*==============================================================*/
/* Table: STEM_RIGHT_ROLE                                       */
/*==============================================================*/
create table STEM_RIGHT_ROLE (
   ID_STEM_RIGHT_ROLE   int                  identity,
   NAME                 VARCHAR(256)         not null,
   CREATEDATE           datetime             null default CURRENT_TIMESTAMP,
   LASTDATE             datetime             null default CURRENT_TIMESTAMP,
   DESCRIPTION          VARCHAR(4096)        null
)
go

SET IDENTITY_INSERT stem_right_role ON;
insert into stem_right_role (id_stem_right_role, name, description) values (1, 'login', '���� � �������');
insert into stem_right_role (id_stem_right_role, name, description) values (2, 'admin', '�������������');
SET IDENTITY_INSERT stem_right_role OFF;
DBCC CHECKIDENT ("stem_right_role", RESEED, 100);

alter table STEM_RIGHT_ROLE
   add constraint PK_STEM_RIGHT_ROLE primary key nonclustered (ID_STEM_RIGHT_ROLE)
go

/*==============================================================*/
/* Index: SI_STEM_RIGHT_ROLE                                    */
/*==============================================================*/
create unique index SI_STEM_RIGHT_ROLE on STEM_RIGHT_ROLE (
NAME ASC
)
go

/*==============================================================*/
/* Table: STEM_RIGHT_ROLERIGHT                                  */
/*==============================================================*/
create table STEM_RIGHT_ROLERIGHT (
   ID_STEM_RIGHT_ROLERIGHT int                  identity,
   ID_STEM_RIGHT_ROLE   int                  not null,
   ID_STEM_RIGHT_RIGHT  int                  not null,
   CREATEDATE           datetime             not null default CURRENT_TIMESTAMP,
   LASTDATE             datetime             not null default CURRENT_TIMESTAMP
)
go

alter table STEM_RIGHT_ROLERIGHT
   add constraint PK_STEM_RIGHT_ROLERIGHT primary key nonclustered (ID_STEM_RIGHT_ROLERIGHT)
go

/*==============================================================*/
/* Index: FI_STEM_RIGHT_ROLERIGHT_ROLE                          */
/*==============================================================*/
create index FI_STEM_RIGHT_ROLERIGHT_ROLE on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_ROLE ASC
)
go

/*==============================================================*/
/* Index: FI_STEM_RIGHT_ROLERIGHT_RIGHT                         */
/*==============================================================*/
create index FI_STEM_RIGHT_ROLERIGHT_RIGHT on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_RIGHT ASC
)
go

/*==============================================================*/
/* Index: SI_STEM_RIGHT_ROLERIGHT                               */
/*==============================================================*/
create unique index SI_STEM_RIGHT_ROLERIGHT on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_ROLE ASC,
ID_STEM_RIGHT_RIGHT ASC
)
go

/*==============================================================*/
/* Table: STEM_RIGHT_USER                                       */
/*==============================================================*/
create table STEM_RIGHT_USER (
   ID_STEM_RIGHT_USER   int                  identity,
   GUUID                VARCHAR(36)          not null,
   CREATEDATE           datetime             null default CURRENT_TIMESTAMP,
   LASTDATE             datetime             null default CURRENT_TIMESTAMP
)
go

alter table STEM_RIGHT_USER
   add constraint PK_STEM_RIGHT_USER primary key nonclustered (ID_STEM_RIGHT_USER)
go

/*==============================================================*/
/* Index: SI_STEM_RIGHT_USER                                    */
/*==============================================================*/
create unique index SI_STEM_RIGHT_USER on STEM_RIGHT_USER (
GUUID ASC
)
go

/*==============================================================*/
/* Table: STEM_RIGHT_USERROLE                                   */
/*==============================================================*/
create table STEM_RIGHT_USERROLE (
   ID_STEM_RIGHT_USERROLE int                  identity,
   ID_STEM_RIGHT_ROLE   int                  not null,
   ID_STEM_RIGHT_USER   int                  not null,
   CREATEDATE           datetime             not null default CURRENT_TIMESTAMP,
   LASTDATE             datetime             not null default CURRENT_TIMESTAMP
)
go

alter table STEM_RIGHT_USERROLE
   add constraint PK_STEM_RIGHT_USERROLE primary key nonclustered (ID_STEM_RIGHT_USERROLE)
go

/*==============================================================*/
/* Index: FI_STEM_RIGHT_USERROLE_USER                           */
/*==============================================================*/
create index FI_STEM_RIGHT_USERROLE_USER on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_USER ASC
)
go

/*==============================================================*/
/* Index: FI_STEM_RIGHT_USERROLE_ROLE                           */
/*==============================================================*/
create index FI_STEM_RIGHT_USERROLE_ROLE on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_ROLE ASC
)
go

/*==============================================================*/
/* Index: SI_STEM_RIGHT_USERROLE                                */
/*==============================================================*/
create unique index SI_STEM_RIGHT_USERROLE on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_ROLE ASC,
ID_STEM_RIGHT_USER ASC
)
go

alter table STEM_RIGHT_ROLERIGHT
   add constraint FK_STEM_RIG_REFERENCE_STEM_0 foreign key (ID_STEM_RIGHT_RIGHT)
      references STEM_RIGHT_RIGHT (ID_STEM_RIGHT_RIGHT)
go

alter table STEM_RIGHT_ROLERIGHT
   add constraint FK_STEM_RIG_REFERENCE_STEM_1 foreign key (ID_STEM_RIGHT_ROLE)
      references STEM_RIGHT_ROLE (ID_STEM_RIGHT_ROLE)
go

alter table STEM_RIGHT_USERROLE
   add constraint FK_STEM_RIG_REFERENCE_STEM_2 foreign key (ID_STEM_RIGHT_ROLE)
      references STEM_RIGHT_ROLE (ID_STEM_RIGHT_ROLE)
go

alter table STEM_RIGHT_USERROLE
   add constraint FK_STEM_RIG_REFERENCE_STEM_3 foreign key (ID_STEM_RIGHT_USER)
      references STEM_RIGHT_USER (ID_STEM_RIGHT_USER)
go

