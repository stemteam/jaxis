/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     22.12.2014 12:50:35                          */
/*==============================================================*/


create sequence G_ID_STEM_RIGHT_APIKEY;

create sequence G_ID_STEM_RIGHT_RIGHT;

create sequence G_ID_STEM_RIGHT_ROLE;

create sequence G_ID_STEM_RIGHT_ROLERIGHT;

create sequence G_ID_STEM_RIGHT_USER;

create sequence G_ID_STEM_RIGHT_USERROLE;

/*==============================================================*/
/* Table: STEM_RIGHT_APIKEY                                     */
/*==============================================================*/
create table STEM_RIGHT_APIKEY (
   ID_STEM_RIGHT_APIKEY INT4                 not null,
   STRDATA              VARCHAR              not null,
   DESCRIPTION          VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_APIKEY
   add constraint PK_STEM_RIGHT_APIKEY primary key (ID_STEM_RIGHT_APIKEY);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_APIKEY                                  */
/*==============================================================*/
create unique index SI_STEM_RIGHT_APIKEY on STEM_RIGHT_APIKEY (
STRDATA
);

/*==============================================================*/
/* Table: STEM_RIGHT_RIGHT                                      */
/*==============================================================*/
create table STEM_RIGHT_RIGHT (
   ID_STEM_RIGHT_RIGHT  INT4                 not null,
   METHOD               VARCHAR              not null,
   DESCRIPTION          VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_RIGHT
   add constraint PK_STEM_RIGHT_RIGHT primary key (ID_STEM_RIGHT_RIGHT);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_RIGHT                                   */
/*==============================================================*/
create unique index SI_STEM_RIGHT_RIGHT on STEM_RIGHT_RIGHT (
METHOD
);

/*==============================================================*/
/* Table: STEM_RIGHT_ROLE                                       */
/*==============================================================*/
create table STEM_RIGHT_ROLE (
   ID_STEM_RIGHT_ROLE   INT4                 not null,
   NAME                 VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP,
   DESCRIPTION          VARCHAR              not null
);

insert into stem_right_role (id_stem_right_role, name, description) values (1, 'login', '���� � �������');
insert into stem_right_role (id_stem_right_role, name, description) values (2, 'admin', '�������������');
alter sequence g_id_stem_right_role restart with 100;

alter table STEM_RIGHT_ROLE
   add constraint PK_STEM_RIGHT_ROLE primary key (ID_STEM_RIGHT_ROLE);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_ROLE                                    */
/*==============================================================*/
create unique index SI_STEM_RIGHT_ROLE on STEM_RIGHT_ROLE (
NAME
);

/*==============================================================*/
/* Table: STEM_RIGHT_ROLERIGHT                                  */
/*==============================================================*/
create table STEM_RIGHT_ROLERIGHT (
   ID_STEM_RIGHT_ROLERIGHT INT4                 not null,
   ID_STEM_RIGHT_ROLE   INT4                 not null,
   ID_STEM_RIGHT_RIGHT  INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_ROLERIGHT
   add constraint PK_STEM_RIGHT_ROLERIGHT primary key (ID_STEM_RIGHT_ROLERIGHT);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_ROLERIGHT_ROLE                          */
/*==============================================================*/
create  index FI_STEM_RIGHT_ROLERIGHT_ROLE on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_ROLE
);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_ROLERIGHT_RIGHT                         */
/*==============================================================*/
create  index FI_STEM_RIGHT_ROLERIGHT_RIGHT on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_RIGHT
);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_ROLERIGHT                               */
/*==============================================================*/
create unique index SI_STEM_RIGHT_ROLERIGHT on STEM_RIGHT_ROLERIGHT (
ID_STEM_RIGHT_ROLE,
ID_STEM_RIGHT_RIGHT
);

/*==============================================================*/
/* Table: STEM_RIGHT_USER                                       */
/*==============================================================*/
create table STEM_RIGHT_USER (
   ID_STEM_RIGHT_USER   INT4                 not null,
   GUUID                VARCHAR              not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_USER
   add constraint PK_STEM_RIGHT_USER primary key (ID_STEM_RIGHT_USER);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_USER                                    */
/*==============================================================*/
create unique index SI_STEM_RIGHT_USER on STEM_RIGHT_USER (
GUUID
);

/*==============================================================*/
/* Table: STEM_RIGHT_USERROLE                                   */
/*==============================================================*/
create table STEM_RIGHT_USERROLE (
   ID_STEM_RIGHT_USERROLE INT4                 not null,
   ID_STEM_RIGHT_ROLE   INT4                 not null,
   ID_STEM_RIGHT_USER   INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

alter table STEM_RIGHT_USERROLE
   add constraint PK_STEM_RIGHT_USERROLE primary key (ID_STEM_RIGHT_USERROLE);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_USERROLE_USER                           */
/*==============================================================*/
create  index FI_STEM_RIGHT_USERROLE_USER on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_USER
);

/*==============================================================*/
/* Index: FI_STEM_RIGHT_USERROLE_ROLE                           */
/*==============================================================*/
create  index FI_STEM_RIGHT_USERROLE_ROLE on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_ROLE
);

/*==============================================================*/
/* Index: SI_STEM_RIGHT_USERROLE                                */
/*==============================================================*/
create unique index SI_STEM_RIGHT_USERROLE on STEM_RIGHT_USERROLE (
ID_STEM_RIGHT_ROLE,
ID_STEM_RIGHT_USER
);

alter table STEM_RIGHT_ROLERIGHT
   add constraint FK_STEM_RIGHT_ROLERIGHT_STEM_RIGHT_RIGHT foreign key (ID_STEM_RIGHT_RIGHT)
      references STEM_RIGHT_RIGHT (ID_STEM_RIGHT_RIGHT)
      on delete restrict on update restrict;

alter table STEM_RIGHT_ROLERIGHT
   add constraint FK_STEM_RIGHT_ROLERIGHT_STEM_RIGHT_ROLE foreign key (ID_STEM_RIGHT_ROLE)
      references STEM_RIGHT_ROLE (ID_STEM_RIGHT_ROLE)
      on delete restrict on update restrict;

alter table STEM_RIGHT_USERROLE
   add constraint FK_STEM_RIGHT_USERROLE_STEM_RIGHT_ROLE foreign key (ID_STEM_RIGHT_ROLE)
      references STEM_RIGHT_ROLE (ID_STEM_RIGHT_ROLE)
      on delete restrict on update restrict;

alter table STEM_RIGHT_USERROLE
   add constraint FK_STEM_RIGHT_USERROLE_STEM_RIGHT_USER foreign key (ID_STEM_RIGHT_USER)
      references STEM_RIGHT_USER (ID_STEM_RIGHT_USER)
      on delete restrict on update restrict;


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_APIKEY() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_APIKEY is null) then
    new.id_STEM_RIGHT_APIKEY := nextval('G_ID_STEM_RIGHT_APIKEY');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_APIKEY 
before insert
on STEM_RIGHT_APIKEY  
for each row
execute procedure F_TIB_STEM_RIGHT_APIKEY();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_APIKEY() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_APIKEY 
before update
on STEM_RIGHT_APIKEY 
for each row
execute procedure F_TUB_STEM_RIGHT_APIKEY();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_RIGHT() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_RIGHT is null) then
    new.id_STEM_RIGHT_RIGHT := nextval('G_ID_STEM_RIGHT_RIGHT');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_RIGHT 
before insert
on STEM_RIGHT_RIGHT  
for each row
execute procedure F_TIB_STEM_RIGHT_RIGHT();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_RIGHT() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_RIGHT 
before update
on STEM_RIGHT_RIGHT 
for each row
execute procedure F_TUB_STEM_RIGHT_RIGHT();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_ROLE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_ROLE is null) then
    new.id_STEM_RIGHT_ROLE := nextval('G_ID_STEM_RIGHT_ROLE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_ROLE 
before insert
on STEM_RIGHT_ROLE  
for each row
execute procedure F_TIB_STEM_RIGHT_ROLE();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_ROLE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_ROLE 
before update
on STEM_RIGHT_ROLE 
for each row
execute procedure F_TUB_STEM_RIGHT_ROLE();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_ROLERIGHT() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_ROLERIGHT is null) then
    new.id_STEM_RIGHT_ROLERIGHT := nextval('G_ID_STEM_RIGHT_ROLERIGHT');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_ROLERIGHT 
before insert
on STEM_RIGHT_ROLERIGHT  
for each row
execute procedure F_TIB_STEM_RIGHT_ROLERIGHT();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_ROLERIGHT() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_ROLERIGHT 
before update
on STEM_RIGHT_ROLERIGHT 
for each row
execute procedure F_TUB_STEM_RIGHT_ROLERIGHT();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_USER() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_USER is null) then
    new.id_STEM_RIGHT_USER := nextval('G_ID_STEM_RIGHT_USER');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_USER 
before insert
on STEM_RIGHT_USER  
for each row
execute procedure F_TIB_STEM_RIGHT_USER();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_USER() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_USER 
before update
on STEM_RIGHT_USER 
for each row
execute procedure F_TUB_STEM_RIGHT_USER();


-- trigger function for INSERT
create or replace function F_TIB_STEM_RIGHT_USERROLE() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_RIGHT_USERROLE is null) then
    new.id_STEM_RIGHT_USERROLE := nextval('G_ID_STEM_RIGHT_USERROLE');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_RIGHT_USERROLE 
before insert
on STEM_RIGHT_USERROLE  
for each row
execute procedure F_TIB_STEM_RIGHT_USERROLE();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_RIGHT_SYSUSERROLE() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_RIGHT_SYSUSERROLE 
before update
on STEM_RIGHT_USERROLE 
for each row
execute procedure F_TUB_STEM_RIGHT_SYSUSERROLE();

