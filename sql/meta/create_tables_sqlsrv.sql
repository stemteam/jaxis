/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     14.01.2015 13:35:01                          */
/*==============================================================*/


/*==============================================================*/
/* Table: STEM_META_FIELD                                       */
/*==============================================================*/
create table STEM_META_FIELD (
   ID_STEM_META_FIELD   int                  identity,
   ID_STEM_META_METHOD  int                  not null,
   NAME                 VARCHAR(256)         not null,
   CAPTION              VARCHAR(4096)        not null,
   DESCRIPTION          VARCHAR(4096)        null,
   IS_ID                smallint             not null default 0
      constraint CKC_IS_ID_STEM_MET check (IS_ID in (0,1)),
   DATATYPE             VARCHAR(256)         not null,
   IS_READONLY          smallint             not null default 0
      constraint CKC_IS_READONLY_STEM_MET check (IS_READONLY in (0,1)),
   IS_HIDDEN            smallint             not null default 0
      constraint CKC_IS_HIDDEN_STEM_MET check (IS_HIDDEN in (0,1)),
   WIDTH                int                  null,
   DEFAULTVALUE         VARCHAR(256)         null,
   VALUELIST            varchar(4096)        null,
   PRESENTATION         VARCHAR              null,
   IMPORTANCE           smallint             null,
   RELATIONMETHOD       VARCHAR(256)         null,
   RELATIONFIELD        VARCHAR(256)         null,
   RELATION_MASTER_FIELD VARCHAR(256)         null,
   REFERENCE_METHOD     VARCHAR(256)         null,
   REFERENCE_FIELD      VARCHAR(256)         null,
   REFERENCE_MASTER_FIELD VARCHAR(256)         null,
   RELATION_FILTER      VARCHAR(256)         null,
   GROUPNAME            VARCHAR(256)         null,
   LASTDATE             datetime             not null default CURRENT_TIMESTAMP,
   CREATEDATE           datetime             not null default CURRENT_TIMESTAMP,
   IS_TREEPARENT        smallint             not null default 0
      constraint CKC_IS_TREEPARENT_STEM_MET check (IS_TREEPARENT in (0,1)),
   IS_TREEFIELD         smallint             not null default 0
      constraint CKC_IS_TREEFIELD_STEM_MET check (IS_TREEFIELD in (0,1)),
   IS_IMMUTABLE         smallint             not null default 0
      constraint CKC_IS_IMMUTABLE_STEM_ME2 check (IS_IMMUTABLE in (0,1)),
   IS_MANDATORY         smallint             not null default 0
      constraint CKC_IS_MANDATORY_STEM_MET check (IS_MANDATORY in (0,1)),
   IS_CAPTION           smallint             null
)
go

alter table STEM_META_FIELD
   add constraint PK_STEM_META_FIELD primary key nonclustered (ID_STEM_META_FIELD)
go

/*==============================================================*/
/* Index: FI_STEM_META_FIELD_METHOD                             */
/*==============================================================*/
create index FI_STEM_META_FIELD_METHOD on STEM_META_FIELD (
ID_STEM_META_METHOD ASC
)
go

/*==============================================================*/
/* Index: SI_STEM_META_FIELD                                    */
/*==============================================================*/
create unique index SI_STEM_META_FIELD on STEM_META_FIELD (
ID_STEM_META_METHOD ASC,
NAME ASC
)
go

/*==============================================================*/
/* Table: STEM_META_METHOD                                      */
/*==============================================================*/
create table STEM_META_METHOD (
   ID_STEM_META_METHOD  int                  identity,
   NAME                 VARCHAR(256)         null,
   DESCRIPTION          VARCHAR(4096)        null,
   ID_STEM_META_METHODGROUP int                  not null,
   CREATEDATE           datetime             null default CURRENT_TIMESTAMP,
   LASTDATE             datetime             null default CURRENT_TIMESTAMP,
   IS_IMMUTABLE         smallint             null default 0
      constraint CKC_IS_IMMUTABLE_STEM_ME1 check (IS_IMMUTABLE is null or (IS_IMMUTABLE in (0,1))),
   HTTP_INSERT_TEMPLATE VARCHAR(4096)        null,
   HTTP_UPDATE_TEMPLATE VARCHAR(4096)        null
)
go

alter table STEM_META_METHOD
   add constraint PK_STEM_META_METHOD primary key nonclustered (ID_STEM_META_METHOD)
go

/*==============================================================*/
/* Index: SI_STEM_META_METHOD                                   */
/*==============================================================*/
create unique index SI_STEM_META_METHOD on STEM_META_METHOD (
NAME ASC
)
go

/*==============================================================*/
/* Index: FI_STEM_META_METHOD_METHODGROUP                       */
/*==============================================================*/
create index FI_STEM_META_METHOD_METHODGROUP on STEM_META_METHOD (
ID_STEM_META_METHODGROUP ASC
)
go

/*==============================================================*/
/* Table: STEM_META_METHODGROUP                                 */
/*==============================================================*/
create table STEM_META_METHODGROUP (
   ID_STEM_META_METHODGROUP int                  identity,
   ID_PARENT            int                  null default 0,
   NAME                 VARCHAR(256)         null,
   CLEVEL               int                  null default 1,
   CCOUNT               int                  null default 0,
   IS_SYSTEM            smallint             null default 0
      constraint CKC_IS_SYSTEM_STEM_MET check (IS_SYSTEM is null or (IS_SYSTEM in (0,1))),
   CREATEDATE           datetime             null default CURRENT_TIMESTAMP,
   LASTDATE             datetime             null default CURRENT_TIMESTAMP,
   IS_IMMUTABLE         smallint             null default 0
      constraint CKC_IS_IMMUTABLE_STEM_ME0 check (IS_IMMUTABLE is null or (IS_IMMUTABLE in (0,1)))
)
go

insert into stem_meta_methodgroup (id_parent, name, is_immutable) values (0, '��� ������', 1);
insert into stem_meta_methodgroup (id_parent, name, is_immutable) values (1, '����������', 1);

declare @methodGroupId int;
select @methodGroupId = max(id_stem_meta_methodgroup) from stem_meta_methodgroup where name = '����������';

insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodGroupListGet', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodGroupGet', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodGroupAdd', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodGroupEdit', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodGroupDelete', 1);
--
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodListGet', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodGet', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodAdd', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodEdit', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/MethodDelete', 1);
--
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/FieldListGet', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/FieldGet', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/FieldAdd', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/FieldEdit', 1);
insert into stem_meta_method (id_stem_meta_methodgroup, name, is_immutable) values (@methodGroupId, 'meta/FieldDelete', 1);

declare @methodId int;


-- MethodGroupListGet
select @methodId = max(id_stem_meta_method) from stem_meta_method where name = 'meta/MethodGroupListGet';
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_treefield, is_immutable) values (@methodId, 'MethodGroupName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_treeparent, is_immutable) values (@methodId, 'MethodGroupParentId', '������������� ������������ ������', '������������� ������������ ������', 0, 'Int32', 0, null, 1, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable) values (@methodId, 'Methods', '������', '������ ������', 0, 'DataSet', 0, null, 1, 'meta/MethodListGet', 'MethodGroupId', 1);

-- MethodGroupGet
select @methodId = max(id_stem_meta_method) from stem_meta_method where name = 'meta/MethodGroupGet';
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodGroupName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodGroupParentId', '������������� ������������ ������', '������������� ������������ ������', 0, 'Int32', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable) values (@methodId, 'Methods', '������', '������ ������', 0, 'DataSet', 0, null, 1, 'meta/MethodListGet', 'MethodGroupId', 1);

-- FieldListGet
select @methodId = max(id_stem_meta_method) from stem_meta_method where name = 'meta/FieldListGet';
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'FieldId', '�������������', '�������������', 1, 'Int32', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Name', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Caption', '���������', '�������� ����(��� �������/����� � �����)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Description', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsId', '�������������', '���� �������� ��������������� ��� ������� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'DataType', '��� ������', '��� ����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsReadonly', '������ ��� ������', '������ ��� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsHidden', '�� ������������', '�������� �������������� �����', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Width', '������', '������ ����(��� ���������� ����������� � ����)', 0, 'Int32', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'DefaultValue', '�������� �� ���������', '�������� �� ���������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'ValueList', '������ ��������', '��������, ������� ����� ��������� ����', 0, 'String[]', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Presentation', '���������� �������������', '���������� ������������� Custom, Text, Decimal, Boolean, DateTime, Time � ������� ��������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Importance', '��������', '��������. 0 - ������, 1 ����� ������ � �.�. ����������� �������, ����������� �������� ��������� ���� � ��������� �� �������� "���" ��� ���������� ������ � �����������, �� ������� �������.', 0, 'Int32', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'RelationMethod', '�����', '����� ��� ������ �� �����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'RelationField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'GroupName', '������ �����', '������������ ��� ����������� ����� � ���� ����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsTreeParent', '������ ��� ������', '�������, ��� ���� �������� ����� ������� �� ��������, �� �������� �������� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsTreeField', '���� ������', '����, �� �������� �������� ������ (� ����� ����� ��� ������ ���)', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsMandatory', '������������', '�������� �� ������������ � ����������', 0, 'Int16', 0, null, 1, 1);

-- FieldGet
select @methodId = max(id_stem_meta_method) from stem_meta_method where name = 'meta/FieldGet';
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'FieldId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Name', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Caption', '���������', '�������� ����(��� �������/����� � �����)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Description', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsId', '�������������', '���� �������� ��������������� ��� ������� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'DataType', '��� ������', '��� ����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsReadonly', '������ ��� ������', '������ ��� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsHidden', '�� ������������', '�������� �������������� �����', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Width', '������', '������ ����(��� ���������� ����������� � ����)', 0, 'Int32', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'DefaultValue', '�������� �� ���������', '�������� �� ���������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'ValueList', '������ ��������', '��������, ������� ����� ��������� ����', 0, 'String[]', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Presentation', '���������� �������������', '���������� ������������� Custom, Text, Decimal, Boolean, DateTime, Time � ������� ��������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'Importance', '��������', '��������. 0 - ������, 1 ����� ������ � �.�. ����������� �������, ����������� �������� ��������� ���� � ��������� �� �������� "���" ��� ���������� ������ � �����������, �� ������� �������.', 0, 'Int32', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'RelationMethod', '�����', '����� ��� ������ �� �����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'RelationField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'GroupName', '������ �����', '������������ ��� ����������� ����� � ���� ����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsTreeParent', '������ ��� ������', '�������, ��� ���� �������� ����� ������� �� ��������, �� �������� �������� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsTreeField', '���� ������', '����, �� �������� �������� ������ (� ����� ����� ��� ������ ���)', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'IsMandatory', '������������', '�������� �� ������������ � ����������', 0, 'Int16', 0, null, 1, 1);

-- MethodListGet
select @methodId = max(id_stem_meta_method) from stem_meta_method where name = 'meta/MethodListGet';
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodDescription', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable) values (@methodId, 'Fields', '����', '���� ������', 0, 'DataSet', 0, null, 1, 'meta/FieldListGet', 'MethodName', 1);

-- MethodGet
select @methodId = max(id_stem_meta_method) from stem_meta_method where name = 'meta/MethodGet';
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodDescription', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable) values (@methodId, 'Fields', '����', '���� ������', 0, 'DataSet', 0, null, 1, 'meta/FieldListGet', 'MethodName', 1);


-- MethodGroupAdd
select @methodId = max(id_stem_meta_method) from stem_meta_method where name = 'meta/MethodGroupAdd';
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodGroupName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodGroupParentId', '������������� ������������ ������', '������������� ������������ ������', 0, 'Int32', 0, null, 1, 1);

-- MethodGroupEdit
select @methodId = max(id_stem_meta_method) from stem_meta_method where name = 'meta/MethodGroupEdit';
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_hidden, is_immutable) values (@methodId, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 0, null, 1, 1, 1);
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (@methodId, 'MethodGroupName', '��������', '�������� ������', 0, 'String', 0, 80, 1, 1);

-- MethodGroupDelete
select @methodId = max(id_stem_meta_method) from stem_meta_method where name = 'meta/MethodGroupDelete';
insert into stem_meta_field (id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_hidden, is_immutable) values (@methodId, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 0, null, 1, 1, 1);


alter table STEM_META_METHODGROUP
   add constraint PK_STEM_META_METHODGROUP primary key nonclustered (ID_STEM_META_METHODGROUP)
go

/*==============================================================*/
/* Index: SI_STEM_META_METHODGROUP                              */
/*==============================================================*/
create unique index SI_STEM_META_METHODGROUP on STEM_META_METHODGROUP (
ID_PARENT ASC,
NAME ASC
)
go

alter table STEM_META_FIELD
   add constraint FK_STEM_MET_REFERENCE_STEM_ME1 foreign key (ID_STEM_META_METHOD)
      references STEM_META_METHOD (ID_STEM_META_METHOD)
go

alter table STEM_META_METHOD
   add constraint FK_STEM_MET_REFERENCE_STEM_ME0 foreign key (ID_STEM_META_METHODGROUP)
      references STEM_META_METHODGROUP (ID_STEM_META_METHODGROUP)
go

