/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     29.12.2014 10:39:00                          */
/*==============================================================*/


create sequence G_ID_STEM_META_FIELD;

create sequence G_ID_STEM_META_FORM;

create sequence G_ID_STEM_META_FRAME;

create sequence G_ID_STEM_META_METHOD;

create sequence G_ID_STEM_META_METHODGROUP;

create sequence Sequence_6;

/*==============================================================*/
/* Table: STEM_META_FIELD                                       */
/*==============================================================*/
create table STEM_META_FIELD (
   ID_STEM_META_FIELD   INT4                 not null,
   ID_STEM_META_METHOD  INT4                 not null,
   NAME                 VARCHAR              not null,
   CAPTION              VARCHAR              not null,
   DESCRIPTION          VARCHAR              null,
   IS_ID                INT2                 not null default 0
      constraint CKC_IS_ID_STEM_MET check (IS_ID in (0,1)),
   DATATYPE             VARCHAR              not null,
   IS_READONLY          INT2                 not null default 0
      constraint CKC_IS_READONLY_STEM_MET check (IS_READONLY in (0,1)),
   IS_HIDDEN            INT2                 not null default 0
      constraint CKC_IS_HIDDEN_STEM_MET check (IS_HIDDEN in (0,1)),
   WIDTH                INT4                 null,
   DEFAULTVALUE         VARCHAR              null,
   VALUELIST            VARCHAR[]            null,
   PRESENTATION         VARCHAR              null,
   IMPORTANCE           INT2                 null,
   RELATIONMETHOD       VARCHAR              null,
   RELATIONFIELD        VARCHAR              null,
   RELATION_MASTER_FIELD VARCHAR              null,
   REFERENCE_METHOD     VARCHAR              null,
   REFERENCE_FIELD      VARCHAR              null,
   REFERENCE_MASTER_FIELD VARCHAR              null,
   RELATIONFILTER       VARCHAR              null,
   GROUPNAME            VARCHAR              null,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   IS_TREEPARENT        INT2                 not null default 0
      constraint CKC_IS_TREEPARENT_STEM_MET check (IS_TREEPARENT in (0,1)),
   IS_TREEFIELD         INT2                 not null default 0
      constraint CKC_IS_TREEFIELD_STEM_MET check (IS_TREEFIELD in (0,1)),
   IS_IMMUTABLE         INT2                 not null default 0
      constraint CKC_IS_IMMUTABLE_STEM_MET check (IS_IMMUTABLE in (0,1)),
   IS_MANDATORY         INT2                 not null default 0
      constraint CKC_IS_MANDATORY_STEM_MET check (IS_MANDATORY in (0,1)),
   IS_CAPTION           INT2                 not null default 0
      constraint CKC_IS_CAPTION_STEM_MET check (IS_CAPTION in (0,1))
);

-- MethodGroupListGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),1, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_treefield, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),1, 'MethodGroupName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_treeparent, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),1, 'MethodGroupParentId', '������������� ������������ ������', '������������� ������������ ������', 0, 'Int32', 0, null, 1, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),1, 'Methods', '������', '������ ������', 0, 'DataSet', 0, null, 1, 'meta/MethodListGet', 'MethodGroupId', 1,0);

-- MethodGroupGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),2, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),2, 'MethodGroupName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),2, 'MethodGroupParentId', '������������� ������������ ������', '������������� ������������ ������', 0, 'Int32', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),2, 'Methods', '������', '������ ������', 0, 'DataSet', 0, null, 1, 'meta/MethodListGet', 'MethodGroupId', 1,0);

-- FieldListGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'FieldId', '�������������', '�������������', 1, 'Int32', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'Name', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'Caption', '���������', '�������� ����(��� �������/����� � �����)', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'Description', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'IsId', '�������������', '���� �������� ��������������� ��� ������� ������', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'DataType', '��� ������', '��� ����', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'IsReadonly', '������ ��� ������', '������ ��� ������', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'IsHidden', '�� ������������', '�������� �������������� �����', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'Width', '������', '������ ����(��� ���������� ����������� � ����)', 0, 'Int32', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'DefaultValue', '�������� �� ���������', '�������� �� ���������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'ValueList', '������ ��������', '��������, ������� ����� ��������� ����', 0, 'String[]', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'Presentation', '���������� �������������', '���������� ������������� Custom, Text, Decimal, Boolean, DateTime, Time � ������� ��������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'Importance', '��������', '��������. 0 - ������, 1 ����� ������ � �.�. ����������� �������, ����������� �������� ��������� ���� � ��������� �� �������� "���" ��� ���������� ������ � �����������, �� ������� �������.', 0, 'Int32', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'RelationMethod', '�����', '����� ��� ������ �� �����', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'RelationField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'RelationMasterField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'ReferenceMethod', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'ReferenceField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'ReferenceMasterField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'GroupName', '������ �����', '������������ ��� ����������� ����� � ���� ����', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'IsTreeParent', '������ ��� ������', '�������, ��� ���� �������� ����� ������� �� ��������, �� �������� �������� ������', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'IsTreeField', '���� ������', '����, �� �������� �������� ������ (� ����� ����� ��� ������ ���)', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),11, 'IsMandatory', '������������', '�������� �� ������������ � ����������', 0, 'Int16', 0, null, 1, 1,0);

-- FieldGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'FieldId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'Name', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'Caption', '���������', '�������� ����(��� �������/����� � �����)', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'Description', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'IsId', '�������������', '���� �������� ��������������� ��� ������� ������', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'DataType', '��� ������', '��� ����', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'IsReadonly', '������ ��� ������', '������ ��� ������', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'IsHidden', '�� ������������', '�������� �������������� �����', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'Width', '������', '������ ����(��� ���������� ����������� � ����)', 0, 'Int32', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'DefaultValue', '�������� �� ���������', '�������� �� ���������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'ValueList', '������ ��������', '��������, ������� ����� ��������� ����', 0, 'String[]', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'Presentation', '���������� �������������', '���������� ������������� Custom, Text, Decimal, Boolean, DateTime, Time � ������� ��������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'Importance', '��������', '��������. 0 - ������, 1 ����� ������ � �.�. ����������� �������, ����������� �������� ��������� ���� � ��������� �� �������� "���" ��� ���������� ������ � �����������, �� ������� �������.', 0, 'Int32', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'RelationMethod', '�����', '����� ��� ������ �� �����', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'RelationField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'RelationMasterField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'ReferenceMethod', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'ReferenceField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'ReferenceMasterField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'GroupName', '������ �����', '������������ ��� ����������� ����� � ���� ����', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'IsTreeParent', '������ ��� ������', '�������, ��� ���� �������� ����� ������� �� ��������, �� �������� �������� ������', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'IsTreeField', '���� ������', '����, �� �������� �������� ������ (� ����� ����� ��� ������ ���)', 0, 'Int16', 0, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),12, 'IsMandatory', '������������', '�������� �� ������������ � ����������', 0, 'Int16', 0, null, 1, 1,0);

-- MethodListGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),6, 'MethodId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),6, 'MethodName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),6, 'MethodDescription', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),6, 'TemplateInsert', 'HTML ������ �������', 'HTML ������ ��� ���������� ������', 0, 'String', 0, 1024*1024, 0, 0,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),6, 'TemplateUpdate', 'HTML ������ ��������������', 'HTML ������ ��� �������������� ������', 0, 'String', 0, 1024*1024, 0, 0,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),6, 'Fields', '����', '���� ������', 0, 'DataSet', 0, null, 1, 'meta/FieldListGet', 'MethodName', 1,0);

-- MethodGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),7, 'MethodId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),7, 'MethodName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),7, 'MethodDescription', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),7, 'TemplateInsert', 'HTML ������ �������', 'HTML ������ ��� ���������� ������', 0, 'String', 0, 1024*1024, 0, 0,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),7, 'TemplateUpdate', 'HTML ������ ��������������', 'HTML ������ ��� �������������� ������', 0, 'String', 0, 1024*1024, 0, 0,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),7, 'Fields', '����', '���� ������', 0, 'DataSet', 0, null, 1, 'meta/FieldListGet', 'MethodName', 1,0);


-- MethodGroupAdd
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),3, 'MethodGroupName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),3, 'MethodGroupParentId', '������������� ������������ ������', '������������� ������������ ������', 0, 'Int32', 0, null, 1, 1,0);

-- MethodGroupEdit
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_hidden, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),4, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 0, null, 1, 1, 1,0);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),4, 'MethodGroupName', '��������', '�������� ������', 0, 'String', 0, 80, 1, 1,0);

-- MethodGroupDelete
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_hidden, is_immutable,is_caption) values (nextval('g_id_stem_meta_field'),5, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 0, null, 1, 1, 1,0);


alter table STEM_META_FIELD
   add constraint PK_STEM_META_FIELD primary key (ID_STEM_META_FIELD);

/*==============================================================*/
/* Index: FI_STEM_META_FIELD_METHOD                             */
/*==============================================================*/
create  index FI_STEM_META_FIELD_METHOD on STEM_META_FIELD (
ID_STEM_META_METHOD
);

/*==============================================================*/
/* Index: SI_STEM_META_FIELD                                    */
/*==============================================================*/
create unique index SI_STEM_META_FIELD on STEM_META_FIELD (
ID_STEM_META_METHOD,
NAME
);

/*==============================================================*/
/* Table: STEM_META_FORM                                        */
/*==============================================================*/
create table STEM_META_FORM (
   ID_STEM_META_FORM    INT4                 not null,
   ID_PARENT            INT4                 not null default 0,
   FIELD_NAME           VARCHAR              null,
   PARENT_FIELD_NAME    VARCHAR              null,
   NAME                 INT8                 null,
   ID_STEM_META_FRAME   INT8                 null,
   CLEVEL               INT4                 not null default 1,
   CCOUNT               INT4                 not null default 0,
   IS_SYSTEM            INT2                 not null default 0
      constraint CKC_IS_SYSTEM_STEM_MET check (IS_SYSTEM in (0,1)),
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP,
   IS_IMMUTABLE         INT2                 not null default 0
      constraint CKC_IS_IMMUTABLE_STEM_MET check (IS_IMMUTABLE in (0,1))
);

insert into stem_meta_methodgroup (id_stem_meta_methodgroup, id_parent, name, is_immutable) values (1, 0, '��� ������', 1);
insert into stem_meta_methodgroup (id_stem_meta_methodgroup, id_parent, name, is_immutable) values (2, 1, '����������', 1);
alter sequence g_id_stem_meta_methodgroup restart with 100;

alter table STEM_META_FORM
   add constraint PK_STEM_META_FORM primary key (ID_STEM_META_FORM);

/*==============================================================*/
/* Index: SI_STEM_META_FORM                                     */
/*==============================================================*/
create unique index SI_STEM_META_FORM on STEM_META_FORM (
ID_PARENT,
PARENT_FIELD_NAME
);

/*==============================================================*/
/* Table: STEM_META_FRAME                                       */
/*==============================================================*/
create table STEM_META_FRAME (
   ID_STEM_META_FRAME   INT8                 not null,
   GET_METHOD           VARCHAR              null,
   LIST_GET_METHOD      VARCHAR              null,
   EDIT_METHOD          VARCHAR              null,
   ADD_METHOD           VARCHAR              null,
   LASTDATE             TIMESTAMP            not null,
   CREATEDATE           TIMESTAMP            not null
);

alter table STEM_META_FRAME
   add constraint PK_STEM_META_FRAME primary key (ID_STEM_META_FRAME);

/*==============================================================*/
/* Table: STEM_META_METHOD                                      */
/*==============================================================*/
create table STEM_META_METHOD (
   ID_STEM_META_METHOD  INT4                 not null,
   NAME                 VARCHAR              not null,
   DESCRIPTION          VARCHAR              null,
   ID_STEM_META_METHODGROUP INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP,
   IS_IMMUTABLE         INT2                 not null default 0
      constraint CKC_IS_IMMUTABLE_STEM_MET check (IS_IMMUTABLE in (0,1)),
   HTTP_INSERT_TEMPLATE VARCHAR              null,
   HTTP_UPDATE_TEMPLATE VARCHAR              null
);

insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (1, 2, 'meta/MethodGroupListGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (2, 2, 'meta/MethodGroupGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (3, 2, 'meta/MethodGroupAdd', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (4, 2, 'meta/MethodGroupEdit', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (5, 2, 'meta/MethodGroupDelete', 1);
--
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (6, 2, 'meta/MethodListGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (7, 2, 'meta/MethodGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (8, 2, 'meta/MethodAdd', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (9, 2, 'meta/MethodEdit', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (10, 2, 'meta/MethodDelete', 1);
--
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (11, 2, 'meta/FieldListGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (12, 2, 'meta/FieldGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (13, 2, 'meta/FieldAdd', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (14, 2, 'meta/FieldEdit', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (15, 2, 'meta/FieldDelete', 1);
alter sequence g_id_stem_meta_method restart with 100;

alter table STEM_META_METHOD
   add constraint PK_STEM_META_METHOD primary key (ID_STEM_META_METHOD);

/*==============================================================*/
/* Index: SI_STEM_META_METHOD                                   */
/*==============================================================*/
create unique index SI_STEM_META_METHOD on STEM_META_METHOD (
NAME
);

/*==============================================================*/
/* Index: FI_STEM_META_METHOD_METHODGROUP                       */
/*==============================================================*/
create  index FI_STEM_META_METHOD_METHODGROUP on STEM_META_METHOD (
ID_STEM_META_METHODGROUP
);

/*==============================================================*/
/* Table: STEM_META_METHODGROUP                                 */
/*==============================================================*/
create table STEM_META_METHODGROUP (
   ID_STEM_META_METHODGROUP INT4                 not null,
   ID_PARENT            INT4                 not null default 0,
   NAME                 VARCHAR              not null,
   CLEVEL               INT4                 not null default 1,
   CCOUNT               INT4                 not null default 0,
   IS_SYSTEM            INT2                 not null default 0
      constraint CKC_IS_SYSTEM_STEM_MET check (IS_SYSTEM in (0,1)),
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP,
   IS_IMMUTABLE         INT2                 not null default 0
      constraint CKC_IS_IMMUTABLE_STEM_MET check (IS_IMMUTABLE in (0,1))
);

insert into stem_meta_methodgroup (id_stem_meta_methodgroup, id_parent, name, is_immutable) values (1, 0, '��� ������', 1);
insert into stem_meta_methodgroup (id_stem_meta_methodgroup, id_parent, name, is_immutable) values (2, 1, '����������', 1);
alter sequence g_id_stem_meta_methodgroup restart with 100;

alter table STEM_META_METHODGROUP
   add constraint PK_STEM_META_METHODGROUP primary key (ID_STEM_META_METHODGROUP);

/*==============================================================*/
/* Index: SI_STEM_META_METHODGROUP                              */
/*==============================================================*/
create unique index SI_STEM_META_METHODGROUP on STEM_META_METHODGROUP (
ID_PARENT,
NAME
);

alter table STEM_META_FIELD
   add constraint FK_STEM_META_FIELD_STEM_META_METHOD foreign key (ID_STEM_META_METHOD)
      references STEM_META_METHOD (ID_STEM_META_METHOD)
      on delete restrict on update restrict;

alter table STEM_META_FORM
   add constraint FK_STEM_META_FORM_STEM_META_FRAME foreign key (ID_STEM_META_FRAME)
      references STEM_META_FRAME (ID_STEM_META_FRAME)
      on delete set null;

alter table STEM_META_METHOD
   add constraint FK_STEM_META_METHOD_STEM_META_METHODGROUP foreign key (ID_STEM_META_METHODGROUP)
      references STEM_META_METHODGROUP (ID_STEM_META_METHODGROUP)
      on delete restrict on update restrict;


-- trigger function for INSERT
create or replace function F_TIB_STEM_META_FIELD() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_META_FIELD is null) then
    new.id_STEM_META_FIELD := nextval('G_ID_STEM_META_FIELD');
  end if;
  -- immutable field init
  if (new.is_immutable is null) then
    new.is_immutable := 0;
  end if;
  -- audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_META_FIELD 
before insert
on STEM_META_FIELD  
for each row
execute procedure F_TIB_STEM_META_FIELD();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_META_FIELD() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- check immutable
  if (old.is_immutable = 1) then
    raise exception '��������� ��������� ��������� ������: ID_STEM_META_FIELD = %', old.ID_STEM_META_FIELD;
  end if;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_META_FIELD 
before update
on STEM_META_FIELD 
for each row
execute procedure F_TUB_STEM_META_FIELD();


-- trigger function for UPDATE
create or replace function F_TDA_STEM_META_FIELD() 
returns trigger as
$body$
declare
begin
  -- check immutable
  if (old.is_immutable = 1) then
    raise exception '��������� �������� ��������� ������: ID_STEM_META_FIELD = %', old.ID_STEM_META_FIELD;
  end if;
  -- return
  return old;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TDA_STEM_META_FIELD 
after delete
on STEM_META_FIELD 
for each row
execute procedure F_TDA_STEM_META_FIELD();


-- trigger function INSERT
create or replace function F_TIB_STEM_META_FORM() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_META_FORM is null) then
    new.id_STEM_META_FORM := nextval('G_ID_STEM_META_FORM');
  end if;
  -- immutable field init
  if (new.is_immutable is null) then
    new.is_immutable := 0;
  end if;
  -- audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- init values
  new.ccount := 0;
  new.is_system := 0;
  new.clevel := 1;
  -- default id_parent init (root node)
  if (new.id_parent is null) then 
    new.id_parent := 0;
  end if;
  -- deny self reference
  if (new.id_parent = new.id_STEM_META_FORM) then
    raise exception 'self reference deny for tree tables';
  end if;
  -- if not root node
  if (new.id_parent != 0) then
    -- calc child level
    select clevel + 1
    into new.clevel
    from STEM_META_FORM
    where id_STEM_META_FORM = new.id_parent;
    -- check parent exists
    if (found = false) then
      raise exception 'parent key with id=% not found', new.id_parent;
    end if;
    -- update parent counter for child
    update STEM_META_FORM
    set ccount = ccount + 1,
        is_system = 1 -- system update flag on
    where id_STEM_META_FORM = new.id_parent;
  end if;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger INSERT
create trigger TIB_STEM_META_FORM
before insert
on STEM_META_FORM 
for each row
execute procedure F_TIB_STEM_META_FORM();


-- check what id_child is child of id_parent, if true return 1 else 0
create or replace function F_CKCH_STEM_META_FORM(aid_parent int, aid_child int)
returns int as 
$body$
declare
  fid int;
begin
  -- search in child nodes (recurse)
  for fid in 
    select id_STEM_META_FORM
    from STEM_META_FORM
    where id_parent = aid_parent
  loop
    if (fid = aid_child) then
      return 1;
    end if;
    -- recurse
    select F_CKCH_STEM_META_FORM(fid, aid_child) into fid;
    if (fid = 1) then
      return 1;
    end if;
  end loop;
  return 0;
end
$body$
language 'plpgsql'; 
-- trigger function for UPDATE
create or replace function F_TUB_STEM_META_FORM() 
returns trigger as
$body$
declare
  fis_child int;
begin
  -- disable id changing
  new.id_STEM_META_FORM := old.id_STEM_META_FORM;
  -- if not system change
  if (new.is_system = 0) then
    -- disable system parameters change
    new.clevel := old.clevel;
    new.ccount := old.ccount;    
    if (new.id_parent is null) then
      new.id_parent := old.id_parent;
    end if;
    -- deny self reference
    if (new.id_parent = new.id_STEM_META_FORM) then
      raise exception 'self reference deny for tree tables';
    end if;    
    -- process id_parent change
    if (new.id_parent != old.id_parent) then
      if (new.id_parent != 0) then
	-- check recursive reference
	select F_CKCH_STEM_META_FORM(new.id_STEM_META_FORM, new.id_parent) into fis_child;
	if (fis_child = 1) then
      raise exception 'recursive references deny for tree table';
	end if;
        -- calc our child level
        select clevel + 1
        into new.clevel
        from STEM_META_FORM
        where id_STEM_META_FORM = new.id_parent;
        -- check parent exists
        if (found = false) then
          raise exception 'parent node with id=% not found', new.id_parent;
        end if;
	-- update parent ccount
	update STEM_META_FORM
	set ccount = ccount + 1,
	    is_system = 1
	where id_STEM_META_FORM = new.id_parent;
      else
        new.clevel := 1;
      end if;
      -- update child nodes clevel
      update STEM_META_FORM
      set clevel = new.clevel + 1,
          is_system = 1
      where id_parent = new.id_STEM_META_FORM;
      -- update old parent ccount
      update STEM_META_FORM
      set ccount = ccount - 1,
          is_system = 1
      where id_STEM_META_FORM = old.id_parent;
    end if;
    -- update audit data
    new.lastdate := current_timestamp;
    new.createdate := old.createdate;
    -- check immutable
    if (old.is_immutable = 1) then
      raise exception '��������� ��������� ��������� ������: ID_STEM_META_FORM = %', old.ID_STEM_META_FORM;
    end if;
  -- system changes  
  else
    if (new.clevel is null) then
      new.clevel := old.clevel;
    end if;
    if (old.clevel != new.clevel) then
      -- update child nodes clevel
      update STEM_META_FORM
      set clevel = new.clevel + 1,
          is_system = 1
      where id_parent = old.id_STEM_META_FORM;
    end if;
  end if;
  -- disable system change flag
  new.is_system := 0;  
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_META_FORM
before update
on STEM_META_FORM
for each row
execute procedure F_TUB_STEM_META_FORM();


-- trigger function for INSERT
create or replace function F_TDA_STEM_META_FORM() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_META_FORM is null) then
    new.id_STEM_META_FORM := nextval('G_ID_STEM_META_FORM');
  end if;
  -- immutable field init
  if (new.is_immutable is null) then
    new.is_immutable := 0;
  end if;
  -- audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TDA_STEM_META_FORM 
before insert
on STEM_META_FORM  
for each row
execute procedure F_TDA_STEM_META_FORM();


-- trigger function for INSERT
create or replace function F_TIB_STEM_META_FRAME() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_META_FRAME is null) then
    new.id_STEM_META_FRAME := nextval('G_ID_STEM_META_FRAME');
  end if;
  --
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_META_FRAME 
before insert
on STEM_META_FRAME  
for each row
execute procedure F_TIB_STEM_META_FRAME();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_META_FRAME() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_META_FRAME 
before update
on STEM_META_FRAME 
for each row
execute procedure F_TUB_STEM_META_FRAME();


-- trigger function for INSERT
create or replace function F_TIB_STEM_META_METHOD() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_META_METHOD is null) then
    new.id_STEM_META_METHOD := nextval('G_ID_STEM_META_METHOD');
  end if;
  -- immutable field init
  if (new.is_immutable is null) then
    new.is_immutable := 0;
  end if;
  -- audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for INSERT
create trigger TIB_STEM_META_METHOD 
before insert
on STEM_META_METHOD  
for each row
execute procedure F_TIB_STEM_META_METHOD();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_META_METHOD() 
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- check immutable
  if (old.is_immutable = 1) then
    raise exception '��������� ��������� ��������� ������: ID_STEM_META_METHOD = %', old.ID_STEM_META_METHOD;
  end if;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_META_METHOD 
before update
on STEM_META_METHOD 
for each row
execute procedure F_TUB_STEM_META_METHOD();


-- trigger function for UPDATE
create or replace function F_TDA_STEM_META_METHOD() 
returns trigger as
$body$
declare
begin
  -- check immutable
  if (old.is_immutable = 1) then
    raise exception '��������� �������� ��������� ������: ID_STEM_META_METHOD = %', old.ID_STEM_META_METHOD;
  end if;
  -- return
  return old;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TDA_STEM_META_METHOD 
after delete
on STEM_META_METHOD 
for each row
execute procedure F_TDA_STEM_META_METHOD();


-- trigger function for DELETE
create or replace function F_TDA_STEM_META_METHODGROUP() 
returns trigger as
$body$
declare
begin
  -- check immutable
  if (old.is_immutable = 1) then
    raise exception '��������� �������� ��������� ������: ID_STEM_META_METHODGROUP = %', old.ID_STEM_META_METHODGROUP;
  end if;
  -- decrease parent ccount
  update STEM_META_METHODGROUP
  set ccount = ccount - 1,
      is_system = 1
  where id_STEM_META_METHODGROUP = old.id_parent;
  -- delete childs
  delete from STEM_META_METHODGROUP
  where id_parent = old.id_STEM_META_METHODGROUP;
  -- return old
  return old;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger after DELETE
create trigger TDA_STEM_META_METHODGROUP
after delete
on STEM_META_METHODGROUP
for each row
execute procedure F_TDA_STEM_META_METHODGROUP();


-- trigger function INSERT
create or replace function F_TIB_STEM_META_METHODGROUP() 
returns trigger as
$body$
declare
begin
  -- get next id
  if (new.id_STEM_META_METHODGROUP is null) then
    new.id_STEM_META_METHODGROUP := nextval('G_ID_STEM_META_METHODGROUP');
  end if;
  -- immutable field init
  if (new.is_immutable is null) then
    new.is_immutable := 0;
  end if;
  -- audit data
  new.createdate := current_timestamp;
  new.lastdate := current_timestamp;
  -- init values
  new.ccount := 0;
  new.is_system := 0;
  new.clevel := 1;
  -- default id_parent init (root node)
  if (new.id_parent is null) then 
    new.id_parent := 0;
  end if;
  -- deny self reference
  if (new.id_parent = new.id_STEM_META_METHODGROUP) then
    raise exception 'self reference deny for tree tables';
  end if;
  -- if not root node
  if (new.id_parent != 0) then
    -- calc child level
    select clevel + 1
    into new.clevel
    from STEM_META_METHODGROUP
    where id_STEM_META_METHODGROUP = new.id_parent;
    -- check parent exists
    if (found = false) then
      raise exception 'parent key with id=% not found', new.id_parent;
    end if;
    -- update parent counter for child
    update STEM_META_METHODGROUP
    set ccount = ccount + 1,
        is_system = 1 -- system update flag on
    where id_STEM_META_METHODGROUP = new.id_parent;
  end if;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger INSERT
create trigger TIB_STEM_META_METHODGROUP
before insert
on STEM_META_METHODGROUP 
for each row
execute procedure F_TIB_STEM_META_METHODGROUP();


-- check what id_child is child of id_parent, if true return 1 else 0
create or replace function F_CKCH_STEM_META_METHODGROUP(aid_parent int, aid_child int)
returns int as 
$body$
declare
  fid int;
begin
  -- search in child nodes (recurse)
  for fid in 
    select id_STEM_META_METHODGROUP
    from STEM_META_METHODGROUP
    where id_parent = aid_parent
  loop
    if (fid = aid_child) then
      return 1;
    end if;
    -- recurse
    select F_CKCH_STEM_META_METHODGROUP(fid, aid_child) into fid;
    if (fid = 1) then
      return 1;
    end if;
  end loop;
  return 0;
end
$body$
language 'plpgsql'; 
-- trigger function for UPDATE
create or replace function F_TUB_STEM_META_METHODGROUP() 
returns trigger as
$body$
declare
  fis_child int;
begin
  -- disable id changing
  new.id_STEM_META_METHODGROUP := old.id_STEM_META_METHODGROUP;
  -- if not system change
  if (new.is_system = 0) then
    -- disable system parameters change
    new.clevel := old.clevel;
    new.ccount := old.ccount;    
    if (new.id_parent is null) then
      new.id_parent := old.id_parent;
    end if;
    -- deny self reference
    if (new.id_parent = new.id_STEM_META_METHODGROUP) then
      raise exception 'self reference deny for tree tables';
    end if;    
    -- process id_parent change
    if (new.id_parent != old.id_parent) then
      if (new.id_parent != 0) then
	-- check recursive reference
	select F_CKCH_STEM_META_METHODGROUP(new.id_STEM_META_METHODGROUP, new.id_parent) into fis_child;
	if (fis_child = 1) then
      raise exception 'recursive references deny for tree table';
	end if;
        -- calc our child level
        select clevel + 1
        into new.clevel
        from STEM_META_METHODGROUP
        where id_STEM_META_METHODGROUP = new.id_parent;
        -- check parent exists
        if (found = false) then
          raise exception 'parent node with id=% not found', new.id_parent;
        end if;
	-- update parent ccount
	update STEM_META_METHODGROUP
	set ccount = ccount + 1,
	    is_system = 1
	where id_STEM_META_METHODGROUP = new.id_parent;
      else
        new.clevel := 1;
      end if;
      -- update child nodes clevel
      update STEM_META_METHODGROUP
      set clevel = new.clevel + 1,
          is_system = 1
      where id_parent = new.id_STEM_META_METHODGROUP;
      -- update old parent ccount
      update STEM_META_METHODGROUP
      set ccount = ccount - 1,
          is_system = 1
      where id_STEM_META_METHODGROUP = old.id_parent;
    end if;
    -- update audit data
    new.lastdate := current_timestamp;
    new.createdate := old.createdate;
    -- check immutable
    if (old.is_immutable = 1) then
      raise exception '��������� ��������� ��������� ������: ID_STEM_META_METHODGROUP = %', old.ID_STEM_META_METHODGROUP;
    end if;
  -- system changes  
  else
    if (new.clevel is null) then
      new.clevel := old.clevel;
    end if;
    if (old.clevel != new.clevel) then
      -- update child nodes clevel
      update STEM_META_METHODGROUP
      set clevel = new.clevel + 1,
          is_system = 1
      where id_parent = old.id_STEM_META_METHODGROUP;
    end if;
  end if;
  -- disable system change flag
  new.is_system := 0;  
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker; 
-- trigger for UPDATE
create trigger TUB_STEM_META_METHODGROUP
before update
on STEM_META_METHODGROUP
for each row
execute procedure F_TUB_STEM_META_METHODGROUP();

