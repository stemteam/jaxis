/*==============================================================*/
/* DBMS name:      InterBase 6.x                                */
/* Created on:     27.11.2014 15:50:30                          */
/*==============================================================*/


create generator G_ID_STEM_META_FIELD;

create generator G_ID_STEM_META_METHOD;

create generator G_ID_STEM_META_METHODGROUP;

/*==============================================================*/
/* Table: STEM_META_FIELD                                       */
/*==============================================================*/
create table STEM_META_FIELD (
ID_STEM_META_FIELD   INTEGER                        not null,
ID_STEM_META_METHOD  INTEGER                        not null,
NAME                 VARCHAR(256),
CAPTION              VARCHAR(2048),
DESCRIPTION          VARCHAR(2048),
IS_ID                SMALLINT                       default 0
      constraint CKC_IS_ID_STEM_MET check (IS_ID is null or (IS_ID in (0,1))),
DATATYPE             VARCHAR(512),
IS_READONLY          SMALLINT                       default 0
      constraint CKC_IS_READONLY_STEM_MET check (IS_READONLY is null or (IS_READONLY in (0,1))),
IS_HIDDEN            SMALLINT                       default 0
      constraint CKC_IS_HIDDEN_STEM_MET check (IS_HIDDEN is null or (IS_HIDDEN in (0,1))),
WIDTH                INTEGER,
DEFAULTVALUE         VARCHAR(256),
VALUELIST            VARCHAR(2048),
PRESENTATION         VARCHAR(256),
IMPORTANCE           SMALLINT,
RELATIONMETHOD       VARCHAR(256),
RELATIONFIELD        VARCHAR(256),
GROUPNAME            VARCHAR(256),
LASTDATE             DATE,
CREATEDATE           DATE,
IS_TREEPARENT        SMALLINT                       default 0
      constraint CKC_IS_TREEPARENT_STEM_MET check (IS_TREEPARENT is null or (IS_TREEPARENT in (0,1))),
IS_TREEFIELD         SMALLINT                       default 0
      constraint CKC_IS_TREEFIELD_STEM_MET check (IS_TREEFIELD is null or (IS_TREEFIELD in (0,1))),
IS_IMMUTABLE         SMALLINT                       default 0
      constraint CKC_IS_IMMUTABLE_STEM_ME2 check (IS_IMMUTABLE is null or (IS_IMMUTABLE in (0,1))),
IS_MANDATORY         SMALLINT                       default 0
      constraint CKC_IS_MANDATORY_STEM_MET check (IS_MANDATORY is null or (IS_MANDATORY in (0,1))),
constraint PK_STEM_META_FIELD primary key (ID_STEM_META_FIELD)
);

-- MethodGroupListGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),1, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_treefield, is_immutable) values (gen_id(g_id_stem_meta_field,1),1, 'MethodGroupName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_treeparent, is_immutable) values (gen_id(g_id_stem_meta_field,1),1, 'MethodGroupParentId', '������������� ������������ ������', '������������� ������������ ������', 0, 'Int32', 0, null, 1, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable) values (gen_id(g_id_stem_meta_field,1),1, 'Methods', '������', '������ ������', 0, 'DataSet', 0, null, 1, 'meta/MethodListGet', 'MethodGroupId', 1);

-- MethodGroupGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),2, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),2, 'MethodGroupName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),2, 'MethodGroupParentId', '������������� ������������ ������', '������������� ������������ ������', 0, 'Int32', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable) values (gen_id(g_id_stem_meta_field,1),2, 'Methods', '������', '������ ������', 0, 'DataSet', 0, null, 1, 'meta/MethodListGet', 'MethodGroupId', 1);

-- FieldListGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'FieldId', '�������������', '�������������', 1, 'Int32', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'Name', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'Caption', '���������', '�������� ����(��� �������/����� � �����)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'Description', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'IsId', '�������������', '���� �������� ��������������� ��� ������� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'DataType', '��� ������', '��� ����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'IsReadonly', '������ ��� ������', '������ ��� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'IsHidden', '�� ������������', '�������� �������������� �����', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'Width', '������', '������ ����(��� ���������� ����������� � ����)', 0, 'Int32', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'DefaultValue', '�������� �� ���������', '�������� �� ���������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'ValueList', '������ ��������', '��������, ������� ����� ��������� ����', 0, 'String[]', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'Presentation', '���������� �������������', '���������� ������������� Custom, Text, Decimal, Boolean, DateTime, Time � ������� ��������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'Importance', '��������', '��������. 0 - ������, 1 ����� ������ � �.�. ����������� �������, ����������� �������� ��������� ���� � ��������� �� �������� "���" ��� ���������� ������ � �����������, �� ������� �������.', 0, 'Int32', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'RelationMethod', '�����', '����� ��� ������ �� �����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'RelationField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'GroupName', '������ �����', '������������ ��� ����������� ����� � ���� ����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'IsTreeParent', '������ ��� ������', '�������, ��� ���� �������� ����� ������� �� ��������, �� �������� �������� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'IsTreeField', '���� ������', '����, �� �������� �������� ������ (� ����� ����� ��� ������ ���)', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),11, 'IsMandatory', '������������', '�������� �� ������������ � ����������', 0, 'Int16', 0, null, 1, 1);

-- FieldGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'FieldId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'Name', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'Caption', '���������', '�������� ����(��� �������/����� � �����)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'Description', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'IsId', '�������������', '���� �������� ��������������� ��� ������� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'DataType', '��� ������', '��� ����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'IsReadonly', '������ ��� ������', '������ ��� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'IsHidden', '�� ������������', '�������� �������������� �����', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'Width', '������', '������ ����(��� ���������� ����������� � ����)', 0, 'Int32', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'DefaultValue', '�������� �� ���������', '�������� �� ���������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'ValueList', '������ ��������', '��������, ������� ����� ��������� ����', 0, 'String[]', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'Presentation', '���������� �������������', '���������� ������������� Custom, Text, Decimal, Boolean, DateTime, Time � ������� ��������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'Importance', '��������', '��������. 0 - ������, 1 ����� ������ � �.�. ����������� �������, ����������� �������� ��������� ���� � ��������� �� �������� "���" ��� ���������� ������ � �����������, �� ������� �������.', 0, 'Int32', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'RelationMethod', '�����', '����� ��� ������ �� �����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'RelationField', '����� (����)', '���� ������������ � �������� ��������� ��� ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'GroupName', '������ �����', '������������ ��� ����������� ����� � ���� ����', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'IsTreeParent', '������ ��� ������', '�������, ��� ���� �������� ����� ������� �� ��������, �� �������� �������� ������', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'IsTreeField', '���� ������', '����, �� �������� �������� ������ (� ����� ����� ��� ������ ���)', 0, 'Int16', 0, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),12, 'IsMandatory', '������������', '�������� �� ������������ � ����������', 0, 'Int16', 0, null, 1, 1);

-- MethodListGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),6, 'MethodId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),6, 'MethodName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),6, 'MethodDescription', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable) values (gen_id(g_id_stem_meta_field,1),6, 'Fields', '����', '���� ������', 0, 'DataSet', 0, null, 1, 'meta/FieldListGet', 'MethodName', 1);

-- MethodGet
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),7, 'MethodId', '�������������', '�������������', 1, 'Int32', 1, null, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),7, 'MethodName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),7, 'MethodDescription', '��������', '�������� ����(������ � �.�.)', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, relationMethod, relationField, is_immutable) values (gen_id(g_id_stem_meta_field,1),7, 'Fields', '����', '���� ������', 0, 'DataSet', 0, null, 1, 'meta/FieldListGet', 'MethodName', 1);


-- MethodGroupAdd
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),3, 'MethodGroupName', '��������', '�������� ���� ��� � ������ ������', 0, 'String', 0, 80, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),3, 'MethodGroupParentId', '������������� ������������ ������', '������������� ������������ ������', 0, 'Int32', 0, null, 1, 1);

-- MethodGroupEdit
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_hidden, is_immutable) values (gen_id(g_id_stem_meta_field,1),4, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 0, null, 1, 1, 1);
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_immutable) values (gen_id(g_id_stem_meta_field,1),4, 'MethodGroupName', '��������', '�������� ������', 0, 'String', 0, 80, 1, 1);

-- MethodGroupDelete
insert into stem_meta_field (id_stem_meta_field, id_stem_meta_method, name, caption, description, is_id, datatype, is_readonly, width, importance, is_hidden, is_immutable) values (gen_id(g_id_stem_meta_field,1),5, 'MethodGroupId', '�������������', '�������������', 1, 'Int32', 0, null, 1, 1, 1);


/*==============================================================*/
/* Index: FI_STEM_META_FIELD_METHOD                             */
/*==============================================================*/
create asc index FI_STEM_META_FIELD_METHOD on STEM_META_FIELD (
ID_STEM_META_METHOD
);

/*==============================================================*/
/* Index: SI_STEM_META_FIELD                                    */
/*==============================================================*/
create unique asc index SI_STEM_META_FIELD on STEM_META_FIELD (
ID_STEM_META_METHOD,
NAME
);

/*==============================================================*/
/* Table: STEM_META_METHOD                                      */
/*==============================================================*/
create table STEM_META_METHOD (
ID_STEM_META_METHOD  INTEGER                        not null,
NAME                 VARCHAR(256),
DESCRIPTION          VARCHAR(2048),
ID_STEM_META_METHODGROUP INTEGER                        not null,
CREATEDATE           DATE,
LASTDATE             DATE,
IS_IMMUTABLE         SMALLINT                       default 0
      constraint CKC_IS_IMMUTABLE_STEM_ME1 check (IS_IMMUTABLE is null or (IS_IMMUTABLE in (0,1))),
constraint PK_STEM_META_METHOD primary key (ID_STEM_META_METHOD)
);

insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (1, 2, 'meta/MethodGroupListGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (2, 2, 'meta/MethodGroupGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (3, 2, 'meta/MethodGroupAdd', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (4, 2, 'meta/MethodGroupEdit', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (5, 2, 'meta/MethodGroupDelete', 1);
--
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (6, 2, 'meta/MethodListGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (7, 2, 'meta/MethodGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (8, 2, 'meta/MethodAdd', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (9, 2, 'meta/MethodEdit', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (10, 2, 'meta/MethodDelete', 1);
--
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (11, 2, 'meta/FieldListGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (12, 2, 'meta/FieldGet', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (13, 2, 'meta/FieldAdd', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (14, 2, 'meta/FieldEdit', 1);
insert into stem_meta_method (id_stem_meta_method, id_stem_meta_methodgroup, name, is_immutable) values (15, 2, 'meta/FieldDelete', 1);
SET GENERATOR g_id_stem_meta_method TO 100;

/*==============================================================*/
/* Index: SI_STEM_META_METHOD                                   */
/*==============================================================*/
create unique asc index SI_STEM_META_METHOD on STEM_META_METHOD (
NAME
);

/*==============================================================*/
/* Index: FI_STEM_META_METHOD_METHODGROUP                       */
/*==============================================================*/
create asc index FI_STEM_META_METHOD_METHODGROUP on STEM_META_METHOD (
ID_STEM_META_METHODGROUP
);

/*==============================================================*/
/* Table: STEM_META_METHODGROUP                                 */
/*==============================================================*/
create table STEM_META_METHODGROUP (
ID_STEM_META_METHODGROUP INTEGER                        not null,
ID_PARENT            INTEGER                        default 0,
NAME                 VARCHAR(256),
CLEVEL               INTEGER                        default 1,
CCOUNT               INTEGER                        default 0,
IS_SYSTEM            SMALLINT                       default 0
      constraint CKC_IS_SYSTEM_STEM_MET check (IS_SYSTEM is null or (IS_SYSTEM in (0,1))),
CREATEDATE           DATE,
LASTDATE             DATE,
IS_IMMUTABLE         SMALLINT                       default 0
      constraint CKC_IS_IMMUTABLE_STEM_ME0 check (IS_IMMUTABLE is null or (IS_IMMUTABLE in (0,1))),
constraint PK_STEM_META_METHODGROUP primary key (ID_STEM_META_METHODGROUP)
);

insert into stem_meta_methodgroup (id_stem_meta_methodgroup, id_parent, name, is_immutable) values (1, 0, '��� ������', 1);
insert into stem_meta_methodgroup (id_stem_meta_methodgroup, id_parent, name, is_immutable) values (2, 1, '����������', 1);
SET GENERATOR g_id_stem_meta_methodgroup TO 100;

/*==============================================================*/
/* Index: SI_STEM_META_METHODGROUP                              */
/*==============================================================*/
create unique asc index SI_STEM_META_METHODGROUP on STEM_META_METHODGROUP (
ID_PARENT,
NAME
);

alter table STEM_META_FIELD
   add constraint FK_STEM_MET_REFERENCE_STEM_1 foreign key (ID_STEM_META_METHOD)
      references STEM_META_METHOD (ID_STEM_META_METHOD);

alter table STEM_META_METHOD
   add constraint FK_STEM_MET_REFERENCE_STEM_0 foreign key (ID_STEM_META_METHODGROUP)
      references STEM_META_METHODGROUP (ID_STEM_META_METHODGROUP);


create trigger TIB_STEM_META_FIELD for STEM_META_FIELD
before insert as
begin
if (new.id_STEM_META_FIELD is null)
    then new.id_STEM_META_FIELD = gen_id(G_ID_STEM_META_FIELD, 1);
new.lastdate = current_timestamp;
new.createdate = current_timestamp;    
end;


create trigger TUB_STEM_META_FIELD for STEM_META_FIELD
  before update
as
begin
  new.lastdate = current_timestamp;
end;


create trigger TIB_STEM_META_METHOD for STEM_META_METHOD
before insert as
begin
if (new.id_STEM_META_METHOD is null)
    then new.id_STEM_META_METHOD = gen_id(G_ID_STEM_META_METHOD, 1);
new.lastdate = current_timestamp;
new.createdate = current_timestamp;    
end;


create trigger TUB_STEM_META_METHOD for STEM_META_METHOD
  before update
as
begin
  new.lastdate = current_timestamp;
end;


create trigger TIB_STEM_META_METHODGROUP for STEM_META_METHODGROUP
before insert as
begin
if (new.id_STEM_META_METHODGROUP is null)
    then new.id_STEM_META_METHODGROUP = gen_id(G_ID_STEM_META_METHODGROUP, 1);
new.lastdate = current_timestamp;
new.createdate = current_timestamp;    
end;


create trigger TUB_STEM_META_METHODGROUP for STEM_META_METHODGROUP
  before update
as
begin
  new.lastdate = current_timestamp;
end;

