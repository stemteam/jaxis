create or replace function stem_version_check(averinfo in varchar) 
returns void as
$body$
declare
  fmajor int;
  fminor int;
  frelease int;
  fmajor_current int;
  fminor_current int;
  frelease_current int;
  fverinfo_current varchar;
begin
  ------------------------------------------------------------------
  -- Проверяет совместимость переданной версии и текущей версии БД.
  -- Если переданная версия меньше текущей - ругается
  -- Проверяется только мажор + минор (билд-релиз не проверяется - пока вернем проверку)
  ------------------------------------------------------------------
  -- парсим переданное
  select unnest(string_to_array(averinfo, '.')) into fmajor limit 1 offset 0;
  select unnest(string_to_array(averinfo, '.')) into fminor limit 1 offset 1;
  select unnest(string_to_array(averinfo, '.')) into frelease limit 1 offset 2;
  --  что в базе
  select major, minor, release, major || '.' || minor || '.' || release
  into fmajor_current, fminor_current, frelease_current, fverinfo_current
  from stem_version;
  -- сравниваем
  if (fmajor < fmajor_current) then
    raise exception 'Текущая версия %. Обновление до версии % не возможно.', fverinfo_current, averinfo;
  end if;
  if (fminor < fminor_current) then
    raise exception 'Текущая версия %. Обновление до версии % не возможно.', fverinfo_current, averinfo;
  end if;
  if (frelease < frelease_current) then
    raise exception 'Текущая версия %. Обновление до версии % не возможно.', fverinfo_current, averinfo;
  end if;
  if (fmajor = fmajor_current) and (fminor = fminor_current) and (frelease = frelease_current) then
    raise exception 'БД имеет актуальную версию %, обновление невозможно.', fverinfo_current;
  end if;
end
$body$
language 'plpgsql' immutable;
