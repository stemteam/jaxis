create or replace function stem_version_update(averinfo in varchar) 
returns void as
$body$
declare
  fmajor int;
  fminor int;
  frelease int;
begin
  ------------------------------------------------------------------
  -- Обнов версию БД на указанную
  ------------------------------------------------------------------
  -- предварительно проверяем версию
  perform stem_version_check(averinfo);
  -- парсим переданное
  select unnest(string_to_array(averinfo, '.')) into fmajor limit 1 offset 0;
  select unnest(string_to_array(averinfo, '.')) into fminor limit 1 offset 1;
  select unnest(string_to_array(averinfo, '.')) into frelease limit 1 offset 2;
  -- обновляем
  update stem_version
  set major = fmajor,
      minor = fminor,
      release = frelease;      
end
$body$
language 'plpgsql';
