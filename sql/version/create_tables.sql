/*==============================================================*/
/* Table: STEM_VERSION                                          */
/*==============================================================*/
create table STEM_VERSION (
   ID_STEM_VERSION      INT4                 not null,
   MAJOR                INT4                 not null,
   MINOR                INT4                 not null,
   RELEASE              INT4                 not null,
   CREATEDATE           TIMESTAMP            not null default CURRENT_TIMESTAMP,
   LASTDATE             TIMESTAMP            not null default CURRENT_TIMESTAMP
);

insert into stem_version (id_stem_version, major, minor, release)
values (1, 1, 0, 0);

alter table STEM_VERSION
   add constraint PK_STEM_VERSION primary key (ID_STEM_VERSION);

/*==============================================================*/
/* Index: SI_STEM_VERSION                                       */
/*==============================================================*/
create unique index SI_STEM_VERSION on STEM_VERSION (
MAJOR,
MINOR,
RELEASE
);


-- trigger function for INSERT
create or replace function F_TIB_STEM_VERSION()
returns trigger as
$body$
declare
begin
  raise exception 'Запрещено добавление записей в таблицу версий';
  return null;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for INSERT
create trigger TIB_STEM_VERSION
before insert
on STEM_VERSION
for each row
execute procedure F_TIB_STEM_VERSION();


-- trigger function for UPDATE
create or replace function F_TUB_STEM_VERSION()
returns trigger as
$body$
declare
begin
  -- update audit data
  new.createdate := old.createdate;
  new.lastdate := current_timestamp;
  -- return mandatory
  return new;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger for UPDATE
create trigger TUB_STEM_VERSION
before update
on STEM_VERSION
for each row
execute procedure F_TUB_STEM_VERSION();


-- trigger function for DELETE
create or replace function F_TDB_STEM_VERSION()
returns trigger as
$body$
declare
begin
  raise exception 'Запрещено удаление данных из таблицы версий';
  return null;
end
$body$
language 'plpgsql' volatile called on null input security invoker;
-- trigger after DELETE
create trigger TDB_STEM_VERSION
before delete
on STEM_VERSION
for each row
execute procedure F_TDB_STEM_VERSION();
