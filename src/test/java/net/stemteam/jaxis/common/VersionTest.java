/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author medved
 */
public class VersionTest {

    public VersionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * 1.1 compareTo 1.1.1
     */
    @Test
    public void test1() {
        Version a = new Version("1.1");
        Version b = new Version("1.1.1");
        int result = a.compareTo(b);
        assertEquals(-1, result);
    }

    /**
     * 1.1 equals 1.1.1
     */
    @Test
    public void test2() {
        Version a = new Version("1.1");
        Version b = new Version("1.1.1");
        assertEquals(a.equals(b), false);
    }

    /**
     * 2.0 compareTo 1.9.9
     */
    @Test
    public void test3() {
        Version a = new Version("2.0");
        Version b = new Version("1.9.9");
        assertEquals(a.compareTo(b), 1);
    }

    /**
     * 2.0 equals 1.9.9
     */
    @Test
    public void test4() {
        Version a = new Version("2.0");
        Version b = new Version("1.9.9");
        assertEquals(a.equals(b), false);
    }

    /**
     * 1.0 compareTo 1
     */
    @Test
    public void test5() {
        Version a = new Version("1.0");
        Version b = new Version("1");
        assertEquals(a.compareTo(b), 0);
    }

    /**
     * 1.0 equals 1
     */
    @Test
    public void test6() {
        Version a = new Version("1.0");
        Version b = new Version("1");
        assertEquals(a.equals(b), true);
    }

    /**
     * 1 compareTo null
     */
    @Test
    public void test7() {
        Version a = new Version("1");
        Version b = null;
        assertEquals(a.compareTo(b), 1);
    }

    /**
     * 1 equals null
     */
    @Test
    public void test8() {
        Version a = new Version("1");
        Version b = null;
        assertEquals(a.equals(b), false);
    }

    /**
     * get min version
     */
    @Test
    public void test9() {
        List<Version> versions = new ArrayList<Version>();
        versions.add(new Version("2"));
        versions.add(new Version("1.0.5"));
        versions.add(new Version("1.01.0"));
        versions.add(new Version("1.00.1"));
        assertEquals(Collections.min(versions).get(), "1.00.1");
    }

    /**
     * get max version
     */
    @Test
    public void test10() {
        List<Version> versions = new ArrayList<Version>();
        versions.add(new Version("2"));
        versions.add(new Version("1.0.5"));
        versions.add(new Version("1.01.0"));
        versions.add(new Version("1.00.1"));
        assertEquals(Collections.max(versions).get(), "2");
    }

    /**
     * warning
     */
    @Test
    public void test11() {
        Version a = new Version("2.06");
        Version b = new Version("2.060");
        assertEquals(a.equals(b), false);
    }

}
