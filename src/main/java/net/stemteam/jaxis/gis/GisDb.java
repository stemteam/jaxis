package net.stemteam.jaxis.gis;

import net.stemteam.jaxis.db.AbstractDbPool;
import net.stemteam.jaxis.err.NotConfiguredException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * База геообъектов
 */
public class GisDb {
    
    private AbstractDbPool pool;
    /**
     * Установка пула соединений с конкретной БД
     * @param pool 
     */
    public void setDbPool(AbstractDbPool pool) {
        this.pool = pool;
    }
    
    public Connection getConnection() throws SQLException, NotConfiguredException {
        if (pool == null) {
            throw new NotConfiguredException("GisDb not configured, pool is null.");
        }
        return pool.getConnection();
    }
    
    public static GisDb getInstance() {
        return GisDbHolder.INSTANCE;
    }

    private static class GisDbHolder {
        private static final GisDb INSTANCE = new GisDb();
    }
}
