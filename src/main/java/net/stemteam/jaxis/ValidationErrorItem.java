package net.stemteam.jaxis;

/**
 * Это элемент ответа сервиса в случае ошибки, поддерживается в .json2, .jaxis
 */
public class ValidationErrorItem {

    /**
     * Сообщение об ошибке
     */
    private String Message;

    /**
     * Класс ошибки (взамен кода ошибки), пример: FieldIsEmpty
     */
    private String Exception;

    /**
     * Объект который виновен
     */
    private String ExceptionObject;

    /**
     * Стек вызовов
     */
    private String CallStack;

    public ValidationErrorItem(String exception, String exceptionObject, String message) {
        this.Exception = exception;
        this.ExceptionObject = exceptionObject;
        this.Message = message;
    }
    
    public ValidationErrorItem(String exception, String exceptionObject, String message, String callStack) {
        this.Exception = exception;
        this.ExceptionObject = exceptionObject;
        this.Message = message;
        this.CallStack = callStack;
    }

    public String getMessage() {
        return Message;
    }

    public String getException() {
        return Exception;
    }

    public String getExceptionObject() {
        return ExceptionObject;
    }

    public String getCallStack() {
        return CallStack;
    }

}
