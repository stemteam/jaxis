package net.stemteam.jaxis.captcha;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 *
 */
final class Background {

    /**
     * @param from
     * @param color
     * @return
     */
    public static BufferedImage add(final BufferedImage from, final Color color) {
        final int width = from.getWidth();
        final int height = from.getHeight();

        final BufferedImage to = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        final Graphics2D g2d = to.createGraphics();
        g2d.setColor(color);
        g2d.fill(new Rectangle2D.Double(0, 0, width, height));
        g2d.drawImage(from, 0, 0, null);
        return to;
    }

}
