/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.captcha;

/**
 * Доступ к IP адресу
 */
public class IpAccess {
    
    private String ip;
    private long lastAccessTime;
    private int incorrectCount;
    
    public IpAccess(String ip, int incorrectCount) {
        this.ip = ip;
        this.incorrectCount = incorrectCount;
    }

    public long getLastAccessTime() {
        return lastAccessTime;
    }

    public int getIncorrectCount() {
        return incorrectCount;
    }

    public void setIncorrectCount(int incorrectCount) {
        this.incorrectCount = incorrectCount;
        lastAccessTime = System.currentTimeMillis();
    }

}
