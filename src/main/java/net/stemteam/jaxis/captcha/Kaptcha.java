package net.stemteam.jaxis.captcha;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;

public class Kaptcha {

    private final int width;
    private final int height;

    private final KaptchaConfig config;

    /**
     *
     */
    public Kaptcha() {
        this(new KaptchaConfig());
    }

    /**
     * @param config
     */
    public Kaptcha(final KaptchaConfig config) {
        this.config = config;
        this.width = config.getWidth();
        this.height = config.getHeight();
    }

    /**
     * Create an image which will have written a distorted text.
     *
     * @param text the distorted characters
     * @return image with the text
     */
    public BufferedImage createImage(final String text) {
        BufferedImage bi = WordRenderer.renderWord(text, config);
        bi = Distorter.distort(bi, config.isAddNoise(), config.getNoiseColor());
        bi = Background.add(bi, config.getBackgroundColor());
        final Graphics2D graphics = bi.createGraphics();

        if (config.isDrawBorder()) {
            drawBorder(graphics);
        }

        return bi;
    }

    /**
     * @param graphics
     */
    private void drawBorder(final Graphics2D graphics) {
        graphics.setColor(config.getBorderColor());
        graphics.draw(new Line2D.Double(0, 0, 0, width));
        graphics.draw(new Line2D.Double(0, 0, width, 0));
        graphics.draw(new Line2D.Double(0, height - 1, width, height - 1));
        graphics.draw(new Line2D.Double(width - 1, height - 1, width - 1, 0));
    }

}
