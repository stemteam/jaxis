package net.stemteam.jaxis.captcha;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Random;

/**
 * Рендерер слов
 */
final class WordRenderer {

    /**
     * @param word
     * @param width
     * @param height
     * @param config
     * @return
     */
    public static BufferedImage renderWord(final String word, final KaptchaConfig config) {
        final int width = config.getWidth();
        final int height = config.getHeight();

        final BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_ARGB);
        final Graphics2D g2D = image.createGraphics();
        g2D.setColor(config.getTextColor());

        final RenderingHints hints = new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        hints.add(new RenderingHints(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY));
        g2D.setRenderingHints(hints);

        final FontRenderContext frc = g2D.getFontRenderContext();
        final Random random = new Random();

        final int fontSize = config.getFontSize();
        final int startPosY = (height - fontSize) / 5 + fontSize;

        final char[] wordChars = word.toCharArray();
        final Font[] chosenFonts = new Font[wordChars.length];
        final int[] charWidths = new int[wordChars.length];
        int widthNeeded = 0;
        final List<Font> fonts = config.getFonts();
        final int size = fonts.size();
        for (int i = 0; i < wordChars.length; i++) {

            chosenFonts[i] = fonts.get(random.nextInt(size));

            final char[] charToDraw = new char[]{wordChars[i]};
            final GlyphVector gv = chosenFonts[i].createGlyphVector(frc, charToDraw);
            charWidths[i] = (int) gv.getVisualBounds().getWidth();
            if (i > 0) {
                widthNeeded = widthNeeded + 2;
            }
            widthNeeded = widthNeeded + charWidths[i];
        }

        final int charSpace = config.getCharacterSpacing();

        int startPosX = (width - widthNeeded) / 2;
        for (int i = 0; i < wordChars.length; i++) {
            g2D.setFont(chosenFonts[i]);
            final char[] charToDraw = new char[]{wordChars[i]};
            g2D.drawChars(charToDraw, 0, charToDraw.length, startPosX,
                    startPosY);
            startPosX = startPosX + charWidths[i] + charSpace;
        }

        return image;
    }

}
