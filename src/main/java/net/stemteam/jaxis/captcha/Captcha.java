/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.captcha;

/**
 * Капча
 */
public class Captcha {
    
    
    private String image;
    private String text;   
    private long createTime;
    
    public Captcha(String image, String text) {
        createTime = System.currentTimeMillis();
        this.image = image;
        this.text = text;
    }

    public String getImage() {
        return image;
    }

    public String getText() {
        return text;
    }

    public long getCreateTime() {
        return createTime;
    }
}
