/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.captcha;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import javax.imageio.ImageIO;

/**
 * Капчи и иже с ними
 */
public class CaptchaHelper {

    private final String imageDir;
    private final int captchaLiveTimeSec;
    // карта ImageId => Captcha
    private volatile HashMap<String, Captcha> captchas = new HashMap();
    // карта IP - время последнего неудачного доступа
    private volatile HashMap<String, IpAccess> ips = new HashMap();

    /**
     * Возвращает путь к каталогу с капчами
     *
     * @return
     */
    public String getImageDir() {
        return imageDir;
    }

    /**
     * Хэлпер капчелпер
     *
     * @param imageDir папка с капчами
     * @param captchaLiveTimeSec время жизни капчи (секунд)
     */
    public CaptchaHelper(String imageDir, int captchaLiveTimeSec) {
        this.imageDir = imageDir;
        this.captchaLiveTimeSec = captchaLiveTimeSec;
        File f = new File(imageDir);
        if (!f.exists()) {
            f.mkdirs();
        }
    }

    /**
     * Проверяет нужно ли требовать капчу от указанного IP
     *
     * @param ip
     * @return
     */
    public synchronized boolean checkIp(String ip) {
        IpAccess access = ips.get(ip);
        if (access == null) {
            return true;
        }
        if ((System.currentTimeMillis() - access.getLastAccessTime()) > 3600000) {
            return true;
        }
        if (access.getIncorrectCount() < 3) {
            return true;
        }
        return false;
    }
    
    /**
     * Обработка некорректного входа
     * @param ip адрес входящего
     */
    public synchronized void processIncorrect(String ip) {
        IpAccess access = ips.get(ip);
        if (access == null) {
            ips.put(ip, new IpAccess(ip, 1));
            return;
        }
        access.setIncorrectCount(access.getIncorrectCount() + 1);
    }
    
    /**
     * Обработка корректного входа
     * @param ip 
     */
    public synchronized void processCorrect(String ip) {
        IpAccess access = ips.get(ip);
        if (access == null) {
            return;
        }
        access.setIncorrectCount(0);
    }

    /**
     * Создает новую капчу
     *
     * @return
     */
    public synchronized Captcha createCaptcha() throws IOException {
        // генерируем имя капчи
        String imageName = UUID.randomUUID().toString().concat(".png");
        
        // генерируем текст
        String text = KaptchaTextCreator.getText(5);
        
        // генерируем картинку
        KaptchaConfig cfg = new KaptchaConfig();
        //        Random rand = new Random();
//        Color randomColor1 = new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
//        Color randomColor2 = new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat());
//        cfg.setTextColor(randomColor1.darker());
//        cfg.setNoiseColor(randomColor1.darker());
//        cfg.setBackgroundColor(randomColor2.brighter());
        
        cfg.setWidth(150);
        cfg.setHeight(60);
        Kaptcha kap = new Kaptcha(cfg);
        BufferedImage bi = kap.createImage(text);
        // отключаем дисковый кэш
        if (!ImageIO.getUseCache()) {
        ImageIO.setUseCache(false);
        }
        ImageIO.write(bi, "png", new File(imageDir.concat("/").concat(imageName)));

        Captcha cap = new Captcha(imageName, text);
        captchas.put(imageName, cap);

        // тут же удалем устаревшие капчи
        Iterator it = captchas.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Captcha> pairs = (Map.Entry) it.next();
            Captcha cp = pairs.getValue();
            if ((System.currentTimeMillis() - cp.getCreateTime()) > captchaLiveTimeSec * 1000L) {
                // удаляем файл изображения
                File f = new File(imageDir.concat("/").concat(cp.getImage()));
                if (f.exists()) {
                    f.delete();
                }
                it.remove(); // avoids a ConcurrentModificationException
            }
        }
        return cap;
    }

    /**
     * Проверка капчи
     *
     * @param captchaImage
     * @param captchaText
     * @return
     */
    public synchronized boolean checkCaptcha(String captchaImage, String captchaText) {
        Captcha cap = captchas.get(captchaImage);
        if (cap == null) {
            return false;
        }
        if (!cap.getText().equals(captchaText)) {
            return false;
        }
        captchas.remove(captchaImage);
        return true;
    }

}
