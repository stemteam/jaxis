package net.stemteam.jaxis.captcha;

import java.util.Random;

/**
 * @author Alexandru Bledea
 * @since Jul 1, 2014
 */
public final class KaptchaTextCreator {

	private static final char[] CHARACTERS = "abcde2345678gfynmnpwx".toCharArray();

	/**
	 *
	 */
	private KaptchaTextCreator() {
	}

	/**
	 * @return the random text
	 */
	public static String getText(final int length) {
		final StringBuffer text = new StringBuffer();
		final Random rand = new Random();
		for (int i = 0; i < length; i++) {
			final int index = rand.nextInt(CHARACTERS.length);
			text.append(CHARACTERS[index]);
		}

		return text.toString();
	}
}
