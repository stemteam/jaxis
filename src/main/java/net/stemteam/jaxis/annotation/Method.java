/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.annotation;

import static java.lang.annotation.ElementType.METHOD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;

/**
 * Аннотация web-метода jaxis
 *
 * @author Андрей Николаев <vajadhava@gmail.com>
 */
@Retention(RUNTIME)
@Target(value = METHOD)
public @interface Method {

    /**
     * Описание метода в аннотации
     *
     * @return
     */
    public String desc() default "";

    /**
     * Параметры метода в аннотации
     *
     * @return
     */
    public Param[] params();

    /**
     * Возврат из метода в аннотации
     *
     * @return
     */
    public Return returns();
}
