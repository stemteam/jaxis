/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.annotation;

import static java.lang.annotation.ElementType.METHOD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import net.stemteam.datatransport.transport.DataColumnType;

/**
 * Аннотация параметра метода
 * 
 * @author Андрей Николаев <vajadhava@gmail.com>
 */
@Retention(RUNTIME)
@Target(value = METHOD)
public @interface Param {

    /**
     * Обязательный ли к заполнению
     * @return 
     */
    boolean required() default false;

    /**
     * Название параметра
     * @return 
     */
    String name();
    
    /**
     * Длина параметра (актуально для текстовых параметров)
     * Если -1 - значит длина данного параметра не важна
     * @retun
     */
    int length() default -1;

    /**
     * Тип параметра
     * @return 
     */
    DataColumnType type() default DataColumnType.STRING;

    /**
     * Человеческое описание параметра
     *
     * @return
     */
    String desc();
}
