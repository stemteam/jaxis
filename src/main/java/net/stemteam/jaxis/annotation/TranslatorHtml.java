/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.annotation;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

/**
 * Транслирует аннотации классов в html формат документации API
 */
public class TranslatorHtml {

    private static final String ROW_SEP = "\n";
    private static final String HTML_P = "<p>";
    private static final String HTML_P_END = "</p>";

    /**
     * Запрещен вызов конструкторов у классов-утилит
     */
    protected TranslatorHtml() {
        throw new UnsupportedOperationException("Forbidden for tool classes");
    }

    /**
     * Транслирует аннотацию класса в html формат
     *
     * @param annotatedClass класс с аннотацией
     * @param url урл вызова метода
     * @return
     * @throws java.io.UnsupportedEncodingException
     */
    public static String getDoc(Class annotatedClass, String url) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        // прочтем-ко анотации методов класса
        for (Method method : annotatedClass.getMethods()) {
            if (Modifier.isPublic(method.getModifiers())) {
                net.stemteam.jaxis.annotation.Method serviceDef = method.getAnnotation(
                        net.stemteam.jaxis.annotation.Method.class);
                if (serviceDef == null) {
                    continue;
                }
                String desc = serviceDef.desc();
                Param[] params = serviceDef.params();
                Return returns = serviceDef.returns();

                // заголовок
                sb.append("<h3>").append(annotatedClass.getSimpleName()).append("</h3>");
                // описание
                sb.append(HTML_P).append(escapeHtml(desc)).append(HTML_P_END);
                // URL
                sb.append(HTML_P).append("<abbr title=\"Uniform Resource Locator\">URL</abbr>: ").
                        append(escapeHtml(url)).append(HTML_P_END);

                // параметры
                if (params.length > 0) {
                    sb.append(HTML_P).append("Параметры:").append(ROW_SEP).append(HTML_P_END);
                    sb.append("<table class=\"table table-striped table-bordered table-condensed\">");
                    sb
                            .append("<thead><tr>")
                            .append("<td><abbr title=\"Обязательное ли к заполнению * = да\">*</abbr></td>")
                            .append("<td>Имя</td>")
                            .append("<td>Тип</td>")
                            .append("<td>Описание</td>")
                            .append("</tr></thead>");
                    for (Param param : params) {
                        sb
                                .append("<tr>")
                                .append("<td>").append(param.required() ? "*" : "").append("</td>")
                                .append("<td>").append(escapeHtml(param.name())).append("</td>")
                                .append("<td>").append(escapeHtml(param.type().getName())).append(param.length() != -1
                                                ? ("(" + param.length() + ")") : "").append("</td>")
                                .append("<td>").append(escapeHtml(param.desc())).append("</td>")
                                .append("</tr>");
                    }
                    sb.append("</table>");
                }

                if (returns.items().length == 0) {
                    sb.append(HTML_P).append("Возвращает: HTTP_OK (200)").append(HTML_P_END);
                } else {
                    sb.append(HTML_P).append("Возвращает: ").append(escapeHtml(returns.type().getName()));
                    if (!returns.desc().isEmpty()) {
                        sb.append(" - ").append(escapeHtml(returns.desc()));
                    }
                    sb.append(HTML_P_END);

                    sb.append("<table class=\"table table-striped table-bordered table-condensed\">");
                    sb
                            .append("<thead><tr>")
                            .append("<td>Имя</td>")
                            .append("<td>Тип</td>")
                            .append("<td>Описание</td>")
                            .append("</tr></thead>");
                    for (Param param : returns.items()) {
                        sb
                                .append("<tr>")
                                .append("<td>").append(escapeHtml(param.name())).append("</td>")
                                .append("<td>").append(escapeHtml(param.type().getName())).append(param.length() != -1
                                                ? "(" + param.length() + ")" : "").append("</td>")
                                .append("<td>").append(escapeHtml(param.desc())).append("</td>")
                                .append("</tr>");
                    }
                    sb.append("</table>");
                }
            }
        }
        return sb.toString();
    }

}
