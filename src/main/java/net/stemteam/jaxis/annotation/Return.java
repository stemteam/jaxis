/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.annotation;

import static java.lang.annotation.ElementType.METHOD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import net.stemteam.datatransport.transport.DataColumnType;

/**
 * Аннотация возврата метода
 *
 * @author Андрей Николаев <vajadhava@gmail.com>
 */
@Retention(RUNTIME)
@Target(value = METHOD)
public @interface Return {

    /**
     * Тип
     *
     * @return
     */
    DataColumnType type() default DataColumnType.DATASET;

    /**
     * Описание
     *
     * @return
     */
    String desc() default "";

    /**
     * Элементы из которых состоит, для табличных результатов
     *
     * @return
     */
    Param[] items();
}
