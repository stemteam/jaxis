/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.annotation;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Транслирует аннотации классов в dokuwiki формат
 */
public class TranslatorWiki {

    private static final String WIKI_TABLE_COLUMN_SEP = "|";
    private static final String ROW_SEP = "\n";
    private static final String WIKI_PARAGRAPH_SEP = ROW_SEP + ROW_SEP;

    /**
     * Запрещен вызов конструкторов у классов-утилит
     */
    protected TranslatorWiki() {
        throw new UnsupportedOperationException("Forbidden for tool classes");
    }

    /**
     * Возвращает dokuwiki-фрагмент из описания класса
     *
     * @param annotatedClass класс с аннотацией
     * @return
     */
    public static String getDoc(Class annotatedClass, String url) {
        StringBuilder sb = new StringBuilder();

        // прочтем-ко анотации методов класса
        for (Method method : annotatedClass.getMethods()) {
            if (Modifier.isPublic(method.getModifiers())) {
                net.stemteam.jaxis.annotation.Method serviceDef = method.getAnnotation(
                        net.stemteam.jaxis.annotation.Method.class);
                if (serviceDef == null) {
                    continue;
                }
                String desc = serviceDef.desc();
                Param[] params = serviceDef.params();
                Return returns = serviceDef.returns();

                sb.append("==== ").append(annotatedClass.getSimpleName()).append(" ====").append(WIKI_PARAGRAPH_SEP);
                sb.append(desc).append(WIKI_PARAGRAPH_SEP);
                sb.append("URL: ").append(url).append(WIKI_PARAGRAPH_SEP);

                if (params.length > 0) {
                    sb.append("Параметры:").append(WIKI_PARAGRAPH_SEP);
                    sb.append("^((Обязательное ли к заполнению * = да))^Имя^Тип^Описание^").append(ROW_SEP);
                    for (Param param : params) {
                        sb.append(WIKI_TABLE_COLUMN_SEP);
                        sb.append(param.required() ? "*" : " ");
                        sb.append(WIKI_TABLE_COLUMN_SEP);
                        sb.append(param.name());
                        sb.append(WIKI_TABLE_COLUMN_SEP);
                        sb.append(param.type().getName());
                        if (param.length() != -1) {
                            sb.append("(").append(param.length()).append(")");
                        }
                        sb.append(WIKI_TABLE_COLUMN_SEP);
                        sb.append(param.desc());
                        sb.append(WIKI_TABLE_COLUMN_SEP);
                        sb.append(ROW_SEP);
                    }
                    sb.append(ROW_SEP);
                }

                if (returns.items().length == 0) {
                    sb.append("Возвращает: HTTP_OK (200)");
                } else {
                    sb.append("Возвращает: ").append(returns.type().getName());
                    if (!returns.desc().isEmpty()) {
                        sb.append(" - ").append(returns.desc());
                    }
                    sb.append(WIKI_PARAGRAPH_SEP);
                    // параметры
                    sb.append("^Имя^Тип^Описание^").append(ROW_SEP);
                    for (Param param : returns.items()) {
                        sb.append(WIKI_TABLE_COLUMN_SEP);
                        sb.append(param.name());
                        sb.append(WIKI_TABLE_COLUMN_SEP);
                        sb.append(param.type().getName());
                        if (param.length() != -1) {
                            sb.append("(").append(param.length()).append(")");
                        }
                        sb.append(WIKI_TABLE_COLUMN_SEP);
                        sb.append(param.desc());
                        sb.append(WIKI_TABLE_COLUMN_SEP);
                        sb.append(ROW_SEP);
                    }
                    sb.append(ROW_SEP);
                }

                sb.append(WIKI_PARAGRAPH_SEP);
            }
        }
        return sb.toString();
    }

}
