/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.annotation;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import net.stemteam.datatransport.transport.DataColumnType;

/**
 * Формирует набор классов и сьюит для тестирования веб-методов в системе TestNG
 */
public class TranslatorTestNg {

    /**
     * Запрещен вызов конструкторов у классов-утилит
     */
    protected TranslatorTestNg() {
        throw new UnsupportedOperationException("Forbidden for tool classes");
    }

    /**
     * Возвращает имя тест-лкасса для переданного класса
     *
     * @param c
     * @return
     */
    public static String getTestClassName(Class c) {
        return c.getCanonicalName() + "Test";
    }

    public static String getTestClassSimpleName(Class c) {
        return c.getSimpleName() + "Test";
    }

    /**
     * Возвращает dokuwiki-фрагмент из описания класса
     *
     * @param annotatedClass класс с аннотацией
     * @param url урла публикации хэндлера
     * @return
     */
    public static String getTestClassSource(Class annotatedClass, String url) {
        StringBuilder sb = new StringBuilder();

        // прочтем-ко анотации методов класса
        for (Method method : annotatedClass.getMethods()) {
            if (Modifier.isPublic(method.getModifiers())) {
                net.stemteam.jaxis.annotation.Method serviceDef = method.getAnnotation(
                        net.stemteam.jaxis.annotation.Method.class);
                if (serviceDef == null) {
                    continue;
                }
                String desc = serviceDef.desc();
                Param[] params = serviceDef.params();
                Return returns = serviceDef.returns();

                // package
                sb.append("package ").append(annotatedClass.getPackage().getName()).append(";\n\n");

                // import
                sb.append("import java.io.IOException;\n");
                sb.append("import java.sql.SQLException;\n");
                sb.append("import java.io.UnsupportedEncodingException;\n");
                sb.append("import net.stemteam.jaxis.common.HttpHelper;\n");
                sb.append("import net.stemteam.jaxis.common.UrlHelper;\n");
                sb.append("import net.stemteam.jaxis.common.WebRequestData;\n");
                sb.append("import org.junit.Assert;\n");
                sb.append("import org.testng.annotations.Parameters;\n");
                sb.append("import org.testng.annotations.Test;\n");
                sb.append("import net.stemteam.datatransport.transport.DataSet;\n");
                sb.append("import net.stemteam.datatransport.transport.DataColumn;\n");
                sb.append("import net.stemteam.datatransport.transport.StringEncoder;\n");

                sb.append("\n");

                // class def
                String testClassName = getTestClassSimpleName(annotatedClass);
                sb.append("/**\n");
                sb.append(" * Тестовый класс для метода ").append(annotatedClass.getSimpleName()).append("\n");
                sb.append(" */\n");
                sb.append("public class ").append(testClassName).append(" {\n\n");

                // get data method
                String jdocDesc = desc.replace("\n", " ");
                sb.append("    /**\n");
                sb.append("     * ").append(jdocDesc).append("\n");
                sb.append("     *\n");
                sb.append("     * @param url URL сервиса\n");
                for (Param param : params) {
                    sb.append("     * @param ").append(param.name()).append(" ").append(param.desc().replace("\n", "")).
                            append("\n");
                }
                sb.append("     * @return WebRequestData\n");
                sb.append("     */\n");
                sb.append("    public static WebRequestData getData(String url");
                for (Param param : params) {
                    sb.append(", ").append(param.type().getJavaClass().getCanonicalName()).append(" ").append(param.
                            name());
                }
                sb.append(")\n");
                sb.append("        throws UnsupportedEncodingException, IOException, SQLException {\n");
                sb.append("            StringBuilder httpGet = new StringBuilder();\n");
                sb.append("            httpGet\n");
                sb.append("                .append(url)\n");
                sb.append("                .append(\"").append(url).append(".jaxis\")");
                boolean first = true;
                for (Param param : params) {
                    sb.append("\n                .append(\"");
                    if (first) {
                        sb.append("?\")");
                    } else {
                        sb.append("&\")");
                    }
                    sb
                            .append(".append(\"")
                            .append(param.name())
                            .append("=\").append(UrlHelper.Encode(");

                    if (param.type().equals(DataColumnType.TIMESTAMP)) {
                        sb.append("StringEncoder.encodeTimeStamp(").append(param.name()).append(")");
                    } else {
                        sb.append(param.name());
                    }
                    sb.append("))");
                    first = false;
                }
                sb.append(";\n");
                sb.append("    System.out.println(\"HTTP GET: \" + httpGet.toString());\n");
                sb.append(
                        "        WebRequestData data = HttpHelper.requestGet(httpGet.toString(), null, null, 30000, 30000);\n");
                sb.append("        return data;\n");
                sb.append("    }\n\n");

                // тесты на не передачу обязательных параметров
                for (Param param : params) {
                    if (param.required()) {
                        // описание
                        sb.append("    @Test(description = \"Ошибка при передаче пустого параметра ").append(param.
                                name()).append("\")\n");
                        // параметры, которые всегда берем из конфига
                        sb.append("    @Parameters({\"Url\"})\n");
                        sb.append("    public void testEmpty").append(param.name()).append(
                                "(String url) throws UnsupportedEncodingException, IOException, SQLException {\n");
                        sb.append("        WebRequestData resp = ").append(testClassName).append(".getData(url");
                        for (Param p : params) {
                            if (p.name().equals(param.name())) {
                                sb.append(", null");
                            } else {
                                // берем непустую заглушку для параметра по типу
                                sb.append(", ").append(getDummyVariable(p.type()));
                            }
                        }
                        sb.append(");\n");
                        sb.append("        Assert.assertEquals(resp.getHttpCode(), 400);\n");
                        sb.append("    }\n\n");
                    }
                }

                // TODO хэлпер для проверки корректности формата возвращенного датасета
                if (returns.items().length > 0) {
                    sb.append("    /**\n");
                    sb.append("     * Проверет переданный датасет на соответствие формату результатов метода\n");
                    sb.append("     */\n");
                    sb.append("    public static void checkResultFormat(DataSet ds) throws Exception {\n");
                    sb.append("        if (ds == null) {\n");
                    sb.append("            throw new Exception(\"DataSet = null\");\n");
                    sb.append("        }\n");
                    sb.append("        if (ds.getColumns().size() != ").append(returns.items().length).append(") {\n");
                    sb.append("            throw new Exception(\"неверное количество полей, ожидается ").append(returns.
                            items().length).append(", фактически \" + ds.getColumns().size());\n");
                    sb.append("        }\n");

                    sb.append("        boolean checked = false;\n");

                    for (Param param : returns.items()) {
                        sb.append("        // проверка поля ").append(param.name()).append("\n");
                        sb.append("        checked = false;\n");
                        sb.append("        for (DataColumn col : ds.getColumns()) {\n");
                        sb.append("            // проверяем имя\n");
                        sb.append("            if (col.getName().equals(\"").append(param.name()).append("\")) {\n");
                        sb.append("                // проверяем тип\n");
                        sb.append("                if (col.getDataTypeName().equals(\"").append(param.type().getName()).
                                append(
                                        "\")) {\n");
                        sb.append("                    checked = true;\n");
                        sb.append("                    break;\n");
                        sb.append("                }\n");
                        sb.append("            }\n");
                        sb.append("        }\n");

                        sb.append("        if (!checked) {\n");
                        sb.append("            throw new Exception(\"Некорректный формат датасета - поле ").append(
                                param.
                                name()).append(" (").append(param.type().getName()).append(") не найдено.\");\n");
                        sb.append("        }\n");
                    }
                    sb.append("    }\n\n");
                }

                // TODO тест на передачу некорректного ApiKey - не понятно как делать, некорректный Token может 
                // начать ругаться первым
                // TODO тест на передачу некорректного токена
                sb.append("}\n\n");
            }
        }
        return sb.toString();
    }

    /**
     * Возвращает заглушку непустого значения для типа
     *
     * @param type
     * @return
     */
    private static String getDummyVariable(DataColumnType type) {
        if (type.equals(DataColumnType.BOOLEAN)) {
            return "new Boolean(true)";
        }
        if (type.equals(DataColumnType.DATASET)) {
            return "new DataSet()";
        }
        if ((type.equals(DataColumnType.DATE)) || (type.equals(DataColumnType.DATETIME)) || (type.equals(
                DataColumnType.TIMESTAMP))) {
            return "new java.sql.Timestamp(System.currentTimeMillis())";
        }
        if (type.equals(DataColumnType.FLOAT)) {
            return "new Double(10.0)";
        }
        if (type.equals(DataColumnType.GEOMETRY)) {
            return "new IkGeometry()";
        }
        if ((type.equals(DataColumnType.INT16)) || (type.equals(DataColumnType.INT32))) {
            return "new Integer(13)";
        }
        if (type.equals(DataColumnType.INT64)) {
            return "new Long(63L)";
        }
        return "\"SampleString\"";
    }

}
