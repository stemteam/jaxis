/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.statistics;

/**
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class StatisticItem {
    private String url;
    private int callCount = 1;
    private long executionTime = 0;
    private long packTime = 0;
    private long sendTime = 0;
    private long dataSize = 0;
    
    private long maxExecutionTime = 0;
    private long maxPackTime = 0;
    private long maxSendTime = 0;
    private long maxAllTime = 0;
    private long maxDataSize = 0;

    public int getCallCount() {
        return callCount;
    }

    public long getExecutionTime() {
        return executionTime;
    }

    public String getUrl() {
        return url;
    }

    public StatisticItem(String url, int callCount, long executionTime, long packTime, long sendTime, long dataSize) {
        this.url = url;
        this.callCount = callCount;
        this.executionTime = executionTime;
        this.packTime = packTime;
        this.sendTime = sendTime;
        this.dataSize = dataSize;
        this.maxDataSize = dataSize;
        maxExecutionTime = executionTime;
        maxPackTime = packTime;
        maxSendTime = sendTime;
        maxAllTime = executionTime + packTime + sendTime;
        
    }

    public void incParams(long executionTime, long packTime, long sendTime, long dataSize) {
        this.callCount++;
        this.executionTime += executionTime;
        if (maxExecutionTime < executionTime) {
            maxExecutionTime = executionTime;
        }
        this.packTime += packTime;
        if (maxPackTime < packTime) {
            maxPackTime = packTime;
        }
        this.sendTime += sendTime;
        if (maxSendTime < sendTime) {
            maxSendTime = sendTime;
        }
        long atime = executionTime + packTime + sendTime;
        if (maxAllTime < atime) {
            maxAllTime = atime;
        }
        this.dataSize += dataSize;
        if (maxDataSize < dataSize) {
            maxDataSize = dataSize;
        }
    }

    public long getMaxExecutionTime() {
        return maxExecutionTime;
    }

    public long getPackTime() {
        return packTime;
    }

    public long getSendTime() {
        return sendTime;
    }
    
    public long getAllTime() {
        return executionTime + packTime + sendTime;
    }
    
    public long getMaxAllTime() {
        return maxAllTime;
    }
    
    public long getMaxPackTime() {
        return maxPackTime;
    }
    
    public long getMaxSendTime() {
        return maxSendTime;
    }

    public long getDataSize() {
        return dataSize;
    }

    public long getMaxDataSize() {
        return maxDataSize;
    }
    
    
}
