/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.statistics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Запись логов вызовов вэб-методов
 */
public class StatisticWriter {

    public static final String DEFAULT_FORMAT = "{}\t{}\t{}\t{}\t{} ms\t{} ms\t{} ms\t{}";

    protected StatisticWriter() {
        throw new UnsupportedOperationException("not supported for tool classes");
    }

    private static final Logger logger = LoggerFactory.getLogger(StatisticWriter.class);

    /**
     * Запись в лог. Используется slf4j с уровнем INFO.
     *
     * @param format строка сообщения, в форматах slf4j {} {} {} ...
     * @param params параметры строки сообщения, подставляются по порядку в {}
     */
    public static void write(String format, Object... params) {
        logger.info(format, params);
    }
}
