/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.jaxis;

import java.io.File;
import java.util.UUID;

/**
 * Хранилка файлов
 */
public class UploadStore {
    
    // неизменяемый урл загрузки изображений уровня вэб-сервера
    public static final String uploadPath = "./upload";

    /**
     * Проверка существования файла
     * @param fileUID
     * @return 
     */
    public static boolean isFileExists(String fileUID) {
        File f = new File(uploadPath + "/" + fileUID);
        return f.exists();
    }
    
    /**
     * Удаление файла
     * @param fileUID 
     * @return  
     */
    public static boolean deleteFile(String fileUID) {
        File f = new File(uploadPath + "/" + fileUID);
        return f.delete();
    }
    
    /**
     * Формирует имя файла для сохранения
     * @param fileExt расширение файла
     * @return 
     */
    public static String prepareFileName(String fileExt) {
        return (new StringBuilder()).append(UUID.randomUUID().toString()).append(".").append(fileExt).toString();
    }
    
    public static String getFileNameByGuid(String fileUID) {
        return uploadPath + "/" + fileUID;
    }
    
    /**
     * Возвращает путь к папке аплоада изображений
     * @return 
     */
    public static String getUploadPath() {
        return uploadPath;
    }
    
    /**
     * Формирует 
     * @param fileUID
     * @param width
     * @param height
     * @return 
     */
    public static String prepareImageName(String fileUID, int width, int height) {
        return width + "x" + height + "-" + fileUID;
    }
        
}
