/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.handler;

import net.stemteam.jaxis.common.HttpHelper;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.common.UrlHelper;
import net.stemteam.jaxis.statistics.StatisticWriter;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Хэндлер, возвращающий статические данные
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class StaticHandler extends AtomHandler implements HttpHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public static final String STATIC_PATH = "./static"; // каталог статических данных

    /**
     * Обработка запросов клиента. Внутри вызывается prepareResponce, в которой и должен быть реализован ответ.
     *
     * @param exchange
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        long executionTimeMs = System.currentTimeMillis();
        long packTimeMs;
        long sendTimeMs;
        long allTimeMs = executionTimeMs;
        String query = "";
        Object userAgent;
        try {
            // получаем путь в урле (без GET параметров, url-перекодированный)
            String url = exchange.getRequestURI().getPath();
            setRequestUrl(url);

            // определяем тип контента
            contentType = getContentType(exchange);

            // получаем параметры GET и POST
            // POST-параметры получаем напрямую здесь, фильтр не используем, 
            // поскольку иногда он срабатывает по нескольку раз на запрос 
            // (приходят не все заголовки, а часть).
            HashMap<String, Object> params = HttpHelper.parseParams(exchange);

            // получаем заголовки
            Headers headers = exchange.getRequestHeaders();
            userAgent = headers.get("User-agent");
            if (userAgent == null) {
                userAgent = "unknown";
            }

            // получаем IP адрес клиента
            remoteIp = extactIp(headers, exchange);

            // TODO выполняем метод проверки прав (abstract)
            // TODO поддерживать сжатие порций данных
            // читаем бинарные данные файла, пишем в поток            
            String filename = STATIC_PATH + "/" + url.substring(8); // TODO в этом месте не катит getRequestUrl(), он иногда возвращает не то что устанавливается выше, особенность.
            log.debug("url: {}, filename: {} ", exchange.getRequestURI().getPath(), filename);
            File file = new File(filename);

            // если файла нет -ругаемся и выходим
            if (!file.exists()) {
                String response
                        = "<html><head><style type=\"text/css\"></style></head><body><h1>404 Not Found</h1>No context found for request</body></html>";
                byte[] responseBin = response.getBytes("utf-8");
                Headers responseHeaders = exchange.getResponseHeaders();
                responseHeaders.set("Content-Type", JaxisContentType.HTML.getContentType());
                // TODO разрешаем кроссдоменные запросы только для допустимых доменов (брать из конфига)
                // responseHeaders.set("Access-Control-Allow-Origin", "*");
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, responseBin.length);
                exchange.getResponseBody().write(responseBin);
                exchange.close();
                return;
            }

            // запоминаем длину файлика
            long dataSize = file.length();

            // определим MIME-TYPE
            String responseContentType = Files.probeContentType(file.toPath()); // TODO для более точного определения читаем здесь: https://odoepner.wordpress.com/2013/07/29/transparently-improve-java-7-mime-type-recognition-with-apache-tika/
            if (responseContentType == null) {
                responseContentType = "application/octet-stream";
            }

            Headers responseHeaders = exchange.getResponseHeaders();
            responseHeaders.set("Content-Type", responseContentType);

            String contentDisposition = "inline; filename*=UTF-8''" + HttpHelper.rfc5987_encode(file.getName());
            responseHeaders.set("Content-Disposition", contentDisposition);

            // TODO разрешаем кроссдоменные запросы только для допустимых доменов (брать из конфига)
//            responseHeaders.set("Access-Control-Allow-Origin", "*");
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, dataSize);
            // шлем содержимое файлика
            FileInputStream in = new FileInputStream(filename);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                exchange.getResponseBody().write(buf, 0, len);
            }
            in.close();
            exchange.close();

            executionTimeMs = System.currentTimeMillis() - executionTimeMs; // продолжительность (мс)
            packTimeMs = 0;
            sendTimeMs = executionTimeMs;
            executionTimeMs = 0;

            // статистика выполнения запросов
            StatisticWriter.write(StatisticWriter.DEFAULT_FORMAT, getExtLogInfo(), url, UrlHelper.Decode(query),
                    userAgent.toString(), executionTimeMs, packTimeMs, sendTimeMs, dataSize);

        } catch (IOException ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам нужна продолжительность выполнения в случае отказов сети
            log.error("CRITICAL EXCEPTION IO" + " Execution time (ms)" + String.valueOf(allTimeMs) + getRequestUrl()
                    + query, ex);
            throw ex;
        } catch (NullPointerException ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам может понадобиться продолжительность выполнения
            log.error("CRITICAL EXCEPTION NULL POINTER" + " Execution time (ms)" + String.valueOf(allTimeMs)
                    + getRequestUrl() + query, ex);
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
            exchange.close();
        } catch (Exception ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам может понадобиться продолжительность выполнения
            log.error("CRITICAL EXCEPTION" + " Execution time (ms)" + String.valueOf(allTimeMs) + getRequestUrl()
                    + query, ex);
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
            exchange.close();
        }
    }

}
