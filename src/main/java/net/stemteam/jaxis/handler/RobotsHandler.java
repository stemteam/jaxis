/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.handler;

import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.common.UrlHelper;
import net.stemteam.jaxis.statistics.StatisticWriter;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.net.HttpURLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Хэндлер, возвращающий robots.txt
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class RobotsHandler extends AtomHandler implements HttpHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        long executionTimeMs = System.currentTimeMillis();
        long packTimeMs = 0;
        long sendTimeMs = 0;
        long allTimeMs = executionTimeMs;
        String query = "";
        Object userAgent;
        try {

            // получаем заголовки
            Headers headers = exchange.getRequestHeaders();
            userAgent = headers.get("User-agent");
            if (userAgent == null) {
                userAgent = "unknown";
            }

            // получаем IP адрес клиента
            remoteIp = extactIp(headers, exchange);

            String response = "User-agent: *\n" + "Disallow: /";
            byte[] responseBin = response.getBytes("utf-8");
            Headers responseHeaders = exchange.getResponseHeaders();
            responseHeaders.set("Content-Type", JaxisContentType.DEFAULT.getContentType());
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, responseBin.length);
            exchange.getResponseBody().write(responseBin);
            exchange.close();

            // статистика выполнения запросов
            StatisticWriter.write(StatisticWriter.DEFAULT_FORMAT, getExtLogInfo(), getRequestUrl(),
                    UrlHelper.Decode(query), userAgent.toString(), executionTimeMs, packTimeMs,
                    sendTimeMs, response.length());

        } catch (IOException ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам нужна продолжительность выполнения в случае отказов сети
            log.error("CRITICAL EXCEPTION IO" + " Execution time (ms)" + String.valueOf(allTimeMs) + getRequestUrl()
                    + query.toString(), ex);
            throw ex;
        } catch (NullPointerException ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам может понадобиться продолжительность выполнения
            log.error("CRITICAL EXCEPTION NULL POINTER" + " Execution time (ms)" + String.valueOf(allTimeMs)
                    + getRequestUrl() + query.toString(), ex);
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
            exchange.close();
        } catch (Exception ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам может понадобиться продолжительность выполнения
            log.error("CRITICAL EXCEPTION" + " Execution time (ms)" + String.valueOf(allTimeMs) + getRequestUrl()
                    + query.toString(), ex);
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
            exchange.close();
        }
    }

}
