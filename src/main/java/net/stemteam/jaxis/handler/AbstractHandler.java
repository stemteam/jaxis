/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.handler;

import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.common.HttpHelper;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.statistics.StatisticWriter;
import net.stemteam.jaxis.ParamValidator;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.common.ExceptionHelper;
import net.stemteam.jaxis.common.UrlHelper;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import net.stemteam.jaxis.common.HttpRedirect;
import net.stemteam.jaxis.err.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Самый главный хэндлер
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public abstract class AbstractHandler extends AtomHandler implements
        HttpHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private HttpRedirect redirect;

    /**
     * Инициирует редирект на указаный урл. Если выставлен этот объект - будет произведен редирект.
     *
     * @param redirect
     */
    protected void redirect(HttpRedirect redirect) {
        this.redirect = redirect;
    }

    public AbstractHandler() {
    }

    /**
     * Обработка запросов клиента. Внутри вызывается prepareResponce, в которой и должен быть реализован ответ.
     *
     * @param exchange
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        long executionTimeMs = System.currentTimeMillis();
        long packTimeMs;
        long sendTimeMs;
        long allTimeMs = executionTimeMs;
        Object userAgent;

        StringBuilder query = new StringBuilder();

        try {
            // получаем путь в урле (без GET параметров, url-перекодированный)
            setRequestUrl(exchange.getRequestURI().getPath());

            // определяем тип контента
            contentType = getContentType(exchange);

            // получаем параметры GET и POST
            // POST-параметры получаем напрямую здесь, фильтр не используем,
            // поскольку иногда он срабатывает по нескольку раз на запрос
            // (приходят не все заголовки, а часть).
            HashMap<String, Object> params = HttpHelper.parseParams(exchange);

            // печатаем параметры в строку, они нам нужны для вывода в лог
            // статистики
            boolean first = true;
            for (Entry<String, Object> entry : params.entrySet()) {
                if (first) {
                    first = false;
                } else {
                    query.append("&");
                }
                String key = entry.getKey();
                Object value = entry.getValue();
                String valueString = "";
                if (value != null) {
                    valueString = value.toString();
                }
                query.append(UrlHelper.Encode(key)).append("=")
                        .append(UrlHelper.Encode(valueString));
            }

            // получаем заголовки
            Headers headers = exchange.getRequestHeaders();
            userAgent = headers.get("User-agent");
            if (userAgent == null) {
                userAgent = "unknown";
            }

            // получаем IP адрес клиента
            remoteIp = extactIp(headers, exchange);

            // определяем, поддерживает ли запрашивающий сжатие методом GZIP
            // Accept-Encoding: gzip, deflate
            boolean isGzipSupported = false;
            String acceptEncoding = exchange.getRequestHeaders().getFirst(
                    "Accept-Encoding");
            if (acceptEncoding != null) {
                acceptEncoding = acceptEncoding.toLowerCase();
                if (acceptEncoding.contains(ENCODING_GZIP)
                        || acceptEncoding.contains(ENCODING_DEFLATE)) {
                    isGzipSupported = true;
                }
            }

            String response = null;
            String error = null;
            String userMessage = null;
            DataSet errorDataSet = null;
            int httpCode = HttpURLConnection.HTTP_OK;

            // чистим массив ошибок
            getParamValidator().clear();

            // выполняем подготовку данных для ответа
            try {
                response = prepareResponce(params, contentType);
            } catch (HttpException ex) {
                userMessage = ex.getUserMessage();
                errorDataSet = ex.getErrorDataSet();
                log.error(query.toString(), ex);
                error = ExceptionHelper.getErrorString(ex);
                httpCode = ex.getHttpCode();
                deleteLoadedFiles(params);
            } catch (SecurityException ex) {
                log.error(query.toString(), ex);
                error = ExceptionHelper.getErrorString(ex);
                httpCode = HttpURLConnection.HTTP_FORBIDDEN;
                deleteLoadedFiles(params);
            } catch (IllegalArgumentException ex) {
                log.error(query.toString(), ex);
                error = ExceptionHelper.getErrorString(ex);
                httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
                deleteLoadedFiles(params);
            } catch (Exception ex) { // в нормальном раскладе сюда вообще упасть
                // не должно
                log.error(query.toString(), ex);
                error = ExceptionHelper.getErrorString(ex);
                httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
                deleteLoadedFiles(params);
            }

            // здесь у нас может произойти редирект, он приоритетнее, выполняем
            if ((redirect != null) && (httpCode == HttpURLConnection.HTTP_OK)) {
                sendTimeMs = System.currentTimeMillis();

                Headers responseHeaders = exchange.getResponseHeaders();

                responseHeaders.add("Location", redirect.getUrl());

                for (Entry<String, String> entry : redirect.getHeaders().entrySet()) {
                    String name = entry.getKey();
                    String val = entry.getValue();
                    responseHeaders.add(name, val);
                }
                exchange.sendResponseHeaders(redirect.getCode(), 0);
                exchange.close();

                StatisticWriter.write(StatisticWriter.DEFAULT_FORMAT, getExtLogInfo(), getRequestUrl(),
                        UrlHelper.Decode(query.toString()), userAgent.toString(), executionTimeMs, 0,
                        System.currentTimeMillis() - sendTimeMs, 0);

                return;
            }

            String exceptionClass = null;
            switch (httpCode) {
                case HttpURLConnection.HTTP_FORBIDDEN:
                    exceptionClass = ParamValidator.FORBIDDEN;
                    break;
                case HttpURLConnection.HTTP_INTERNAL_ERROR:
                    exceptionClass = ParamValidator.INTERNAL;
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    exceptionClass = ParamValidator.BAD_REQUEST;
                    break;
                case HttpURLConnection.HTTP_NOT_FOUND:
                    exceptionClass = ParamValidator.NOT_FOUND;
                    break;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    exceptionClass = ParamValidator.UNAUTHORIZED;
                    break;
                default:
                    exceptionClass = String.valueOf(httpCode);
                    break;
            }

            executionTimeMs = System.currentTimeMillis() - executionTimeMs; // продолжительность
            // (мс)
            packTimeMs = System.currentTimeMillis();

            if (error == null) {
                if (response == null) {
                    response = "";
                }
            } else {
                // может быть так что случилось какое-то исключение, не
                // предусмотренное логикой, и тогда оно не попало в
                // ParamValidator
                if ((contentType == JaxisContentType.JSON2)
                        || (contentType == JaxisContentType.JAXIS)
                        || (contentType == JaxisContentType.JSON)) {
                    if (getParamValidator().isEmpty()) {
                        getParamValidator().add(
                                new ValidationErrorItem(exceptionClass, null,
                                        userMessage, error));
                    }
                }

                // если случилось непоправимое - думаем как и что нам отвечать
                if (contentType == JaxisContentType.JSON2) {
                    if (errorDataSet != null) {
                        response = errorDataSet.toJson2();
                    } else {
                        response = getParamValidator().toDataSet().toJson2();
                    }
                } else if (contentType == JaxisContentType.JAXIS) {
                    if (errorDataSet != null) {
                        response = errorDataSet.toString();
                    } else {
                        response = getParamValidator().toDataSet().toString();
                    }
                } else if (contentType == JaxisContentType.JSON) {
                    if (errorDataSet != null) {
                        response = errorDataSet.toJson();
                    } else {
                        response = getParamValidator().toDataSet().toJson();
                    }
                } else {
                    response = error;
                }
            }

            // формируем ответ
            byte[] responseBin = response.getBytes("utf-8");

            // формируем заголовки
            Headers responseHeaders = exchange.getResponseHeaders();
            responseHeaders.set("Content-Type", contentType.getContentType());

            // если поддерживается GZIP и будем возвращать пожатые данные -
            // будем возвращать еще и заголовок
            if ((isGzipSupported) && (responseBin.length > GZIP_MIN_LENGTH)) {
                responseHeaders.set("Content-Encoding", "gzip");
                responseBin = HttpHelper.packArray(responseBin);
            }
            packTimeMs = System.currentTimeMillis() - packTimeMs;
            int dataSize = responseBin.length;

            sendTimeMs = System.currentTimeMillis();
            // TODO разрешаем кроссдоменные запросы только для допустимых
            // доменов (брать из конфига)
            // responseHeaders.set("Access-Control-Allow-Origin", "*");
            // возвращаем заголовки
            exchange.sendResponseHeaders(httpCode, responseBin.length);
            exchange.getResponseBody().write(responseBin);
            exchange.close();
            sendTimeMs = System.currentTimeMillis() - sendTimeMs;

            // статистика выполнения запросов
            StatisticWriter.write(StatisticWriter.DEFAULT_FORMAT, getExtLogInfo(), getRequestUrl(),
                    UrlHelper.Decode(query.toString()), userAgent.toString(), executionTimeMs,
                    packTimeMs, sendTimeMs, dataSize);

        } catch (IOException ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам нужна
            // продолжительность
            // выполнения в
            // случае
            // отказов сети
            log.error(
                    "CRITICAL EXCEPTION IO" + " Execution time (ms)"
                    + String.valueOf(allTimeMs) + getRequestUrl()
                    + query.toString(), ex);
            throw ex;
        } catch (NullPointerException ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам может
            // понадобиться
            // продолжительность
            // выполнения
            log.error("CRITICAL EXCEPTION NULL POINTER"
                    + " Execution time (ms)" + String.valueOf(allTimeMs)
                    + getRequestUrl() + query.toString(), ex);
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR,
                    0);
            exchange.close();
        } catch (Exception ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам может
            // понадобиться
            // продолжительность
            // выполнения
            log.error(
                    "CRITICAL EXCEPTION" + " Execution time (ms)"
                    + String.valueOf(allTimeMs) + getRequestUrl()
                    + query.toString(), ex);
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR,
                    0);
            exchange.close();
        }
    }

    /**
     * Подготавливает ответ на запрос сервера (строкой)
     *
     * @param params параметры запроса (HTTP параметры)
     * @param contentType (тип содержимого запроса, должен влиять на формат ответа)
     * @return строка
     * @throws net.stemteam.jaxis.err.HttpUnauthorizedException
     * @throws HttpForbiddenException
     * @throws HttpInternalException
     * @throws HttpNotFoundException
     * @throws net.stemteam.jaxis.err.HttpBadRequestException
     */
    public abstract String prepareResponce(HashMap params,
            JaxisContentType contentType) throws HttpException, HttpUnauthorizedException,
            HttpForbiddenException, HttpInternalException,
            HttpNotFoundException, HttpBadRequestException;
}
