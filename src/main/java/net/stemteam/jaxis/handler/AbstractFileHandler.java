/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.handler;

import net.stemteam.jaxis.common.HttpHelper;
import net.stemteam.jaxis.err.HttpBadRequestException;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.err.HttpNotFoundException;
import net.stemteam.jaxis.err.HttpUnauthorizedException;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.common.JsonHelper;
import net.stemteam.jaxis.statistics.StatisticWriter;
import net.stemteam.jaxis.common.ExceptionHelper;
import net.stemteam.jaxis.common.UrlHelper;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map.Entry;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.ParamValidator;
import net.stemteam.jaxis.ValidationErrorItem;
import net.stemteam.jaxis.err.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Самый главный хэндлер (файловый)
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public abstract class AbstractFileHandler extends AtomHandler implements HttpHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private String responseContentType = null;
    private String responseFileName = null;

    /**
     * Имя файла для ответа (Content-Disposition)
     *
     * @return
     */
    public String getResponseFileName() {
        return responseFileName;
    }

    /**
     * Имя файла для ответа (Content-Disposition)
     *
     * @param responseFileName
     */
    public void setResponseFileName(String responseFileName) {
        this.responseFileName = responseFileName;
    }

    /**
     * Content-Type ответа. Можно переопределить используя setResponseContentType(). Если не определен - будет попытка
     * определения по заголовку файла.
     *
     * @return
     */
    public String getResponseContentType() {
        return responseContentType;
    }

    /**
     * Content-Type ответа. Если не определен - будет попытка определения по заголовку файла.
     *
     * @param responseContentType
     */
    public void setResponseContentType(String responseContentType) {
        this.responseContentType = responseContentType;
    }

    /**
     * Обработка запросов клиента. Внутри вызывается prepareResponce, в которой и должен быть реализован ответ.
     *
     * @param exchange
     * @throws IOException
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {

        long executionTimeMs = System.currentTimeMillis();
        long packTimeMs;
        long sendTimeMs;
        long allTimeMs = executionTimeMs;
        StringBuilder query = new StringBuilder();
        Object userAgent;

        try {
            // получаем путь в урле (без GET параметров, url-перекодированный)
            setRequestUrl(exchange.getRequestURI().getPath());

            // определяем тип контента
            contentType = getContentType(exchange);

            // получаем параметры GET и POST
            // POST-параметры получаем напрямую здесь, фильтр не используем, 
            // поскольку иногда он срабатывает по нескольку раз на запрос 
            // (приходят не все заголовки, а часть).
            HashMap<String, Object> params = HttpHelper.parseParams(exchange);

            // печатаем параметры в строку, они нам нужны для вывода в лог статистики
            boolean first = true;
            for (Entry<String, Object> entry : params.entrySet()) {
                if (first) {
                    first = false;
                } else {
                    query.append("&");
                }
                String key = entry.getKey();
                Object value = entry.getValue();
                String valueString = "";
                if (value != null) {
                    valueString = value.toString();
                }
                query.append(UrlHelper.Encode(key)).append("=").append(UrlHelper.Encode(valueString));
            }

            // получаем заголовки
            Headers headers = exchange.getRequestHeaders();
            userAgent = headers.get("User-agent");
            if (userAgent == null) {
                userAgent = "unknown";
            }

            // получаем IP адрес клиента
            remoteIp = extactIp(headers, exchange);

            // TODO поддерживать сжатие порций данных
            // читаем бинарные данные файла, пишем в поток
            // получаем имя файла 
            int httpCode = HttpURLConnection.HTTP_OK;

            // чистим массив ошибок
            getParamValidator().clear();

            // выполняем подготовку датачета для ответа
            String filename = null;
            String error = null;
            String userMessage = null;
            DataSet errorDataSet = null;
            try {
                filename = getFileName(params);
            } catch (HttpException ex) {
                userMessage = ex.getUserMessage();
                errorDataSet = ex.getErrorDataSet();
                log.error(query.toString(), ex);
                error = ExceptionHelper.getErrorString(ex);
                httpCode = ex.getHttpCode();
                deleteLoadedFiles(params);
            } catch (SecurityException ex) {
                log.error(query.toString(), ex);
                error = ExceptionHelper.getErrorString(ex);
                httpCode = HttpURLConnection.HTTP_FORBIDDEN;
                deleteLoadedFiles(params);
            } catch (IllegalArgumentException ex) {
                log.error(query.toString(), ex);
                error = ExceptionHelper.getErrorString(ex);
                httpCode = HttpURLConnection.HTTP_BAD_REQUEST;
                deleteLoadedFiles(params);
            } catch (Exception ex) { // в нормальном раскладе сюда вообще упасть
                // не должно
                log.error(query.toString(), ex);
                error = ExceptionHelper.getErrorString(ex);
                httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;
                deleteLoadedFiles(params);
            }

            executionTimeMs = System.currentTimeMillis() - executionTimeMs; // продолжительность (мс)
            packTimeMs = System.currentTimeMillis();

            String exceptionClass = null;
            switch (httpCode) {
                case HttpURLConnection.HTTP_FORBIDDEN:
                    exceptionClass = ParamValidator.FORBIDDEN;
                    break;
                case HttpURLConnection.HTTP_INTERNAL_ERROR:
                    exceptionClass = ParamValidator.INTERNAL;
                    break;
                case HttpURLConnection.HTTP_BAD_REQUEST:
                    exceptionClass = ParamValidator.BAD_REQUEST;
                    break;
                case HttpURLConnection.HTTP_NOT_FOUND:
                    exceptionClass = ParamValidator.NOT_FOUND;
                    break;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    exceptionClass = ParamValidator.UNAUTHORIZED;
                    break;
                default:
                    exceptionClass = String.valueOf(httpCode);
                    break;
            }

            String response = null;
            if (error != null) {
                // может быть так что случилось какое-то исключение, не
                // предусмотренное логикой, и тогда оно не попало в
                // ParamValidator
                if ((contentType == JaxisContentType.JSON2)
                        || (contentType == JaxisContentType.JAXIS)
                        || (contentType == JaxisContentType.JSON)) {
                    if (getParamValidator().isEmpty()) {
                        getParamValidator().add(
                                new ValidationErrorItem(exceptionClass, null,
                                        userMessage, error));
                    }
                }

                // если случилось непоправимое - думаем как и что нам отвечать
                if (contentType == JaxisContentType.JSON2) {
                    if (errorDataSet != null) {
                        response = errorDataSet.toJson2();
                    } else {
                        response = getParamValidator().toDataSet().toJson2();
                    }
                } else if (contentType == JaxisContentType.JAXIS) {
                    if (errorDataSet != null) {
                        response = errorDataSet.toString();
                    } else {
                        response = getParamValidator().toDataSet().toString();
                    }
                } else if (contentType == JaxisContentType.JSON) {
                    if (errorDataSet != null) {
                        response = errorDataSet.toJson();
                    } else {
                        response = getParamValidator().toDataSet().toJson();
                    }
                } else {
                    response = error;
                }

                Headers responseHeaders = exchange.getResponseHeaders();
                responseHeaders.set("Content-Type", contentType.getContentType());
                // TODO разрешаем кроссдоменные запросы только для допустимых доменов (брать из конфига)
                // responseHeaders.set("Access-Control-Allow-Origin", "*");
                exchange.sendResponseHeaders(httpCode, response.getBytes("utf-8").length);
                exchange.getResponseBody().write(response.getBytes("utf-8"));
                exchange.close();

                // статистика выполнения запросов
                StatisticWriter.write(StatisticWriter.DEFAULT_FORMAT, getExtLogInfo(), getRequestUrl(),
                        UrlHelper.Decode(query.toString()), userAgent.toString(), executionTimeMs, packTimeMs, 0,
                        error.getBytes("utf-8").length);
                return;
            }
            File file = new File(filename);

            // если файла нет -ругаемся и выходим
            if (!file.exists()) {
                getParamValidator().add(new ValidationErrorItem("FileNotFound", null, filename));
                response = getParamValidator().toDataSet().toJson2();
                byte[] responseBin = response.getBytes("utf-8");
                Headers responseHeaders = exchange.getResponseHeaders();
                responseHeaders.set("Content-Type", JaxisContentType.JSON2.getContentType());
                // TODO разрешаем кроссдоменные запросы только для допустимых доменов (брать из конфига)
                // responseHeaders.set("Access-Control-Allow-Origin", "*");
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, responseBin.length);
                exchange.getResponseBody().write(responseBin);
                exchange.close();
                return;
            }

            // запоминаем длину файлика
            long dataSize = file.length();

            // определим MIME-TYPE
            if (responseContentType == null) {
                responseContentType = Files.probeContentType(file.toPath());
                if (responseContentType == null) {
                    responseContentType = "application/octet-stream";
                }
            }

            Headers responseHeaders = exchange.getResponseHeaders();
            responseHeaders.set("Content-Type", responseContentType);

            String contentDisposition;
            if (responseFileName != null) {
                contentDisposition = "inline; filename*=UTF-8''" + HttpHelper.rfc5987_encode(responseFileName);
            } else {
                contentDisposition = "inline; filename*=UTF-8''" + HttpHelper.rfc5987_encode(file.getName());
            }

            responseHeaders.set("Content-Disposition", contentDisposition);

            // TODO разрешаем кроссдоменные запросы только для допустимых доменов (брать из конфига)
            // responseHeaders.set("Access-Control-Allow-Origin", "*");
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, dataSize);

            // NIO version
            final ByteBuffer buffer = ByteBuffer.allocateDirect(32 * 1024);
            FileChannel src = new FileInputStream(file).getChannel();
            WritableByteChannel dest = Channels.newChannel(exchange.getResponseBody());
            while (src.read(buffer) != -1) {
                buffer.flip();
                dest.write(buffer);
                buffer.compact();
            }
            buffer.flip();
            while (buffer.hasRemaining()) {
                dest.write(buffer);
            }
            src.close();
            exchange.close();

            executionTimeMs = System.currentTimeMillis() - executionTimeMs; // продолжительность (мс)
            packTimeMs = 0;
            sendTimeMs = executionTimeMs;
            executionTimeMs = 0;

            // статистика выполнения запросов
            StatisticWriter.write(StatisticWriter.DEFAULT_FORMAT, getExtLogInfo(), getRequestUrl(),
                    UrlHelper.Decode(query.toString()), userAgent.toString(), executionTimeMs,
                    packTimeMs, sendTimeMs, dataSize);
        } catch (IOException ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам нужна продолжительность выполнения в случае отказов сети
            log.error("CRITICAL EXCEPTION IO" + " Execution time (ms)" + String.valueOf(allTimeMs) + getRequestUrl()
                    + query.toString(), ex);
            throw ex;
        } catch (NullPointerException ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам может понадобиться продолжительность выполнения
            log.error("CRITICAL EXCEPTION NULL POINTER" + " Execution time (ms)" + String.valueOf(allTimeMs)
                    + getRequestUrl() + query.toString(), ex);
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
            exchange.close();
        } catch (Exception ex) {
            allTimeMs = System.currentTimeMillis() - allTimeMs; // нам может понадобиться продолжительность выполнения
            log.error("CRITICAL EXCEPTION" + " Execution time (ms)" + String.valueOf(allTimeMs) + getRequestUrl()
                    + query.toString(), ex);
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
            exchange.close();
        }
    }

    public static void fastCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
        final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);

        while (src.read(buffer) != -1) {
            buffer.flip();
            dest.write(buffer);
            buffer.compact();
        }

        buffer.flip();

        while (buffer.hasRemaining()) {
            dest.write(buffer);
        }
    }

    /**
     * Возвращает имя файла для загрузки.
     *
     * @return
     */
    public abstract String getFileName(HashMap<String, Object> params) throws HttpUnauthorizedException,
            HttpForbiddenException, HttpInternalException, HttpNotFoundException, HttpBadRequestException;

}
