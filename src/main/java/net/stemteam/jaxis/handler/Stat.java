/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.handler;

import net.stemteam.jaxis.common.HttpHelper;
import net.stemteam.datatransport.tool.Base64;
import net.stemteam.datatransport.tool.Splitter;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.statistics.StatisticItem;
import net.stemteam.jaxis.statistics.StatisticWriter;
import net.stemteam.jaxis.common.UrlHelper;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.HttpInternalException;
import net.stemteam.jaxis.common.ExceptionHelper;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

/**
 * Возвращает статистику
 */
public class Stat extends AbstractHandler {

    // пароль 
    private String password;

    /**
     * Пароль для выполнения метода
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String prepareResponce(HashMap params, JaxisContentType contentType) throws HttpInternalException,
            HttpForbiddenException {
        // проверка иничиализации пароля
        if ((password == null) || ("".equals(password))) {
            throw new HttpInternalException("Не задан мастер пароль для метода Stat, выполнение невозможно.");
        }
        // считываем параметры
        String Passwd = getParamValidator().getString(params, "Passwd");
        String TmpPasswd = getParamValidator().getString(params, "TmpPasswd");
        String Method = getParamValidator().getString(params, "Method");
        String fileName = getParamValidator().getString(params, "FileName");

        // уа-уа, если валидация параметров не прошла - здесь вылетим
        getParamValidator().postValidate();

        // может быть вход по временному паролю или по настоящему
        if ((Passwd == null) && (TmpPasswd == null)) {
            getParamValidator().throwForbidden("Пароль не указан");
        } else if (Passwd != null) {
            if (!password.equals(Passwd)) {
                getParamValidator().throwForbidden("Пароль не корректен");
            }
        } else if (TmpPasswd != null) {
            // проверяем временный пароль
            if (!checkTemporaryPassword(TmpPasswd)) {
                getParamValidator().throwForbidden("Временный пароль не корректен");
            }
        }

        String NewTmpPasswd;
        try {
            // новый временный парольчик
            NewTmpPasswd = Stat.getTemporaryPassword();
        } catch (UnsupportedEncodingException ex) {
            throw new SecurityException("Password Generation Exception");
        }

        File f = new File(fileName);
        if (!f.exists()) {
            return "Log file not found.";
        }

        StringBuilder sb = new StringBuilder();

        // заголовок
        sb.append("<!DOCTYPE html>");
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<title>jaxis</title>");
        sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />");
        sb.append("<meta name=\"norobots\" content=\"noindex\" />");
        sb.append("<link href=\"http://idea.navstat.ru/assets/css/bootstrap.min.css\" rel=\"stylesheet\">");
        sb.append("<link href=\"http://idea.navstat.ru/assets/css/bootstrap-responsive.min.css\" rel=\"stylesheet\">");
        sb.append(
                "<link media=\"all\" type=\"text/css\" href=\"http://code.jquery.com/ui/1.8.19/themes/base/jquery-ui.css\" rel=\"stylesheet\">\n");
        sb.append(
                "<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"></script>\n");
        sb.append(
                "<script type=\"text/javascript\" src=\"http://code.jquery.com/ui/1.8.19/jquery-ui.min.js\"></script>\n");
        sb.append(
                "<script type=\"text/javascript\" src=\"http://jquery-ui.googlecode.com/svn/tags/latest/ui/minified/i18n/jquery-ui-i18n.min.js\"></script>\n");

        // google chart api
        sb.append("<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>");
        int chartIndex = sb.length();

        // стили для выравнивания в таблице по правому краю
        sb.append("<style>th.right, td.right {text-align:right;}</style>");
        sb.append("</head>");
        sb.append("<body>");
//        sb.append("<div class=\"container\">");
        sb.append("<div class=\"container-fluid\">");

        // если у нас не указан метод - выводим статистику по вызовам методов (общую сгруппированную)
        if (Method == null) {

            sb.append("<div class=\"page-header\"><h1>Общая статистика использования сервиса <small>(");
            sb.append(fileName);
            sb.append(")</small></h1></div>\n\n");

            // календарик выбора дня
            sb.append("<form class=\"well form-inline\">");
            sb.append("<label for=\"datepicker\">Дата:</label> ");
            sb.append("<input name=\"FileName\" class=\"input-small\" id=\"datepicker\" type=\"text\" value=\""
                    + fileName + "\" > ");
            sb.append("<button type=\"submit\" class=\"btn\"><i class=\"icon-play\"></i> Показать</button>");
            sb.append("<input type=\"hidden\" name=\"Passwd\" value=\"" + Passwd + "\" >");
            sb.append("</form>");

            HashMap<String, StatisticItem> map = new HashMap<String, StatisticItem>();

            // карта производительности по часам
            HashMap<Integer, Long> mapProductivityPerHour = new HashMap<Integer, Long>();
            for (Integer i = 0; i < 24; i++) {
                mapProductivityPerHour.put(i, 0L);
            }

            try {
                FileInputStream fstream = new FileInputStream(fileName);
                DataInputStream in = new DataInputStream(fstream);
                BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));
                String strLine;
                while ((strLine = br.readLine()) != null) {

                    Vector data = Splitter.Split(strLine, "\t");

                    String time = (String) data.get(0);
                    try {

                        int hour = Integer.valueOf(time.substring(0, 2));

                        String url = (String) data.get(1);
                        int executionTime = 0;
                        int packTime = 0;
                        int sendTime = 0;
                        long dataSize = 0;
                        if (data.size() > 4) {
                            String s = (String) data.get(4);
                            executionTime = Integer.parseInt(s.replace(" ms", "").trim());
                        }
                        if (data.size() > 5) {
                            String s = (String) data.get(5);
                            try {
                                packTime = Integer.parseInt(s.replace(" ms", "").trim());
                            } catch (NumberFormatException ex) {
                                packTime = 0;
                            }
                        }
                        if (data.size() > 6) {
                            String s = (String) data.get(6);
                            try {
                                sendTime = Integer.parseInt(s.replace(" ms", "").trim());
                            } catch (NumberFormatException ex) {
                                sendTime = 0;
                            }
                        }
                        if (data.size() > 7) {
                            String s = (String) data.get(7);
                            try {
                                dataSize = Long.parseLong(s.trim());
                            } catch (NumberFormatException ex) {
                                sendTime = 0;
                            }
                        }
                        if (!map.containsKey(url)) {
                            map.put(url, new StatisticItem(url, 1, executionTime, packTime, sendTime, dataSize));
                        } else {
                            StatisticItem item = map.get(url);
                            item.incParams(executionTime, packTime, sendTime, dataSize);
                        }

                        // производительность
                        Long l = mapProductivityPerHour.get(hour);
                        if (l == null) {
                            l = 0L;
                        }
                        l += executionTime;
                        mapProductivityPerHour.put(hour, l);
                    } catch (java.lang.RuntimeException eee) {
                        // TODO fix statistic logging unurlencoded params
                    }
                }
                in.close();

                // Chart
                // сортируем по ключу
                List sK = new ArrayList(mapProductivityPerHour.keySet());
                Collections.sort(sK);

                //sb.append("<div id=\"chart_div\" style=\"width: 900px; height: 500px;\"></div>");
                sb.append("\n<div id=\"chart_div\" ></div>\n");

                // чарт добавляем
                String chart
                        = "<script type=\"text/javascript\">\n"
                        + "      google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});\n"
                        + "      google.setOnLoadCallback(drawChart);\n"
                        + "      function drawChart() {\n"
                        + "        var data = google.visualization.arrayToDataTable([\n"
                        + "          ['Час', 'Время'],\n";

                for (Object key : sK) {
                    Long v = mapProductivityPerHour.get(key);
                    chart += "['" + key + "', " + (v / 1000) + "],\n";
                }

                chart += "        ]);\n"
                        + "\n"
                        + "        var options = {\n"
                        + "          title: 'Производительность (секунд)'\n"
                        + "        };\n"
                        + "\n"
                        + "        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));\n"
                        + "        chart.draw(data, options);\n"
                        + "      }\n"
                        + "    </script>\n";
                sb.insert(chartIndex, chart);

                sb.append("<table class=\"table table-striped table-bordered table-condensed\">");
                sb.append("<thead>");
                sb.append(
                        "<tr><th class=\"span3\">Метод</th><th class=\"span1 right\">Вызовов</th><th class=\"span2 right\">Общее</th><th class=\"span2 right\" title=\"Суммарное время выполнения\">В-общее</th><th class=\"span2 right\" title=\"Суммарное время упаковки\">У-общее</th><th class=\"span2 right\" title=\"Суммарное время отправки клиенту\">О-общее</th> <th class=\"span1 right\">Среднее (мс)</th><th class=\"span1 right\">В-сред</th><th class=\"span1 right\">У-сред</th><th class=\"span1 right\">О-сред</th><th class=\"span1 right\">Макс (мс)</th><th class=\"span1 right\">В-макс</th><th class=\"span1 right\">У-макс</th><th class=\"span1 right\">О-макс</th><th class=\"span1 right\">Передано (байт)</th><th class=\"span1 right\">Пер. сред</th><th class=\"span1 right\">Пер. макс</th></tr>");
                sb.append("</thead>");
                sb.append("<tbody>");

                // сортируем по ключу
                List sortedKeys = new ArrayList(map.keySet());
                Collections.sort(sortedKeys);

                // выпечатываем статистику
                long fullExecutionTime = 0;
                for (Object key : sortedKeys) {
                    StatisticItem item = map.get(key);
                    sb.append("<tr>");
                    sb.append("<td>");
                    String url = getRequestUrl() + "?FileName=" + fileName + "&Passwd=" + UrlHelper.Encode(Passwd)
                            + "&Method=" + UrlHelper.Encode(item.getUrl());
                    sb.append("<a href=\"").append(url).append("\">");
                    sb.append(item.getUrl());
                    sb.append("</a>");
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(item.getCallCount());
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(longToTimeIntervalString(item.getAllTime()));
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(longToTimeIntervalString(item.getExecutionTime()));
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(longToTimeIntervalString(item.getPackTime()));
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(longToTimeIntervalString(item.getSendTime()));
                    sb.append("</td>");

                    sb.append("<td class=\"right\">");
                    sb.append(item.getAllTime() / item.getCallCount());
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(item.getExecutionTime() / item.getCallCount());
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(item.getPackTime() / item.getCallCount());
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(item.getSendTime() / item.getCallCount());
                    sb.append("</td>");

                    sb.append("<td class=\"right\">");
                    if (item.getMaxAllTime() >= (this.getJaxis().getKeepAliveTime() * 1000)) {
                        sb.append("<span class=\"label label-important\">" + item.getMaxAllTime() + "</span>");
                    } else {
                        sb.append(item.getMaxAllTime());
                    }
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(item.getMaxExecutionTime());
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(item.getMaxPackTime());
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(item.getMaxSendTime());
                    sb.append("</td>");

                    // передано
                    sb.append("<td class=\"right\">");
                    sb.append(item.getDataSize());
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(item.getDataSize() / item.getCallCount());
                    sb.append("</td>");
                    sb.append("<td class=\"right\">");
                    sb.append(item.getMaxDataSize());
                    sb.append("</td>");

                    sb.append("</tr>");

                    fullExecutionTime += item.getExecutionTime();
                }
                sb.append("</tbody>");
                sb.append("</table>");

                // итого:
                sb.append("<h3>Время выполнения (CPUTIME): ");
                sb.append(longToTimeIntervalString(fullExecutionTime));
                sb.append("</h3>");

            } catch (Exception e) {
                throw new SecurityException("Случилось непоправимое. Подробнее: " + ExceptionHelper.getErrorString(e));
            }
        } else { // если указан метод
            sb.append("<div class=\"page-header\"><h1>" + Method + " <small>(");
            sb.append(fileName);
            sb.append(")</small></h1></div>\n\n");
            try {
                // навигатор
                sb.append("<div class=\"btn-group\">");
                sb.append("<a class=\"btn\" href=\"" + getRequestUrl() + "?FileName=" + fileName + "&Passwd="
                        + UrlHelper.Encode(Passwd) + "\"><i class=\"icon-arrow-left\"></i> Общая статистика</a>");
                sb.append("</div><hr>");

                // календарик выбора дня
                sb.append("<form class=\"well form-inline\">");
                sb.append("<label for=\"datepicker\">Дата:</label> ");
                sb.append("<input name=\"FileName\" class=\"input-small\" id=\"datepicker\" type=\"text\" value=\""
                        + fileName + "\" > ");
                sb.append("<button type=\"submit\" class=\"btn\"><i class=\"icon-play\"></i> Показать</button>");
                sb.append("<input type=\"hidden\" name=\"Passwd\" value=\"" + Passwd + "\" >");
                sb.append("<input type=\"hidden\" name=\"Method\" value=\"" + Method + "\" >");
                sb.append("</form>");

                // парсим вызовы метода
                boolean isTable = false;
                String[] keys = null;

                FileInputStream fstream = new FileInputStream(fileName);
                DataInputStream in = new DataInputStream(fstream);
                BufferedReader br = new BufferedReader(new InputStreamReader(in, "utf-8"));
                String strLine;
                while ((strLine = br.readLine()) != null) {

                    Vector data = Splitter.Split(strLine, "\t");

                    StringBuilder tm = new StringBuilder();

                    try {

                        String url = (String) data.get(1);

                        if (!Method.equals(url)) {
                            continue;
                        }

                        String query = (String) data.get(2);
                        if ("null".equals(query)) { // для совместимости со старым форматом записи логов
                            query = "";
                        }

                        // парсим параметры
                        HashMap pms = new HashMap<String, Object>();
                        HttpHelper.parseQuery(query, pms);

                        // если еще нет шапки таблицы - рожаем
                        if (!isTable) {
                            // определяем количество параметров, формируем список ключей
                            keys = (String[]) pms.keySet().toArray(new String[0]);
                            // создаем заголовок таблицы
                            tm.append("<table class=\"table table-striped table-bordered table-condensed\">");
                            tm.append("<thead>");
                            tm.append("<tr><th class=\"span1 right\">Время</th><th>Агент</th>");
                            for (String key : keys) {
                                tm.append("<th>");
                                tm.append(key);
                                tm.append("</th>");
                            }
                            tm.append("<th class=\"right\">Выполнение (мс)</th></tr>");
                            tm.append("</thead>");
                            tm.append("<tbody>");
                            isTable = true;
                        }

                        int executionTime = -1;
                        String userAgent = "";
                        if (data.size() > 4) {
                            String eTime = (String) data.get(4);
                            executionTime = Integer.parseInt(eTime.replace(" ms", "").trim());
                        }
                        if (data.size() > 3) {
                            userAgent = (String) data.get(3);
                        }
                        tm.append("<tr>");
                        tm.append("<td class=\"right\">");
                        tm.append(data.get(0));
                        tm.append("</td>");
                        tm.append("<td>");
                        tm.append(userAgent);
                        tm.append("</td>");

                        for (String key : keys) {
                            Object o = pms.get(key);
                            String val = o == null ? "" : (String) o;
                            tm.append("<td>");
                            tm.append(val); // todo: encode?
                            tm.append("</td>");
                        }

//                    sb.append("<td>");
//                    sb.append(query);
//                    sb.append("</td>");
                        tm.append("<td class=\"right\">");

                        if (executionTime >= (this.getJaxis().getKeepAliveTime() * 1000)) {
                            tm.append("<span class=\"label label-important\">" + executionTime + "</span>");
                        } else {
                            tm.append(executionTime);
                        }

                        tm.append("</td>");
                        tm.append("</tr>\n");

                        sb.append(tm.toString());

                    } catch (java.lang.RuntimeException eee) {
                        // TODO fix statistic logging unurlencoded params
                    }

                }
                in.close();

                // закрываем таблицу
                if (isTable) {
                    sb.append("</tbody>");
                    sb.append("</table>");
                }

            } catch (Exception ex) {
                throw new SecurityException("Случилось непоправимое. Подробнее: " + ex.toString());
            }
        }

        sb.append("</div>");

        sb.append("<script>\n");
        sb.append("$(document).ready(function(){\n");

        sb.append("$.datepicker.setDefaults( $.datepicker.regional[ \"ru\" ] );");
        sb.append("$('#datepicker').datepicker({dateFormat: 'yy.mm.dd'});\n");
//        sb.append("$('#datepicker').datepicker($.datepicker.regional['ru']);\n");
        sb.append("});\n");
        sb.append("</script>\n");

        sb.append("</body>");
        sb.append("</html>");

        return sb.toString();
    }

    /**
     * Форматирует кол-во милисекунд в кол-во суток - часов - минут - секунд
     *
     * @param time
     * @return
     */
    public static String longToTimeIntervalString(long time) {
        String result = "";
        time = time / 1000;
        int days = (int) Math.ceil(time / 86400);
        time -= days * 86400;
        int hours = (int) Math.ceil(time / 3600);
        time -= hours * 3600;
        int minutes = (int) Math.ceil(time / 60);
        time -= minutes * 60;
        int seconds = (int) Math.ceil(time);
        result = String.format("%04d %02d:%02d:%02d", days, hours, minutes, seconds);
        return result;
    }

    /**
     * Проверяет временный пароль
     *
     * @param TmpPasswd
     * @return
     */
    public static boolean checkTemporaryPassword(String TmpPasswd) {
        String passwd;
        try {
            passwd = new String(Base64.decode(TmpPasswd), "utf-8");
        } catch (IOException ex) {
            return false;
        }
        String time = "";
        for (int i = 1; i < 28; i = i + 2) {
            time += passwd.charAt(i);
        }
        String timeEnd = time.substring(0, 4);
        String timeBegin = time.substring(4, 14);
        String t = "";
        for (int i = timeBegin.length() - 1; i >= 0; i--) {
            t += timeBegin.charAt(i);
        }
        time = t + timeEnd;
        long tt = Long.parseLong(time);
        if (Math.abs(System.currentTimeMillis() - tt) <= 600000) {
            return true;
        }
        return false;
    }

    /**
     * Генерирует временный пароль
     *
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String getTemporaryPassword() throws UnsupportedEncodingException {
        String time = String.format("%014d", System.currentTimeMillis());
        // разворачиваем половину времени
        String timeBegin = time.substring(0, 10);
        String timeEnd = time.substring(10, 14);
        time = timeEnd;
        for (int i = timeBegin.length() - 1; i >= 0; i--) {
            time += timeBegin.charAt(i);
        }
        String passwd = "";
        // перемешиваем
        for (int i = 0; i < 14; i++) {
            char c = (char) (int) (Math.random() * 254);
            passwd += c;
            passwd += time.charAt(i);
        }
        passwd = Base64.encode(passwd.getBytes("utf-8"));
        return passwd;
    }
}
