package net.stemteam.jaxis.handler;

import com.sun.net.httpserver.Headers;
import net.stemteam.datatransport.exception.ColumnNotFoundException;
import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.jaxis.Jaxis;
import net.stemteam.jaxis.JaxisContentType;
import net.stemteam.jaxis.ParamValidator;
import net.stemteam.jaxis.UploadStore;
import net.stemteam.jaxis.UploadedFile;
import net.stemteam.jaxis.common.HtmlHelper;
import net.stemteam.jaxis.err.HttpInternalException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.net.InetSocketAddress;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import javax.sql.DataSource;
import net.stemteam.jaxis.err.NotConfiguredException;

public abstract class AtomHandler implements HttpHandler {

    /**
     * Кодировка в которой работает сервер
     */
    public static final String CHARSET = "UTF-8";

    // патерн для проверки json чи не
    protected static final Pattern jsonPattern = Pattern.compile(".+\\.json$");
    protected static final Pattern htmlPattern = Pattern.compile(".+\\.html$");
    protected static final Pattern json2Pattern = Pattern.compile(".+\\.json2$");
    protected static final Pattern jaxisPattern = Pattern.compile(".+\\.jaxis$");

    /**
     * Тип возвращаемого содержимого по умолчанию
     */
    protected static final String CONTENT_TYPE_DEFAULT = "text/plain;charset=utf-8";
    protected static final String CONTENT_TYPE_JSON = "application/json;charset=utf-8";
    protected static final String CONTENT_TYPE_HTML = "text/html;charset=utf-8";
    /**
     * Минимальная длина содержимого, при превышении которой происходит сжатие
     * GZIP если таковое поддерживается клиентом.
     */
    protected static final int GZIP_MIN_LENGTH = 1024;
    protected static final String ENCODING_GZIP = "gzip";
    protected static final String ENCODING_DEFLATE = "deflate";

    private final ParamValidator paramValidator = new ParamValidator(this);

    protected String remoteIp;

    // дополнительная информация о вызове
    protected String extLogInfo = null;

    /**
     * Получить дополнительную информацию о текущем вызове
     *
     * @return строка с дополнительной информацией
     */
    public String getExtLogInfo() {
        return extLogInfo;
    }

    public void setExtLogInfo(String extLogInfo) {
        this.extLogInfo = extLogInfo;
    }

    /**
     * Возвращает IP адрес клиента
     *
     * @return
     */
    public String getRemoteIp() {
        return remoteIp;
    }

    // ссылка на сервер, на котором зарегистрирован данный обработчик
    private Jaxis jaxis;

    public Jaxis getJaxis() {
        return jaxis;
    }

    public void setJaxis(Jaxis jaxis) {
        this.jaxis = jaxis;
    }

    private String requestUrl = "";

    /**
     * Возвращает URL запроса
     *
     * @return
     */
    public String getRequestUrl() {
        return requestUrl;
    }

    /**
     * Возвращает URL запроса без расширения .json и т.д.
     *
     * @return
     */
    public String getRequestUrlWithoutExt() {
        return requestUrl.replaceFirst("[.][^.]+$", "");
    }

    protected void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    /**
     * Формат контента по умолчанию
     */
    protected JaxisContentType contentType = JaxisContentType.DEFAULT;

    public JaxisContentType getContentType() {
        return contentType;
    }

    /**
     * Удаляет загруженные в UPLOAD файлы в данном контексте в случае ошибок в
     * handle
     *
     * @param params параметры http запроса
     */
    protected void deleteLoadedFiles(HashMap<String, Object> params) {
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            if (entry.getValue() instanceof UploadedFile) {
                UploadedFile f = (UploadedFile) entry.getValue();
                if (UploadStore.isFileExists(f.getGuid())) {
                    UploadStore.deleteFile(f.getGuid());
                }
            }
        }
    }

    /**
     * Определяет тип контента по эксченжу
     *
     * @param exchange
     * @return
     */
    protected JaxisContentType getContentType(HttpExchange exchange) {
        // проверяем, жсон чи ни
        if (json2Pattern.matcher(exchange.getRequestURI().getPath()).matches()) {
            return JaxisContentType.JSON2;
        } else if (jsonPattern.matcher(exchange.getRequestURI().getPath()).matches()) {
            return JaxisContentType.JSON;
        } else if (htmlPattern.matcher(exchange.getRequestURI().getPath()).matches()) {
            return JaxisContentType.HTML;
        } else if (jaxisPattern.matcher(exchange.getRequestURI().getPath()).matches()) {
            return JaxisContentType.JAXIS;
        }
        return JaxisContentType.DEFAULT;
    }

    /**
     * Возвращает список ошибок хэндлера
     *
     * @return
     */
    public ParamValidator getParamValidator() {
        return paramValidator;
    }

    /**
     * Эта клевая метода преобразует датасет в строку в зависимости от типа
     * контента
     *
     * @param ds
     * @param ct
     * @return
     * @throws HttpInternalException
     */
    public String getResponceAsString(DataSet ds, JaxisContentType ct) throws HttpInternalException {
        if (ds == null) {
            return null;
        }
        if (ct == JaxisContentType.JSON2) {
            try {
                return ds.toJson2();
            } catch (DataSetException ex) {
                throw new HttpInternalException(ex);
            }
        } else if (ct == JaxisContentType.JSON) {
            return ds.toJson();
        } else if (ct == JaxisContentType.HTML) {
            try {
                return HtmlHelper.getPageHeader(getRequestUrl()) + HtmlHelper.getDataSetAsHtmlTable(ds) + HtmlHelper.getPageFooter();
            } catch (ColumnNotFoundException ex) {
                throw new HttpInternalException(ex);
            }
        } else {
            return ds.toString();
        }
    }

    private DataSource dataSource;

    /**
     * Возвращает DataSource, связанный с данным хэндлером (в хэндлере можно
     * использовать для полученния соединения с БД)
     *
     * @return
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Связывает DataSource с хэндлером (в хэндлере можно использовать для
     * полученния соединения с БД)
     *
     * @param dataSource
     * @return
     */
    public AtomHandler setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        return this;
    }

    /**
     * Возвращает соединение из связанного с хэндлером DataSource
     *
     * @return
     * @throws SQLException
     * @throws NotConfiguredException
     */
    public Connection getConnection() throws SQLException, NotConfiguredException {
        if (dataSource == null) {
            throw new NotConfiguredException("DataSource not configured for this handler. Developers, please use setDataSource() to configure it.");
        }
        return dataSource.getConnection();
    }

    /**
     * Расковыривание IP адреса клиента из всяческих заголовков
     * @param headers
     * @param exchange
     * @return 
     */
    public static String extactIp(Headers headers, HttpExchange exchange) {
        // получаем IP адрес клиента
        String remoteIp = headers.getFirst("X-Forwarded-For");
        if (remoteIp == null || remoteIp.length() == 0 || "unknown".equalsIgnoreCase(remoteIp)) {
            remoteIp = headers.getFirst("Proxy-Client-IP");
        }
        if (remoteIp == null || remoteIp.length() == 0 || "unknown".equalsIgnoreCase(remoteIp)) {
            remoteIp = headers.getFirst("WL-Proxy-Client-IP");
        }
        if (remoteIp == null || remoteIp.length() == 0 || "unknown".equalsIgnoreCase(remoteIp)) {
            remoteIp = headers.getFirst("HTTP_CLIENT_IP");
        }
        if (remoteIp == null || remoteIp.length() == 0 || "unknown".equalsIgnoreCase(remoteIp)) {
            remoteIp = headers.getFirst("HTTP_X_FORWARDED_FOR");
        }
        if (remoteIp == null || remoteIp.length() == 0 || "unknown".equalsIgnoreCase(remoteIp)) {
            InetSocketAddress adr = exchange.getRemoteAddress();
            if (adr != null) {
                remoteIp = adr.getHostString();
            }
        }
        return remoteIp;
    }
}
