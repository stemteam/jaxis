package net.stemteam.jaxis.mail;

/**
 * Типы защищенности соединения
 *
 * Created by warmouse on 12.02.16.
 */
public enum MailConnectionSecurity {
    NONE(1, "NONE"),
    SSLTLS(2, "SSL/TLS"),
    STARTTLS(3, "STARTTLS");

    private final int id;
    private final String code;

    private MailConnectionSecurity(int id, String code) {
        this.id = id;
        this.code = code;
    }

    public static MailConnectionSecurity getByCode(String code) {
        MailConnectionSecurity[] arr$ = values();
        int len$ = arr$.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            MailConnectionSecurity v = arr$[i$];
            if(v.getCode().equals(code)) {
                return v;
            }
        }

        throw new IllegalArgumentException("No such element: " + code);
    }

    public String getCode() {
        return this.code;
    }
}
