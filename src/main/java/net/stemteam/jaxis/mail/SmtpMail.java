/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.mail;

import com.sun.mail.smtp.SMTPTransport;
import java.io.IOException;
import java.security.Security;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Шлет письма по SMTP
 *
 * Класс отмечен как более не поддерживаемый.
 * Теперь нужно использовать SmtpMailSender, он поддерживает SSL/TLS и STARTTLS соединения
 *
 */
@Deprecated
public class SmtpMail {
    
    /**
     * Шлет письмо без вложения через SMTP. Для отправки с вложением используйте
     * метод sendWithAttachments
     *
     * @param mailConfig
     * @param recipientEmail
     * @param ccEmail
     * @param title
     * @param message
     * @throws AddressException
     * @throws MessagingException
     */
    public static void send(MailConfig mailConfig, String recipientEmail, String ccEmail, String title, String message) throws AddressException, MessagingException {
        send(mailConfig.getHost(), mailConfig.getPort(), mailConfig.getLogin(), mailConfig.getPassword(), mailConfig.getReplyTo(), recipientEmail, ccEmail, title, message);        
    }
    
    /**
     * Шлет письмо c вложениями через SMTP
     * @param mailConfig
     * @param recipientEmail
     * @param ccEmail
     * @param title
     * @param message
     * @param attachFiles имена файлов вложений
     * @throws AddressException
     * @throws MessagingException
     * @throws IOException 
     */
    public static void sendWithAttachments(MailConfig mailConfig, String recipientEmail, String ccEmail, String title, String message, String... attachFiles) throws AddressException, MessagingException, IOException {
        sendWithAttachments(mailConfig.getHost(), mailConfig.getPort(), mailConfig.getLogin(), mailConfig.getPassword(), mailConfig.getReplyTo(), recipientEmail, ccEmail, title, message, attachFiles);
    }

    /**
     * Шлет письмо без вложения через SMTP. Для отправки с вложением используйте
     * метод sendWithAttachments
     *
     * @param smtpHost
     * @param smtpPort
     * @param username
     * @param password
     * @param replyTo
     * @param recipientEmail
     * @param ccEmail
     * @param title
     * @param message
     * @throws AddressException
     * @throws MessagingException
     */
    public static void send(String smtpHost, Integer smtpPort, String username, String password, String replyTo, String recipientEmail, String ccEmail, String title, String message) throws AddressException, MessagingException {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        
        Properties props = System.getProperties();
        props.setProperty("mail.smtps.host", smtpHost);
        props.setProperty("mail.smtps.port", smtpPort.toString());
        props.setProperty("mail.smtps.auth", "true");
        props.put("mail.smtps.quitwait", "false");
        
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", smtpPort.toString());

        Session session = Session.getInstance(props, null);

        // -- Create a new message --
        final MimeMessage msg = new MimeMessage(session);

        // -- Set the FROM and TO fields --
        msg.setFrom(new InternetAddress(username));

        // куда отвечать
        InternetAddress[] replyes = new InternetAddress[]{new InternetAddress(replyTo)};
        msg.setReplyTo(replyes);

        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));

        if ((ccEmail != null) && (ccEmail.length() > 0)) {
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
        }

        msg.setSubject(title, "utf-8");
        msg.setText(message, "utf-8");
        msg.setSentDate(new Date());

        SMTPTransport t = (SMTPTransport) session.getTransport("smtps");

        t.connect(smtpHost, smtpPort, username, password);
        t.sendMessage(msg, msg.getAllRecipients());
        t.close();
    }

    /**
     * Шлет письмо c вложениями через SMTP
     * @param smtpHost
     * @param smtpPort
     * @param username
     * @param password
     * @param replyTo
     * @param recipientEmail
     * @param ccEmail
     * @param title
     * @param message
     * @param attachFiles имена файлов вложений
     * @throws AddressException
     * @throws MessagingException
     * @throws IOException 
     */
    public static void sendWithAttachments(String smtpHost, Integer smtpPort, String username, String password, String replyTo, String recipientEmail, String ccEmail, String title, String message, String... attachFiles) throws AddressException, MessagingException, IOException {
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        
        Properties props = System.getProperties();
        props.setProperty("mail.smtps.host", smtpHost);
        props.setProperty("mail.smtp.port", smtpPort.toString());
        props.setProperty("mail.smtps.auth", "true");
        props.put("mail.smtps.quitwait", "false");
        
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", smtpPort.toString());

        Session session = Session.getInstance(props, null);
        final MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(username));
        InternetAddress[] replyes = new InternetAddress[]{new InternetAddress(replyTo)};
        msg.setReplyTo(replyes);
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));
        if ((ccEmail != null) && (ccEmail.length() > 0)) {
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
        }
        msg.setSubject(title, "utf-8");
        msg.setText(message, "utf-8");
        msg.setSentDate(new Date());

        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html; charset=utf-8"); // кодировка обязательна

        // creates multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        // adds attachments
        if (attachFiles != null && attachFiles.length > 0) {
            for (String filePath : attachFiles) {
                MimeBodyPart attachPart = new MimeBodyPart();
                attachPart.setDisposition(Part.ATTACHMENT);
                attachPart.setHeader("Content-Transfer-Encoding", "base64");
                attachPart.attachFile(filePath);
                multipart.addBodyPart(attachPart);
            }
        }

        // sets the multi-part as e-mail's content
        msg.setContent(multipart);

        SMTPTransport t = (SMTPTransport) session.getTransport("smtps");

        t.connect(smtpHost, smtpPort, username, password);
        t.sendMessage(msg, msg.getAllRecipients());
        t.close();
    }
    
    public static void main(String[] args) throws MessagingException {
        System.out.println("started");
        MailConfig mailConfig = new MailConfig();
        mailConfig.setHost("smtp.yandex.ru");
        mailConfig.setPort(465);
        mailConfig.setLogin("support@telemediclab.ru");
        mailConfig.setPassword("DCD1993IntoTheLabyr^");
        mailConfig.setName("support@telemediclab.ru");
        mailConfig.setReplyTo("noreply@telemediclab.ru");
        mailConfig.setSecurity(MailConnectionSecurity.NONE);
        
        SmtpMail.send(mailConfig, "andrey@infokinetika.ru", null, "preved", "preved emdveed");
        
        System.out.println("done");
    }
}
