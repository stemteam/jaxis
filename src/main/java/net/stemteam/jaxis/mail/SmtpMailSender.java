package net.stemteam.jaxis.mail;

import com.sun.net.ssl.internal.ssl.Provider;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.security.Security;
import java.util.Date;
import java.util.Properties;

/**
 * Вспомогательный класс для отправки писем по SMTP
 *
 * Пример использования:
 * SmtpMailSender mail = SmtpMailSender.getNewInstance(myMailConfig);
 * mail.send(recipient, copyTo, title, message);
 *
 * Created by warmouse on 12.02.2016.
 */
public class SmtpMailSender {
    protected final MailConfig config;
    protected Session SMTPSession = null;

    protected SmtpMailSender(MailConfig mailConfig) {
        config = mailConfig;
        SMTPSession = getSMTPSession(mailConfig);
    }

    /**
     * Создание экземпляра класса
     * @param mailConfig настройки SMTP
     * @return экземпляр MailHelper
     */
    public static SmtpMailSender getNewInstance(MailConfig mailConfig) {
        return new SmtpMailSender(mailConfig);
    }

    protected Transport getSMTPTransport() throws NoSuchProviderException {
        if (config.getSecurity() == MailConnectionSecurity.SSLTLS) {
            return SMTPSession.getTransport("smtps");
        }
        else {
            return SMTPSession.getTransport("smtp");
        }
    }

    /**
     * Создает SMTP сессию в зависимости от типа используемого шиврования соединения
     *
     * @param mailConfig
     * @return
     */
    protected static Session getSMTPSession(final MailConfig mailConfig) {
        Session session = null;
        switch (mailConfig.getSecurity()) {
            case SSLTLS: {
                Properties props = new Properties();
                props.setProperty("mail.smtps.host", mailConfig.getHost());
                props.setProperty("mail.smtps.port", mailConfig.getPort().toString());
                props.setProperty("mail.smtps.auth", "true");
                props.setProperty("mail.smtps.quitwait", "false");
                props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                props.setProperty("mail.smtp.socketFactory.fallback", "false");
                props.setProperty("mail.smtp.socketFactory.port", mailConfig.getPort().toString());

                session = Session.getInstance(props, null);
                break;
            }
            case STARTTLS: {
                Properties props = new Properties();
                props.put("mail.smtp.host", mailConfig.getHost());
                props.put("mail.smtp.port", mailConfig.getPort());
                props.put("mail.smtp.auth", true);
                props.put("mail.smtp.starttls.enable", "true");

                session = Session.getDefaultInstance(props, new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(mailConfig.getLogin(), mailConfig.getPassword());
                    }

                });
                break;
            }
            case NONE: {
                Properties props = new Properties();
                props.put("mail.smtp.host", mailConfig.getHost());
                props.put("mail.smtp.port", mailConfig.getPort());
                props.put("mail.smtp.auth", true);

                session = Session.getInstance(props, null);
                break;
            }
        }
        return session;
    }

    /**
     * Шлет письмо без вложения через SMTP. Для отправки с вложением используйте
     * метод sendWithAttachments
     *
     * @param recipientEmail
     * @param ccEmail
     * @param title
     * @param message
     * @throws MessagingException
     */
    public void send(String recipientEmail, String ccEmail, String title, String message) throws MessagingException {
        if (config.getSecurity() == MailConnectionSecurity.SSLTLS) {
            Security.addProvider(new Provider());
        }

        MimeMessage msg = new MimeMessage(SMTPSession);
        msg.setFrom(new InternetAddress(config.getName()));
        InternetAddress[] replyes = new InternetAddress[]{new InternetAddress(config.getReplyTo())};
        msg.setReplyTo(replyes);
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));
        if(ccEmail != null && ccEmail.length() > 0) {
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
        }

        msg.setSubject(title, "utf-8");
        msg.setText(message, "utf-8");
        msg.setSentDate(new Date());

        Transport transport = getSMTPTransport();
        transport.connect(config.getHost(), config.getPort(), config.getLogin(), config.getPassword());
        transport.sendMessage(msg, msg.getAllRecipients());
        transport.close();
    }

    /**
     * Шлет письмо c вложениями через SMTP
     *
     * @param recipientEmail
     * @param ccEmail
     * @param title
     * @param message
     * @param attachFiles
     * @throws MessagingException
     * @throws IOException
     */
    public void sendWithAttachments(String recipientEmail, String ccEmail, String title, String message, String... attachFiles) throws MessagingException, IOException {
        if (config.getSecurity() == MailConnectionSecurity.SSLTLS) {
            Security.addProvider(new Provider());
        }

        MimeMessage msg = new MimeMessage(SMTPSession);
        msg.setFrom(new InternetAddress(config.getName()));
        InternetAddress[] replyes = new InternetAddress[]{new InternetAddress(config.getReplyTo())};
        msg.setReplyTo(replyes);
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail, false));
        if(ccEmail != null && ccEmail.length() > 0) {
            msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(ccEmail, false));
        }

        msg.setSubject(title, "utf-8");
        msg.setText(message, "utf-8");
        msg.setSentDate(new Date());
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html; charset=utf-8");
        MimeMultipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        if(attachFiles != null && attachFiles.length > 0) {
            String[] t = attachFiles;
            int len$ = attachFiles.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                String filePath = t[i$];
                MimeBodyPart attachPart = new MimeBodyPart();
                attachPart.setDisposition("attachment");
                attachPart.setHeader("Content-Transfer-Encoding", "base64");
                attachPart.attachFile(filePath);
                multipart.addBodyPart(attachPart);
            }
        }

        msg.setContent(multipart);
        Transport transport = getSMTPTransport();
        transport.connect(config.getHost(), config.getPort(), config.getLogin(), config.getPassword());
        transport.sendMessage(msg, msg.getAllRecipients());
        transport.close();
    }
}

