/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.mail;

/**
 * Конфиг учетных записей почты
 */
public class MailConfig {
    private String name;
    private String host;
    private Integer port;
    private String login;
    private String password;
    private String replyTo;
    private MailConnectionSecurity security = MailConnectionSecurity.SSLTLS;

    public MailConfig() {
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if(name != null) {
            name = name.toLowerCase();
        }

        this.name = name;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return this.port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getLogin() {
        return this.login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getReplyTo() {
        return this.replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public MailConnectionSecurity getSecurity() {
        return security;
    }

    public void setSecurity(MailConnectionSecurity security) {
        this.security = security;
    }
}
