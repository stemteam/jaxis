/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.err;

import java.net.HttpURLConnection;
import net.stemteam.datatransport.transport.DataSet;

/**
 * HTTP INTERNAL ERROR
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class HttpInternalException extends HttpException {

    public HttpInternalException(Exception ex) {
        super(HttpURLConnection.HTTP_INTERNAL_ERROR, ex);
    }

    public HttpInternalException(String string) {
        super(HttpURLConnection.HTTP_INTERNAL_ERROR, string);
    }

    public HttpInternalException(DataSet errorDataSet) {
        super(HttpURLConnection.HTTP_INTERNAL_ERROR, errorDataSet);
    }

}
