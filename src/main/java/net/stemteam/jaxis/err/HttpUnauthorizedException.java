/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.err;

import java.net.HttpURLConnection;
import net.stemteam.datatransport.transport.DataSet;

/**
 * 401 HTTP UNAUTHORIZED
 */
public class HttpUnauthorizedException extends HttpException {

    public HttpUnauthorizedException(Exception ex) {
        super(HttpURLConnection.HTTP_UNAUTHORIZED, ex);
        this.setStackTrace(ex.getStackTrace());
    }

    public HttpUnauthorizedException(String userMessage) {
        super(HttpURLConnection.HTTP_UNAUTHORIZED, userMessage);
    }

    public HttpUnauthorizedException(DataSet errorDataSet) {
        super(HttpURLConnection.HTTP_UNAUTHORIZED, errorDataSet);
    }
}
