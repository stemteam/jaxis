/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.err;

import net.stemteam.datatransport.transport.DataSet;

/**
 * Базовое исключение. Содержит пользовательское сообщение, так же может содержать DataSet с формализованными данными
 * ошибки.
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class StemException extends Exception {

    protected String userMessage;
    protected DataSet errorDataSet;

    /**
     * Пользовательское сообщение
     *
     * @return
     */
    public String getUserMessage() {
        return userMessage;
    }

    /**
     * DataSet с формализованными данными ошибки
     *
     * @return
     */
    public DataSet getErrorDataSet() {
        return errorDataSet;
    }

    /**
     * Создает исключение
     *
     * @param ex
     * @param userMessage пользовательское сообщение
     */
    public StemException(Exception ex, String userMessage) {
        super(userMessage, ex);
        // применяем стэк вызова от переданной ошибки (а то непосредственно строка вызова исключения пропадет)
        this.setStackTrace(ex.getStackTrace());
        // пользовательская ошибка
        this.userMessage = userMessage;
        if (ex instanceof StemException) {
            this.errorDataSet = ((StemException) ex).getErrorDataSet();
        }
    }

    /**
     * Создает исключение
     *
     * @param userMessage сообщение, которое может быть отображено пользователю
     */
    public StemException(String userMessage) {
        super(userMessage);
        // пользовательская ошибка
        this.userMessage = userMessage;
    }

    /**
     * Создает исключение
     *
     * @param errorDataSet DataSet с формализованными данными ошибки
     */
    public StemException(DataSet errorDataSet) {
        super(errorDataSet == null ? "" : errorDataSet.toJson());
        this.errorDataSet = errorDataSet;
    }

    /**
     * Создает исключение
     *
     * @param ex
     */
    public StemException(Exception ex) {
        super(ex);
        if (ex instanceof StemException) {
            this.userMessage = ((StemException) ex).getUserMessage();
            this.errorDataSet = ((StemException) ex).getErrorDataSet();
        }
        // применяем стэк вызова от переданной ошибки (а то непосредственно строка вызова исключения пропадет)
        this.setStackTrace(ex.getStackTrace());
    }

}
