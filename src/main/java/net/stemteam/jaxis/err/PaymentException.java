/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.err;

import java.io.IOException;

/**
 * Ошибка оплаты
 */
public class PaymentException extends StemException {

    public PaymentException(String message) {
        super(message);
    }

    public PaymentException(IOException ex) {
        super(ex);
    }

    public PaymentException(Exception ex, String userMessage) {
        super(ex, userMessage);
    }

}
