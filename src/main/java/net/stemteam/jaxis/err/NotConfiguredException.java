/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.err;

/**
 * Ошибка: компонент сервиса не сконфигурирован
 */
public class NotConfiguredException extends StemException {

    public NotConfiguredException(String msg) {
        super(msg);
    }

}
