/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.err;

import java.net.HttpURLConnection;
import net.stemteam.datatransport.transport.DataSet;

/**
 * HTTP NOT FOUND ERROR
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class HttpNotFoundException extends HttpException {

    public HttpNotFoundException(String string) {
        super(HttpURLConnection.HTTP_NOT_FOUND, string);
    }

    public HttpNotFoundException(Exception ex) {
        super(HttpURLConnection.HTTP_NOT_FOUND, ex);
        // применяем стэк вызова от переданной ошибки (а то непосредственно строка вызова исключения пропадет)
        this.setStackTrace(ex.getStackTrace());
    }

    public HttpNotFoundException(DataSet errorDataSet) {
        super(HttpURLConnection.HTTP_NOT_FOUND, errorDataSet);
    }

}
