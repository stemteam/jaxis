/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.err;

import java.net.HttpURLConnection;
import net.stemteam.datatransport.transport.DataSet;

/**
 * HTTP ERROR
 */
public class HttpException extends StemException {

    protected int httpCode = HttpURLConnection.HTTP_INTERNAL_ERROR;

    public HttpException(int httpCode, Exception ex) {
        super(ex);
        if (ex instanceof HttpException) {
            this.httpCode = ((HttpException) ex).getHttpCode();
        } else {
            this.httpCode = httpCode;
        }
        // применяем стэк вызова от переданной ошибки (а то непосредственно строка вызова исключения пропадет)
        this.setStackTrace(ex.getStackTrace());
    }

    /**
     *
     * @param httpCode HTTP_CODE
     * @param ex
     * @param userMessage сообщение, которое может быть отображено пользователю
     */
    public HttpException(int httpCode, Exception ex, String userMessage) {
        super(ex, httpCode + ": " + userMessage);
        // применяем стэк вызова от переданной ошибки (а то непосредственно строка вызова исключения пропадет)
        this.setStackTrace(ex.getStackTrace());
        this.httpCode = httpCode;
        // пользовательская ошибка
        this.userMessage = userMessage;
    }

    /**
     *
     * @param httpCode HTTP_CODE
     * @param userMessage сообщение, которое может быть отображено пользователю
     */
    public HttpException(int httpCode, String userMessage) {
        super(httpCode + ": " + userMessage);
        this.httpCode = httpCode;
        this.userMessage = userMessage;
    }

    /**
     *
     * @param httpCode HTTP_CODE
     * @param errorDataSet дата сет с ошибкой другого сервиса в формате валидатора
     */
    public HttpException(int httpCode, DataSet errorDataSet) {
        super(httpCode + (errorDataSet == null ? "" : errorDataSet.toJson()));
        this.httpCode = httpCode;
        this.errorDataSet = errorDataSet;
    }

    /**
     * Возвращает HTTP код ошибки
     *
     * @return
     */
    public int getHttpCode() {
        return httpCode;
    }

}
