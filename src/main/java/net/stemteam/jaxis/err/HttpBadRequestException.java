/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.err;

import java.net.HttpURLConnection;
import net.stemteam.datatransport.transport.DataSet;

/**
 * HTTP BAD REQUEST ERROR
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class HttpBadRequestException extends HttpException {

    public HttpBadRequestException(Exception ex) {
        super(HttpURLConnection.HTTP_BAD_REQUEST, ex);
        // применяем стэк вызова от переданной ошибки (а то непосредственно строка вызова исключения пропадет)
        this.setStackTrace(ex.getStackTrace());
    }

    public HttpBadRequestException(String userMessage) {
        super(HttpURLConnection.HTTP_BAD_REQUEST, userMessage);
    }

    public HttpBadRequestException(DataSet errorDataSet) {
        super(HttpURLConnection.HTTP_BAD_REQUEST, errorDataSet);
    }

}
