/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.err;

import java.net.HttpURLConnection;
import net.stemteam.datatransport.transport.DataSet;

/**
 * HTTP FORBIDDEN ERROR
 */
public class HttpForbiddenException extends HttpException {

    public HttpForbiddenException(Exception ex) {
        super(HttpURLConnection.HTTP_FORBIDDEN, ex);
    }

    public HttpForbiddenException(String userMessage) {
        super(HttpURLConnection.HTTP_FORBIDDEN, userMessage);
    }

    /**
     * Исключение HTTP_FORBIDDEN
     *
     * @param ex
     * @param userMessage сообщение, которое может быть отображено пользователю
     */
    public HttpForbiddenException(Exception ex, String userMessage) {
        super(HttpURLConnection.HTTP_FORBIDDEN, ex, userMessage);
    }

    public HttpForbiddenException(DataSet errorDataSet) {
        super(HttpURLConnection.HTTP_FORBIDDEN, errorDataSet);
    }
}
