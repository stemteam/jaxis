/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis;

import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.datatransport.transport.StringEncoder;
import java.util.HashMap;

/**
 * Проверялка параметров
 */
@Deprecated
public class ParamChecker {

    public static Integer getInteger(HashMap params, String paramName) throws IllegalArgumentException {
        return getInteger(params, paramName, false);
    }

    public static Integer getInteger(HashMap params, String paramName, boolean checkNotNull) throws IllegalArgumentException {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            throw new IllegalArgumentException("Параметр " + paramName + " пуст.");
        }
        try {
            Integer i = o == null ? null : Integer.parseInt((String) o);
            return i;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Параметр " + paramName + " не соответствует формату Integer.");
        }
    }

    public static Double getDouble(HashMap params, String paramName) throws IllegalArgumentException {
        return getDouble(params, paramName, false);
    }

    /**
     * Возвращает значение параметра из карты параметров
     * @param params HashMap параметров
     * @param paramName String название параметра
     * @param checkNotNull boolean проверять ли значение параметра на непустоту
     * @return
     * @throws IllegalArgumentException 
     */
    public static Double getDouble(HashMap params, String paramName, boolean checkNotNull) throws IllegalArgumentException {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            throw new IllegalArgumentException("Параметр " + paramName + " пуст.");
        }
        try {
            Double i = o == null ? null : Double.parseDouble((String) o);
            return i;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Параметр " + paramName + " не соответствует формату Double.");
        }
    }

    /**
     * Возвращает значение строкового параметра из карты параметров
     * @param params HashMap параметров
     * @param paramName String название параметра
     * @param checkNotNull boolean проверять ли значение параметра на непустоту
     * @return 
     */
    public static String getString(HashMap params, String paramName, boolean checkNotNull) throws IllegalArgumentException {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            throw new IllegalArgumentException("Параметр " + paramName + " пуст.");
        }
        return (String) o;
    }

    /**
     * Возвращает значение строкового параметра из карты параметров
     * @param params HashMap параметров
     * @param paramName String название параметра
     * @return 
     */
    public static String getString(HashMap params, String paramName) throws IllegalArgumentException {
        return getString(params, paramName, false);
    }

    /**
     * Возвращает значение DateTime параметра из карты параметров
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     * @throws IllegalArgumentException 
     */
    public static java.sql.Timestamp getDateTime(HashMap params, String paramName, boolean checkNotNull) throws IllegalArgumentException {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            throw new IllegalArgumentException("Параметр " + paramName + " пуст.");
        }
        try {
            java.sql.Timestamp ts = StringEncoder.decodeDateTime((String) o);
            return ts;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Параметр " + paramName + " не соответствует формату DateTime.");
        }
    }

    /**
     * Возвращает значение DateTime параметра из карты параметров
     * @param params
     * @param paramName
     * @return
     * @throws IllegalArgumentException 
     */
    public static java.sql.Timestamp getDateTime(HashMap params, String paramName) throws IllegalArgumentException {
        return getDateTime(params, paramName, false);
    }

    /**
     * Возвращает дата-сет из карты параметров
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     * @throws IllegalArgumentException 
     */
    public static DataSet getDataSet(HashMap params, String paramName, boolean checkNotNull) throws IllegalArgumentException {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            throw new IllegalArgumentException("Параметр " + paramName + " пуст.");
        }
        try {
            DataSet dataSet = new DataSet();
            dataSet.UnPackData((String) o);
            return dataSet;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Параметр " + paramName + " не соответствует формату DataSet");
        }
    }

    /**
     * Возвращает дата-сет из карты параметров
     * @param params карта параметров
     * @param paramName название параметра
     * @return DataSet
     * @throws IllegalArgumentException 
     */
    public static DataSet getDataSet(HashMap params, String paramName) throws IllegalArgumentException {
        return getDataSet(params, paramName, false);
    }

    /**
     * Возвращает датасет из JSON представления
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return 
     */
    public static DataSet getDataSetFromJson(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            throw new IllegalArgumentException("Параметр " + paramName + " пуст.");
        }
        try {
            DataSet ds = DataSet.fromJson((String) o);
            return ds;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Параметр " + paramName + " не соответствует формату DataSet (JSON)");
        }
    }

    /**
     * Возвращает датасет из JSON представления
     * @param params
     * @param paramName
     * @return 
     */
    public static DataSet getDataSetFromJson(HashMap params, String paramName) {
        return getDataSetFromJson(params, paramName, false);
    }

    /**
     * Возвращает значение TimeStamp параметра из карты параметров
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     * @throws IllegalArgumentException 
     */
    public static java.sql.Timestamp getTimeStamp(HashMap params, String paramName, boolean checkNotNull) throws IllegalArgumentException {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            throw new IllegalArgumentException("Параметр " + paramName + " пуст.");
        }
        try {
            java.sql.Timestamp ts = StringEncoder.decodeTimeStamp((String) o);
            return ts;
        } catch (Exception ex) {
            throw new IllegalArgumentException("Параметр " + paramName + " не соответствует формату DateTime.");
        }
    }

    /**
     * Возвращает значение TimeStamp параметра из карты параметров
     * @param params
     * @param paramName
     * @return
     * @throws IllegalArgumentException 
     */
    public static java.sql.Timestamp getTimeStamp(HashMap params, String paramName) throws IllegalArgumentException {
        return getTimeStamp(params, paramName, false);
    }
}
