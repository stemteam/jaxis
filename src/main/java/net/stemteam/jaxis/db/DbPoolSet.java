/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.db;

import net.stemteam.datatransport.exception.ConnectionPoolNotFoundException;
import java.util.Vector;

/**
 * Набор пулов соединений с БД
 */
public class DbPoolSet {

    private volatile Vector<AbstractDbPool> dbPools;

    /**
     * Возвращает все пулы
     *
     * @return
     */
    public synchronized Vector<AbstractDbPool> getDbPools() {
        return dbPools;
    }

    /**
     * Добавляет пул в набор
     *
     * @param pool
     */
    public synchronized void addPool(AbstractDbPool pool) {
        dbPools.add(pool);
    }
    
    /**
     * Возвращает пулл по идентификатору
     *
     * @param name уникальное наименование БД
     * @return
     */
    public synchronized AbstractDbPool getPoolById(String name) throws ConnectionPoolNotFoundException {
        for (AbstractDbPool pool : dbPools) {
            if (pool.getDbConfig().getName().equals(name)) {
                return pool;
            }
        }
        // не нашли - родим исключение
        throw new ConnectionPoolNotFoundException("Пул с именем " + name + " не найден.");
    }

    /**
     * Возвращает первый пул указанных типов
     *
     * @param dbKinds типы пулов по назначению БД
     * @param dbkind 
     * @return
     * @throws ConnectionPoolNotFoundException
     */
    public synchronized AbstractDbPool getFirstPoolByDbKind(DbKind... dbKinds) throws ConnectionPoolNotFoundException {
        for (AbstractDbPool pool : dbPools) {
            for (DbKind dbKind : dbKinds) {
                if (pool.getDbConfig().getDbkind() == dbKind) {
                    return pool;
                }
            }
        }
        // не нашли - родим исключение
        StringBuilder sb = new StringBuilder();
        for (DbKind dbKind : dbKinds) {
            sb.append(dbKind.getName()).append(", ");
        }
        throw new ConnectionPoolNotFoundException("Пул с типами " + sb.toString() + " не найден.");
    }

    /**
     * Возвращает пул соединений по умолчанию (он первый в конфиге)
     *
     * @return
     */
    public synchronized AbstractDbPool getDefaultPool() {
        return dbPools.firstElement();
    }

    /**
     * Конструктор (если кто не догадался)
     */
    public DbPoolSet() {
        dbPools = new Vector<>();
    }

    public static DbPoolSet getInstance() {
        return DbPoolSetHolder.INSTANCE;
    }

    private static class DbPoolSetHolder {

        private static final DbPoolSet INSTANCE = new DbPoolSet();
    }
}
