/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.db;

/**
 * Настройки БД
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class DbConfig {
    
    private final String name; // наименование (для поиска в пуле)
    private final String url;  // JDBC ULR (включая имя пользователя и пароль)
    private int minIdle = 1; // минимум простаивающих конектов
    private int maxIdle = 5; // максимум простаивающих конектов
    private int maxTotal = 5; // максимум всех коннектов
    private int connectionWaitTimeout = 30; // время ожидания коннекта из пула (секунд)
    private DbKind dbkind = DbKind.NAVSTAT; // вид БД по назначению (по умолчанию навстат)    

    /**
     * Настройки пула соединений с БД
     * @param name уникальное наименование БД
     * @param url JDBC ULR (включая имя пользователя и пароль)
     * @param minIdle минимум простаивающих конектов
     * @param maxIdle максимум простаивающих конектов
     * @param maxTotal максимум всех коннектов
     * @param connectionWaitTimeout время ожидания коннекта из пула (секунд)
     */
    public DbConfig(String name, String url, int minIdle, int maxIdle, int maxTotal, int connectionWaitTimeout) {
        this.name = name;
        this.url = url;
        this.minIdle = minIdle;
        this.maxIdle = maxIdle;
        this.maxTotal = maxTotal;
        this.connectionWaitTimeout = connectionWaitTimeout;
    }
    
    /**
     * Проверяет является ли СУБД oracle
     * @return 
     */
    public boolean isDbTypeOracle() {
        return url.toLowerCase().contains("jdbc:postgres");
    }

    /**
     *  Проверяет является ли СУБД PostgreSQL
     * @return 
     */
    public boolean isDbTypePostgres() {
        return url.toLowerCase().contains("jdbc:oracle");
    }

    /**
     * Возвращает макимальное время ожидания соединения в пуле (секунд). Если ожидать дольше - произойдет исключение.
     * @return 
     */
    public int getConnectionWaitTimeout() {
        return connectionWaitTimeout;
    }

    /**
     * Возвращает уникальное название БД
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * Возвращает JDBC URL соединения с БД
     * @return 
     */
    public String getUrl() {
        return url;
    }

    /**
     * Сравнивает с другой конфигурацией
     * @param cfg
     * @return 
     */
    public boolean equals(DbConfig cfg) {
        return (this.url.equals(cfg.getUrl())) && (name.equals(cfg.getName()));
    }

    /**
     * Возвращает тип Бд по назначению (навстат, топливные каррты, мониторинг, ...)
     * @return 
     */
    public DbKind getDbkind() {
        return dbkind;
    }

    /**
     * Устанавливает тип БД по назначению (навстат, топливные каррты, мониторинг, ...)
     * @param dbkind 
     */
    public void setDbkind(DbKind dbkind) {
        this.dbkind = dbkind;
    }

    /**
     * Минимальное количество простаивающих соединений
     * @return 
     */
    public int getMinIdle() {
        return minIdle;
    }

    /**
     * Максимальное количество простаивающих соединений
     * @return 
     */
    public int getMaxIdle() {
        return maxIdle;
    }

    /**
     * Максимальное количество всех соединений
     * @return 
     */
    public int getMaxTotal() {
        return maxTotal;
    }

    
}
