/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.db;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp2.ConnectionFactory;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import javax.sql.DataSource;
import org.apache.commons.configuration.ConfigurationRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Database Connection Pool with dominoes and cleaners
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class AbstractDbPool {

    private static Logger logger = LoggerFactory.getLogger(AbstractDbPool.class);
    private DbConfig dbConfig;
    protected DataSource dataSource;

    /**
     * Конструктор без DbConfig, подразумевает вызов setDataSource() для
     * дальнейшей работы
     */
    public AbstractDbPool() {
    }

    /**
     * Возвращает конфиг для текущего пула (или null если не установлен)
     *
     * @return
     */
    public DbConfig getDbConfig() {
        return this.dbConfig;
    }

    /**
     * Закрытие соединения с БД
     *
     * @param conn
     * @throws SQLException
     */
    public synchronized void closeConnection(Connection conn)
            throws SQLException {
        if (conn != null) {
            conn.close();
        }
    }

    /**
     * Создает пулл, инициирует DataSource настройками DBConfig
     *
     * @param dbConfig настройки пула
     */
    public AbstractDbPool(DbConfig dbConfig) {
        this.dbConfig = dbConfig;
        // создаем датасорс через процедуру создания DBCP2
        dataSource = createDbcpDataSource(dbConfig);
    }

    /**
     * Запрос соединения с БД
     *
     * @return
     * @throws SQLException
     */
    public Connection getConnection() throws SQLException {
        // не люблю NullPointerException
        if (dataSource == null) {
            throw new SQLException("Pool has no DataSource, please set it through setDataSource()");
        }
        Connection conn = getDataSource().getConnection();
        if (conn.getAutoCommit()) {
            conn.setAutoCommit(false);
        }
        return conn;
    }

    /**
     * Формирует DataSource посредством Apache DBCP2 и конфига
     *
     * @param dbconfig
     * @return
     */
    public static javax.sql.DataSource createDbcpDataSource(DbConfig dbconfig) {
        // http://jdbc.postgresql.org/documentation/head/connect.html

        // First, we'll create a ConnectionFactory that the
        // pool will use to create Connections.
        // We'll use the DriverManagerConnectionFactory,
        // using the connect string passed in the command line
        // arguments.
        //
        ConnectionFactory connectionFactory
                = new DriverManagerConnectionFactory(dbconfig.getUrl(), null);

        // Next we'll create the PoolableConnectionFactory, which wraps
        // the "real" Connections created by the ConnectionFactory with
        // the classes that implement the pooling functionality.
        //
        PoolableConnectionFactory poolableConnectionFactory
                = new PoolableConnectionFactory(connectionFactory, null);

        // создаем конифг пула
        GenericObjectPoolConfig x = new GenericObjectPoolConfig();
        x.setMaxTotal(dbconfig.getMaxTotal());
        x.setMinIdle(dbconfig.getMinIdle());
        x.setMaxIdle(dbconfig.getMaxIdle());
        // максимальное время ожидания свободного соединения в пуле до того 
        // момента когда произойдет ошибка получения соединения (-1 означает 
        // ожидать бесконечно)
        x.setMaxWaitMillis(dbconfig.getConnectionWaitTimeout() * 1000L); 

        // изоляцию зададим явно
        poolableConnectionFactory.setDefaultTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        //poolableConnectionFactory.setDefaultQueryTimeout(Integer.MIN_VALUE);
        
        // Now we'll need a ObjectPool that serves as the
        // actual pool of connections.
        //
        // We'll use a GenericObjectPool instance, although
        // any ObjectPool implementation will suffice.
        ObjectPool<PoolableConnection> connectionPool
                = new GenericObjectPool(poolableConnectionFactory, x);
                
        // Set the factory's pool property to the owning pool
        poolableConnectionFactory.setPool(connectionPool);

        // вырубаем автокоммит
        poolableConnectionFactory.setDefaultAutoCommit(Boolean.FALSE);

        // Finally, we create the PoolingDriver itself,
        // passing in the object pool we created.
        PoolingDataSource<PoolableConnection> ds
                = new PoolingDataSource(connectionPool);
        logger.info("Created connection pool for {}", dbconfig.getName());  
        return ds;
    }

    /**
     * Возвращает DataSource
     *
     * @return
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Устанавливает DataSource. DbConfig должен быть не установлен.
     *
     * @param dataSource
     */
    public void setDataSource(DataSource dataSource) {
        if (this.dbConfig != null) {
            ConfigurationRuntimeException ex = new ConfigurationRuntimeException("Db Config is set for current instance pool.");
            logger.error("", ex);
            throw ex;
        } else if (this.dataSource != null) {
            ConfigurationRuntimeException ex = new ConfigurationRuntimeException("Data Source for current instance pool was set earlier.");
            logger.error("", ex);
            throw ex;
        } else {
            this.dataSource = dataSource;
        }
    }
}
