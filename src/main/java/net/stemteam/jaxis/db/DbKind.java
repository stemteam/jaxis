/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.db;

/**
 * Тип базы данных по назначению (навстат, топливные карты и т.д.)
 */
public enum DbKind {

    NAVSTAT(1, "navstat", "Навстат"),
    FUELCARD(2, "fuelcard", "Топливные карты"),
    MONITORING(3, "monitoring", "Мониторинг"),
    CRAFT(4, "craft", "Крафт"),;

    private final int id;
    private final String code;
    private final String name;

    DbKind(int id, String code, String name) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    /**
     * Возвращает название типа БД
     *
     * @return
     */
    public String getName() {
        return name;
    }
   
    /**
     * Доступ к элементу по коду
     * @param code код (navstat, fuelcard, monitoring, craft)
     * @return 
     */
    public static DbKind getByCode(String code) {
        for (DbKind v : values()) {
            if (v.getCode().equals(code)) {
                return v;
            }
        }
        return null;
    }

    /**
     * Возвращает код
     * @return 
     */
    public String getCode() {
        return code;
    }
    
    
}
