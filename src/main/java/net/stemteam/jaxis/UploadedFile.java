/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.jaxis;

import org.apache.commons.io.FilenameUtils;

/**
 * Файловый тип данных. Используется в HashMap параметров запроса в Handler-ах, 
 * для обработки загруженных файлов.
 */
public class UploadedFile {
    
    private final String guid;
    private final String fileName;
    private final String originalName;
    
    public UploadedFile(String guid, String fileName, String originalName) {
        this.guid = guid;
        this.fileName = fileName;
        this.originalName = originalName;
    }

    /**
     * Возвращает идентификатор в хранилище
     * @return 
     */
    public String getGuid() {
        return guid;
    }
    
    /**
     * Возвращает имя файла в локальной файловой системе (GUID + EXT)
     * @return 
     */
    public String getFileName() {
        return fileName;
    }
    
    /**
     * Возвращает расширение файла
     * @return 
     */
    public String getFileExt() {
        return FilenameUtils.getExtension(fileName);
    }

    /**
     * Оригинальное имя файла (то, с которым загружался)
     * @return 
     */
    public String getOriginalName() {
        return originalName;
    }
    
    
    
}
