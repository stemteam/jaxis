package net.stemteam.jaxis;

import net.stemteam.jaxis.handler.RobotsHandler;
import net.stemteam.jaxis.handler.StaticHandler;

import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;
import net.stemteam.jaxis.handler.AtomHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Jaxis HTTP server
 */
public class Jaxis {

    // HTTP params
    private boolean httpEnabled = true;
    private HttpServer httpServer;
    private int httpPort;

    // HTTPS params
    private boolean httpsEnabled = false;
    private int httpsPort = 0;
    private String httpsAlgorithm = "TLS"; // см. http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#SSLContext
    private String httpsKeystoreFilename = "jaxis.jks";
    private String httpsKeystorePassword = "jaxisjaxis";
    private HttpsServer httpsServer;
    private boolean httpStarted = false;
    private boolean httpsStarted = false;

    // bouth
    private int corePoolSize;
    private int maximumPoolSize;
    private int blockingQueueSize;
    private int keepAliveTime;
    private String staticNamespace = "/static/";
    private String configFilePath = "properties.xml";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Устанавливает пространство имен для статики (по умолчанию /static/)
     *
     * @param staticNamespace
     */
    public void setStaticNamespace(String staticNamespace) {
        this.staticNamespace = staticNamespace;
    }

    /**
     * Возвращает путь к файлу настроек
     *
     * @return
     */
    public String getConfigFilePath() {
        return configFilePath;
    }

    /**
     * Устанавливает путь к файлу настроек
     *
     * @param configFilePath
     */
    public void setConfigFilePath(String configFilePath) {
        this.configFilePath = configFilePath;
    }

    /**
     * Возвращает пространство имен для ститики (по умолчанию /static/)
     *
     * @return
     */
    public String getStaticNamespace() {
        return staticNamespace;
    }

    /**
     * Инициализация сервера для обработки
     *
     * @param httpPort порт для прослушивания запросов
     * @param httpThreadPoolCoreSize кол-во невыгружаемых потоков в пуле обработки запрососв
     * @param httpThreadPoolMaxSize максимальное кол-во потоков в пуле обработки запросов
     * @param keepAliveTime время поддержки простаивающего соединения (секунд)
     * @param blockingQueueSize размер стэка обработки
     * @throws IOException
     * @deprecated Следует использовать сеттеры для параметров и затем вызов startHttp()
     */
    @Deprecated
    public void init(int httpPort, int httpThreadPoolCoreSize,
            int httpThreadPoolMaxSize, int keepAliveTime, int blockingQueueSize)
            throws IOException {

        this.httpPort = httpPort;
        this.corePoolSize = httpThreadPoolCoreSize;
        this.maximumPoolSize = httpThreadPoolMaxSize;
        this.keepAliveTime = keepAliveTime;
        this.blockingQueueSize = blockingQueueSize;
        startHttp();
    }

    /**
     * Запуск HTTP сервера
     *
     * @throws IOException
     */
    public void startHttp()
            throws IOException {
        if (!httpEnabled) {
            logger.warn("HTTP server disabled, but startHttp() called");
            return;
        }
        if (httpStarted) {
            logger.warn("HTTP server started, but startHttp() called");
            return;
        }

        // создаем HTTP сервер
        InetSocketAddress address = new InetSocketAddress(httpPort);
        httpServer = HttpServer.create(address, 0);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                corePoolSize, maximumPoolSize, keepAliveTime,
                TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(
                        blockingQueueSize));
        httpServer.setExecutor(executor);

        // создаем патч для складывания статики
        checkAndCreateDirs();

        // тут же создаем статический контекст
        StaticHandler h = new StaticHandler();
        h.setJaxis(this);
        httpServer.createContext(getStaticNamespace(), h);

        // тут же создаем контекст для поисковых ботов robots.txt
        httpServer.createContext("/robots.txt", new RobotsHandler());

        // отключение проверки обновления планировщика
        System.setProperty("org.terracotta.quartz.skipUpdateCheck", "true");

        // запускаем
        httpServer.start();
        httpStarted = true;
        logger.info("HTTP server started on {}", httpPort);
    }

    /**
     * Создает необходимые директории
     */
    private void checkAndCreateDirs() {
        // если нет папки для статики - создаем
        File staticPath = new File(StaticHandler.STATIC_PATH);
        if (!staticPath.exists()) {
            staticPath.mkdirs();
        }

        // если нет папки для аплоада файлов
        File uploadPath = new File(UploadStore.getUploadPath());
        if (!uploadPath.exists()) {
            uploadPath.mkdirs();
        }
    }

    /**
     * Запускает HTTPS сервер
     *
     * @throws java.io.IOException
     * @throws java.security.NoSuchAlgorithmException
     * @throws java.security.KeyStoreException
     * @throws java.security.cert.CertificateException
     * @throws java.security.UnrecoverableKeyException
     * @throws java.security.KeyManagementException
     */
    public void startHttps() throws IOException, NoSuchAlgorithmException, KeyStoreException, CertificateException,
            UnrecoverableKeyException, KeyManagementException {
        if (!httpsEnabled) {
            logger.warn("HTTPS server disabled, but startHttps() called");
            return;
        }
        if (httpsStarted) {
            logger.warn("HTTPS server started, but startHttps() called");
            return;
        }
        InetSocketAddress address = new InetSocketAddress(httpsPort);
        httpsServer = HttpsServer.create(address, 0);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                corePoolSize, maximumPoolSize, keepAliveTime,
                TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(
                        blockingQueueSize));
        httpsServer.setExecutor(executor);

        SSLContext sslContext = SSLContext.getInstance(httpsAlgorithm);

        char[] password = httpsKeystorePassword.toCharArray();
        KeyStore ks = KeyStore.getInstance("JKS");
        FileInputStream fis = new FileInputStream(httpsKeystoreFilename);
        ks.load(fis, password);

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, password);

        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(ks);

        // setup the HTTPS context and parameters
        sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        httpsServer.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
            @Override
            public void configure(HttpsParameters params) {
                try {
                    // initialise the SSL context
                    SSLContext c = getSSLContext();

                    SSLEngine engine = c.createSSLEngine();

                    params.setNeedClientAuth(false);
                    params.setCipherSuites(engine.getEnabledCipherSuites());
                    params.setProtocols(engine.getEnabledProtocols());

                    // get the default parameters
                    SSLParameters defaultSSLParameters = c.getDefaultSSLParameters();
                    params.setSSLParameters(defaultSSLParameters);
                } catch (Exception ex) {
                    logger.error("Failed to create HTTPS port", ex);
                }
            }
        });

        // создаем патч для складывания статики
        checkAndCreateDirs();

        // тут же создаем статический контекст
        StaticHandler h = new StaticHandler();
        h.setJaxis(this);
        httpsServer.createContext(getStaticNamespace(), h);

        httpsServer.start();
        httpsStarted = true;
        logger.info("HTTPS server started on {}", httpsPort);
    }

    /**
     * Создание контекста обработки (в HTTP и HTTPS сразу). Для индивидуального создания контекстов следует использовать
     * createContextHttp или createContextHttps
     *
     * @param url URL запросов, который будет обрабатывать данный хэндлер
     * @param handler хэндлер
     */
    public void createContext(String url, AtomHandler handler) {
        createContextHttp(url, handler);
        createContextHttps(url, handler);
    }

    /**
     * Создание контекста HTTP
     *
     * @param url URL запросов, который будет обрабатывать данный хэндлер
     * @param handler хэндлер
     */
    public void createContextHttp(String url, AtomHandler handler) {
        if (httpStarted) {
            httpServer.createContext(url, handler);
            logger.info("Created HTTP context for {}", url);
        }
        handler.setJaxis(this);
    }

    /**
     * Создание контекста HTTPS
     *
     * @param url URL запросов, который будет обрабатывать данный хэндлер
     * @param handler хэндлер
     */
    public void createContextHttps(String url, AtomHandler handler) {
        if (httpsStarted) {
            httpsServer.createContext(url, handler);
            logger.info("Created HTTPS context for {}", url);
        }
        handler.setJaxis(this);
    }

    /**
     * Удаление контекста обработки для заданного урла. Удаляет контектс для HTTP и HTTPS сразу. Для индивидуального
     * удаления следует использовать removeContextHttp или removeContextHttps
     *
     * @param url
     */
    public void removeContext(String url) {
        removeContextHttp(url);
        removeContextHttps(url);
    }

    /**
     * Удаление конткеста обработки урла HTTP
     *
     * @param url
     */
    public void removeContextHttp(String url) {
        try {
            if (httpStarted) {
                httpServer.removeContext(url);
                logger.info("Removed HTTP context for {}", url);
            }
        } catch (RuntimeException ex) {
            logger.warn("", ex);
        }
    }

    /**
     * Удаление конткеста обработки урла HTTPS
     *
     * @param url
     */
    public void removeContextHttps(String url) {
        try {
            if (httpsStarted) {
                httpsServer.removeContext(url);
                logger.info("Removed HTTPS context for {}", url);
            }
        } catch (RuntimeException ex) {
            logger.warn("", ex);
        }
    }

    public int getKeepAliveTime() {
        return keepAliveTime;
    }

    /**
     * Возвращает включен ли защищенный транспорт
     *
     * @return
     */
    public boolean isHttpsEnabled() {
        return httpsEnabled;
    }

    /**
     * Включает/отключает защищенный транспорт
     *
     * @param httpsEnabled
     */
    public void setHttpsEnabled(boolean httpsEnabled) {
        this.httpsEnabled = httpsEnabled;
    }

    /**
     * Возвращает алгоритм
     *
     * @return
     */
    public String getHttpsAlgorithm() {
        return httpsAlgorithm;
    }

    /**
     * Устанавливает алгоритм (по умолчанию TLS, см.
     * http://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#SSLContext)
     *
     * @param httpsAlgorithm
     */
    public void setHttpsAlgorithm(String httpsAlgorithm) {
        this.httpsAlgorithm = httpsAlgorithm;
    }

    /**
     * Возвращает путь к хранилищу ключей и сертификатов HTTPS
     *
     * @return httpsKeystoreFilename
     */
    public String getHttpsKeystoreFilename() {
        return httpsKeystoreFilename;
    }

    /**
     * Устанавливает путь к хранилищу ключей и сертификатов HTTPS
     *
     * @param httpsKeystoreFilename
     */
    public void setHttpsKeystoreFilename(String httpsKeystoreFilename) {
        this.httpsKeystoreFilename = httpsKeystoreFilename;
    }

    /**
     * Устанавливает ключ к хранилищу ключей и сертификатов HTTPS
     *
     * @param httpsKeystorePassword
     */
    public void setHttpsKeystorePassword(String httpsKeystorePassword) {
        this.httpsKeystorePassword = httpsKeystorePassword;
    }

    /**
     * Возвращает порт обработки HTTPS
     *
     * @return
     */
    public int getHttpsPort() {
        return httpsPort;
    }

    /**
     * Устанавливает порт обработки HTTPS
     *
     * @param httpsPort
     */
    public void setHttpsPort(int httpsPort) {
        this.httpsPort = httpsPort;
    }

    public int getCorePoolSize() {
        return corePoolSize;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    public void setMaximumPoolSize(int maximumPoolSize) {
        this.maximumPoolSize = maximumPoolSize;
    }

    public int getBlockingQueueSize() {
        return blockingQueueSize;
    }

    public void setBlockingQueueSize(int blockingQueueSize) {
        this.blockingQueueSize = blockingQueueSize;
    }

    public void setKeepAliveTime(int keepAliveTime) {
        this.keepAliveTime = keepAliveTime;
    }

    public int getHttpPort() {
        return httpPort;
    }

    public void setHttpPort(int httpPort) {
        this.httpPort = httpPort;
    }

    /**
     * Останавливает HTTP сервер. Если не запущен исключение не вызывается.
     */
    public void stopHttp() {
        if (httpStarted) {
            httpServer.stop(0);
        }
    }

    /**
     * Останавливает HTTPS сервер. Если не запущен исключение не вызывается.
     */
    public void stopHttps() {
        if (httpsStarted) {
            httpsServer.stop(0);
        }
    }

    public void setHttpEnabled(boolean httpEnabled) {
        this.httpEnabled = httpEnabled;
    }
}
