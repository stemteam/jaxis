/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.payment;

import java.util.Vector;
import net.stemteam.jaxis.err.HttpNotFoundException;

/**
 * Менеджер платежных систем
 */
public class PaymentSystemManager {
    
    private final Vector<PaymentSystem> paymentSystems = new Vector<>();
    
    private PaymentSystemManager() {
    }
    
    public static PaymentSystemManager getInstance() {
        return PaymentSystemManagerHolder.INSTANCE;
    }
    
    private static class PaymentSystemManagerHolder {
        private static final PaymentSystemManager INSTANCE = new PaymentSystemManager();
    }
    
    /**
     * Добавить платежную сстему в набор
     * @param system 
     */
    public void add(PaymentSystem system) {
        paymentSystems.add(system);
    }
    
    /**
     * Получить платежную систему по типу
     * @param type
     * @return 
     * @throws net.stemteam.jaxis.err.HttpNotFoundException если не удалось найти
     */
    public PaymentSystem get(PaymentSystemType type) throws HttpNotFoundException {
        for (PaymentSystem sys : paymentSystems) {
            if (sys.getType() == type) {
                return sys;
            }
        }
        throw new HttpNotFoundException("Платежная система " + type.getName() + " не найдена в списке платежных систем.");
    }

    /**
     * Возвращает список зарегистрированных платежных систем
     * @return 
     */
    public Vector<PaymentSystem> getPaymentSystems() {
        return paymentSystems;
    }
    
    
}
