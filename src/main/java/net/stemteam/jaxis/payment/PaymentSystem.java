/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.payment;

/**
 * Платежная система
 */
public class PaymentSystem {
    
    
    
    private final PaymentSystemType type;
    public PaymentSystem(PaymentSystemType type) {
        this.type = type;
    }

    /**
     * Возвращает тип платежной системы
     * @return 
     */
    public PaymentSystemType getType() {
        return type;
    }
    
    
    
}
