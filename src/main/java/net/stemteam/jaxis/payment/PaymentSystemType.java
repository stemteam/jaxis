/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.payment;

import net.stemteam.jaxis.err.PaymentException;

/**
 * Виды платежных систем
 */
public enum PaymentSystemType {
    
    alphabank("alphabank", "Альфа Банк");
    
    private final String code;
    private final String name;
    PaymentSystemType(String code, String name) {
        this.code = code;
        this.name= name;
    }

    /**
     * Возвращает название платежной системы
     * @return 
     */
    public String getName() {
        return name;
    }

    /**
     * Возвращает код платежной системы
     * @return 
     */
    public String getCode() {
        return code;
    }
    
    /**
     * Получение платежной системы по коду
     * @param code
     * @return 
     * @throws net.stemteam.jaxis.err.PaymentException 
     */
    public static PaymentSystemType get(String code) throws PaymentException {
        for (PaymentSystemType v : values()) {
            if (v.getCode().equals(code)) {
                return v;
            }
        }
        throw new PaymentException("PaymentSystemType с кодом " + code + " не существует");
    }
    
}
