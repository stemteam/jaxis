/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.payment;

/**
 * Информация о зарегистрированном заказе
 */
public class PaymentRegisterOrderAnswer {
    
    private final String orderId;
    private final String orderUrl;
    
    public PaymentRegisterOrderAnswer(String orderId, String formUrl) {
        this.orderId = orderId;
        this.orderUrl = formUrl;
    }

    /**
     * Возвращает идентификатор заказа оплаты в платежной системе
     * @return 
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Возвращает адрес формы оплаты в платежной системе
     * @return 
     */
    public String getOrderUrl() {
        return orderUrl;
    }
    
    
}
