/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.payment;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Возврат метода проверки статуса оплаты
 * 
 *    OrderStatus = 0 (не было попыток обработки)
 *    OrderStatus = 2 (заказ обработан)
 */
public class PaymentOrderStatus {

    private final String orderId; // Номер заказа в платежной системе. Уникален в пределах системы
    private final String orderNumber; // Номер (идентификатор) заказа в системе магазина
    private final String orderDescription; // произвольное описание платежа, указанное при запросе регистрации
    private final int orderStatus; // Состояние заказа в платежной системе. Значение выбирается из списка, приведенного ниже. Отсутствует, если заказ не был найден.
    private final int actionCode; // Код ответа.
    private final String actionCodeDescription; // Расшифровка кода ответа на языке, переданном в параметре Language в запросе.
    private final BigDecimal amount; // Сумма платежа в копейках (или центах)
    private final Timestamp date; // Дата регистрации заказа
    private final String currency; // Код валюты платежа ISO 4217. Если не указан, считается равным 810 (российские рубли).

    private final String maskedPan; // Маскированный номер карты, которая использовалась для оплаты. Указан только после оплаты заказа.
    private final String expiration; // Срок истечения действия карты в формате YYYYMM. Указан только после оплаты заказа.
    private final String cardholderName; // Имя держателя карты. Указан только после оплаты заказа.
    private final String ip; // IP адрес покупателя
    private final String bankName; // Наименование банка-эмитента
    private final String bankCountryCode; // Код страны банка-эмитента
    private final String bankCountryName; // Наименование страны банка-эмитента на языке, переданном в параметре language в запросе, или на языке пользователя, вызвавшего метод, если язык в запросе не указан
    
    private final String strdata;
    
    // Время сессии истекло
    private static final int ACTION_CODE_TIMEOUT = -2007;

    public PaymentOrderStatus(String orderId, String orderNumber, String orderDescription, Integer orderStatus, Integer actionCode, String actionCodeDescription, BigDecimal amount, Timestamp date, String currency, String maskedPan, String expiration, String cardholderName, String ip, String bankName, String bankCountryCode, String bankCountryName, String strdata) {
        this.orderId = orderId;
        this.orderNumber = orderNumber;
        this.orderDescription = orderDescription;
        this.orderStatus = orderStatus;
        this.actionCode = actionCode;
        this.actionCodeDescription = actionCodeDescription;
        this.amount = amount;
        this.date = date;
        this.currency = currency;
        this.maskedPan = maskedPan;
        this.expiration = expiration;
        this.cardholderName = cardholderName;
        this.ip = ip;
        this.bankName = bankName;
        this.bankCountryCode = bankCountryCode;
        this.bankCountryName = bankCountryName;
        this.strdata = strdata;
    }

    /**
     * Возвращает идентификатор платежа в системе оплат
     *
     * @return
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Возвращает идентификатор платежа в системе магазина (клиента)
     *
     * @return
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Возвращает описание платежа
     *
     * @return
     */
    public String getOrderDescription() {
        return orderDescription;
    }

    /**
     * Возвращает статус платежа
     *
     * @return
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }

    /**
     * Возвращает код действий, произведенных с платежем
     *
     * @return
     */
    public Integer getActionCode() {
        return actionCode;
    }

    /**
     * Возвращает описание действий, произведенных с платежем
     *
     * @return
     */
    public String getActionCodeDescription() {
        return actionCodeDescription;
    }

    /**
     * Возвращает сумму платежа
     *
     * @return
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Возвращает дату платежа
     *
     * @return
     */
    public Timestamp getDate() {
        return date;
    }

    /**
     * Код валюты платежа ISO 4217. Если не указан, считается равным 810
     * (российские рубли).
     *
     * @return
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Маскированный номер карты, которая использовалась для оплаты. Указан
     * только после оплаты заказа.
     *
     * @return
     */
    public String getMaskedPan() {
        return maskedPan;
    }

    /**
     * Срок истечения действия карты в формате YYYYMM. Указан только после
     * оплаты заказа.
     *
     * @return
     */
    public String getExpiration() {
        return expiration;
    }

    /**
     * Имя держателя карты. Указан только после оплаты заказа.
     *
     * @return
     */
    public String getCardholderName() {
        return cardholderName;
    }

    /**
     * IP адрес покупателя
     *
     * @return
     */
    public String getIp() {
        return ip;
    }

    /**
     * Наименование банка-эмитента
     *
     * @return
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * Код страны банка-эмитента
     *
     * @return
     */
    public String getBankCountryCode() {
        return bankCountryCode;
    }

    /**
     * Наименование страны банка-эмитента на языке, переданном в параметре
     * language в запросе, или на языке пользователя, вызвавшего метод, если
     * язык в запросе не указан
     *
     * @return
     */
    public String getBankCountryName() {
        return bankCountryName;
    }
    
    /**
     * Проверка успешного окончания оплаты
     * @return 
     */
    public boolean isSuccess() {
        return actionCode == 0;
    }
    
    /**
     * Проверяет, в обработке ли платеж или уже закончен/отменен
     * @return 
     */
    public boolean isFinished() {
        if (actionCode == 0) {
            return true;
        }
        if (orderStatus == 6) {
            return true;
        }
        if (actionCode == ACTION_CODE_TIMEOUT) {
            return true;
        }
        return false;
    }

    /**
     * Возвращает необработанную информацию статуса, возвращенную шлюзом
     * @return 
     */
    public String getStrdata() {
        return strdata;
    }
    
}
