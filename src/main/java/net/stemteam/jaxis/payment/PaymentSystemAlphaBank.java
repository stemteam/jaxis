/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.payment;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Timestamp;
import javax.net.ssl.HttpsURLConnection;
import net.stemteam.jaxis.err.NotConfiguredException;
import net.stemteam.jaxis.err.PaymentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Методы оплаты через шлюз альфа-банка
 */
public class PaymentSystemAlphaBank extends PaymentSystem {

    /**
     * Валюта оплаты - рубли (810)
     */
    public static String CURRENCY_RUR = "810";

    /**
     * Вид страницы возврата - для дэсктопа
     */
    public static String PAGEVIEW_DESKTOP = "DESKTOP";

    /**
     * Вид страницы возврата - для мобильного устройства
     */
    public static String PAGEVIEW_MOBILE = "MOBILE";

    /**
     * Язык страницы
     */
    public static String LANGUAGE_RU = "ru";

    private static String registerOrderUrl;
    private static String checkOrderStatusUrl;
    private static String username;
    private static String password;
    private static Integer sessionTimeoutSec;
    
    /**
     * Возвращает вермя жизни платежной сессии (секунд) 
     * @return 
     */
    public static Integer getSessionTimeoutSec() {
        return sessionTimeoutSec;
    }

    /**
     * Инициализация статических свойств для работы со шлюзом
     *
     * @param registerOrderUrl урл запроса регистрации заявки на полату в шлюзе платежной системы
     * @param checkOrderStatusUrl урл запроса проверки состояния оплаты в шлюзе платежной системы
     * @param username имя пользователя для авторизации в шлюзе
     * @param password пароль пользователя для авторизации в шлюзе
     * @param sessionTimeoutSec
     */
    public static void configure(String registerOrderUrl, String checkOrderStatusUrl, String username, String password, Integer sessionTimeoutSec) {
        PaymentSystemAlphaBank.registerOrderUrl = registerOrderUrl;
        PaymentSystemAlphaBank.checkOrderStatusUrl = checkOrderStatusUrl;
        PaymentSystemAlphaBank.username = username;
        PaymentSystemAlphaBank.password = password;
        PaymentSystemAlphaBank.sessionTimeoutSec = sessionTimeoutSec;
    }

    /**
     * Проверяет настройки работы со шлюзом
     */
    private static void checkConfigured() throws NotConfiguredException {
        if ((registerOrderUrl == null) || (checkOrderStatusUrl == null) || (username == null) || (password == null) || (sessionTimeoutSec == null)) {
            throw new NotConfiguredException("PaymentSystemAlphaBank not configured. Please call PaymentSystemAlphaBank.configure() void");
        }
    }

    public PaymentSystemAlphaBank() {
        super(PaymentSystemType.alphabank);
    }

    /**
     * Инициализация оплаты через шлюз альфа-банка. При вызове и успешных
     * параметрах возвращается номер заказа в платежной системы, и URL старницы
     * обработки оплаты платежной системы, на которой и производится конечная
     * оплата
     *
     * @param requestUrl урл запроса в шлюзе платежной системы
     * @param username имя пользователя для авторизации
     * @param password пароль пользователя для авторизации
     * @param returnUrl Адрес, на который надо перенаправить пользователя в
     * случае успешной оплаты
     * @param failUrl Адрес, на который надо перенаправить пользователя в случае
     * неуспешной оплаты
     * @param pageView В pageView передаётся признак - мобильное устройство
     * (pageView=MOBILE) или десктоп (pageView=DESKTOP). В зависимости от этого
     * в ответе будет ссылка на мобильную платёжную страницу или на обычную.
     * @param orderNum Номер (идентификатор) заказа в системе магазина, уникален
     * для каждого магазина в пределах системы (генерируется магазином)
     * @param orderDescription
     * @param amount сумма оплаты в количестве минимальных денежных единиц (для
     * рублей это сумма в копейках)
     * @param currency Код валюты платежа ISO 4217 (810 = рубли, ...)
     * @param language Язык в кодировке ISO 639-1 (ru, ...)
     * @param sessionTimeoutSec время жизни сессии, секунд
     * @return
     * @throws net.stemteam.jaxis.err.PaymentException
     */
    public static PaymentRegisterOrderAnswer registerOrder(String requestUrl, String username, String password, String returnUrl, String failUrl, String pageView, String orderNum, String orderDescription, BigDecimal amount, String currency, String language, Integer sessionTimeoutSec) throws PaymentException {
        URL myurl;
        try {
            myurl = new URL(requestUrl);
        } catch (MalformedURLException ex) {
            throw new PaymentException("registerOrderUrl incorrect");
        }
        
        // удаляем ноли в дробной части (альфашлюз понимает только целое число)
        amount = amount.stripTrailingZeros();
        
        StringBuilder query = new StringBuilder();
        query.append("amount=");
        query.append(amount.toPlainString());
        query.append("&currency=");
        query.append(currency);
        query.append("&language=");
        query.append(language);
        query.append("&sessionTimeoutSecs=");
        query.append(sessionTimeoutSec);
        query.append("&pageView=");
        query.append(pageView);
        try {
            query.append("&orderNumber=");
            query.append(URLEncoder.encode(orderNum, "UTF-8"));
            if (orderDescription != null) {
                query.append("&description=");
                query.append(URLEncoder.encode(orderDescription, "UTF-8"));
            }
            query.append("&userName=");
            query.append(URLEncoder.encode(username, "UTF-8"));
            query.append("&password=");
            query.append(URLEncoder.encode(password, "UTF-8"));
            query.append("&returnUrl=");
            query.append(URLEncoder.encode(returnUrl, "UTF-8"));
            query.append("&failUrl=");
            query.append(URLEncoder.encode(failUrl, "UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            throw new PaymentException("Unsupported encoding UTF-8");
        }

        HttpsURLConnection con = null;
        StringBuilder sb = new StringBuilder();
        try {
            con = (HttpsURLConnection) myurl.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-length", String.valueOf(query.length()));
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
            con.setDoOutput(true);
            con.setDoInput(true);

            try (DataOutputStream output = new DataOutputStream(con.getOutputStream())) {
                output.writeBytes(query.toString());
            }

            con.connect();
            int status = con.getResponseCode();
            if (status == HttpURLConnection.HTTP_OK) {
                try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8")) // UTF-8 обязательно
                        ) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                }
            } else { // некая внутренняя ошибка сервера
                try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                        ) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                }
                Logger log = LoggerFactory.getLogger(PaymentSystemAlphaBank.class);
                log.error("Не удалось совершить оплату {} {}", orderNum, sb.toString());
                throw new PaymentException((new StringBuilder()).append("Не удалось совершить оплату ").append(orderNum).append(". Внутренняя ошибка.").toString());
            }
        } catch (IOException ex) {
            throw new PaymentException(ex, "Не удалось выполнить запрос к платежному шлюзу Альфа Банк");
        }

//        Параметры ответа:
//Название Тип Обязательно Описание
//orderId Номер заказа в платежной системе. Уникален в пределах системы. Отсутствует, если
//регистрация заказа на удалась по причине ошибки, детализированной в ErrorCode.
//formUrl URL платежной формы, на который надо перенаправить браузер клиента. Не возвращается,
//если регистрация заказа не удалась по причине ошибки, детализированной в ErrorCode.
//errorCode  Код ошибки
//errorMessage Описание ошибки на языке, переданном в параметре Language в запросе.
        JsonParser parser = new JsonParser();
        JsonElement elem = parser.parse(sb.toString());
        JsonObject obj = elem.getAsJsonObject();

        String orderId = obj.getAsJsonPrimitive("orderId") == null ? null : obj.getAsJsonPrimitive("orderId").getAsString();
        String formUrl = obj.getAsJsonPrimitive("formUrl") == null ? null : obj.getAsJsonPrimitive("formUrl").getAsString();
        String errorMessage = obj.getAsJsonPrimitive("errorMessage") == null ? null : obj.getAsJsonPrimitive("errorMessage").getAsString();

        if (orderId == null) {
            throw new PaymentException((new StringBuilder()).append("Не удалось совершить оплату ").append(orderNum).append(". Сообщение шлюза: ").append(errorMessage).toString());
        }

        PaymentRegisterOrderAnswer info = new PaymentRegisterOrderAnswer(orderId, formUrl);
        return info;
    }

    /**
     * Инициализация оплаты через шлюз альфа-банка. При вызове и успешных
     * параметрах возвращается номер заказа в платежной системы, и URL старницы
     * обработки оплаты платежной системы, на которой и производится конечная
     * оплата
     *
     * @param returnUrl Адрес, на который надо перенаправить пользователя в
     * случае успешной оплаты
     * @param failUrl Адрес, на который надо перенаправить пользователя в случае
     * неуспешной оплаты
     * @param pageView В pageView передаётся признак - мобильное устройство
     * (pageView=MOBILE) или десктоп (pageView=DESKTOP). В зависимости от этого
     * в ответе будет ссылка на мобильную платёжную страницу или на обычную.
     * @param orderNum Номер (идентификатор) заказа в системе магазина, уникален
     * для каждого магазина в пределах системы (генерируется магазином)
     * @param orderDescription
     * @param amount сумма оплаты в количестве минимальных денежных единиц (для
     * рублей это сумма в копейках)
     * @param currency Код валюты платежа ISO 4217 (810 = рубли, ...)
     * @param language Язык в кодировке ISO 639-1 (ru, ...)
     * @return
     * @throws net.stemteam.jaxis.err.PaymentException
     * @throws net.stemteam.jaxis.err.NotConfiguredException
     */
    public static PaymentRegisterOrderAnswer registerOrder(String returnUrl, String failUrl, String pageView, String orderNum, String orderDescription, BigDecimal amount, String currency, String language) throws PaymentException, NotConfiguredException {
        checkConfigured();
        return registerOrder(registerOrderUrl, username, password, returnUrl, failUrl, pageView, orderNum, orderDescription, amount, currency, language, sessionTimeoutSec);
    }

    /**
     * Инициализация оплаты через шлюз альфа-банка. При вызове и успешных
     * параметрах возвращается номер заказа в платежной системы, и URL старницы
     * обработки оплаты платежной системы, на которой и производится конечная
     * оплата
     *
     * @param username имя пользователя для авторизации
     * @param password пароль пользователя для авторизации
     * @param returnUrl Адрес, на который надо перенаправить пользователя в
     * случае успешной оплаты
     * @param failUrl Адрес, на который надо перенаправить пользователя в случае
     * неуспешной оплаты
     * @param pageView В pageView передаётся признак - мобильное устройство
     * (pageView=MOBILE) или десктоп (pageView=DESKTOP). В зависимости от этого
     * в ответе будет ссылка на мобильную платёжную страницу или на обычную.
     * @param orderNum Номер (идентификатор) заказа в системе магазина, уникален
     * для каждого магазина в пределах системы (генерируется магазином)
     * @param orderDescription
     * @param amount сумма оплаты в количестве минимальных денежных единиц (для
     * рублей это сумма в копейках)
     * @param currency Код валюты платежа ISO 4217 (810 = рубли, ...)
     * @param language Язык в кодировке ISO 639-1 (ru, ...)
     * @return
     * @throws net.stemteam.jaxis.err.PaymentException
     * @throws net.stemteam.jaxis.err.NotConfiguredException
     */
    public static PaymentRegisterOrderAnswer registerOrder(String username, String password, String returnUrl, String failUrl, String pageView, String orderNum, String orderDescription, BigDecimal amount, String currency, String language) throws PaymentException, NotConfiguredException {
        checkConfigured();
        return registerOrder(registerOrderUrl, username, password, returnUrl, failUrl, pageView, orderNum, orderDescription, amount, currency, language, sessionTimeoutSec);
    }

    /**
     * Проверка состояния оплаты
     *
     * @param requestUrl урл запроса в шлюзе платежной системы
     * @param username имя пользователя для авторизации
     * @param password пароль пользователя для авторизации
     * @param language Язык в кодировке ISO 639-1 (ru, ...)
     * @param orderId Идентификатор оплаты в системе банка
     * @return
     * @throws MalformedURLException
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws PaymentException
     */
    public static PaymentOrderStatus checkOrderStatus(String requestUrl, String username, String password, String language, String orderId) throws PaymentException {
        URL myurl;
        try {
            myurl = new URL(requestUrl);
        } catch (MalformedURLException ex) {
            throw new PaymentException("checkOrderStatusUrl incorrect");
        }
        StringBuilder query = new StringBuilder();
        query.append("language=").append(language);
        query.append("&orderId=").append(orderId);
        try {
            query.append("&userName=").append(URLEncoder.encode(username, "UTF-8"));
            query.append("&password=").append(URLEncoder.encode(password, "UTF-8"));
        } catch (UnsupportedEncodingException ex) {
            throw new PaymentException("Unsupported encoding UTF-8");
        }

        HttpsURLConnection con = null;
        StringBuilder sb = new StringBuilder();
        try {
            con = (HttpsURLConnection) myurl.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-length", String.valueOf(query.length()));
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
            con.setDoOutput(true);
            con.setDoInput(true);

            try (DataOutputStream output = new DataOutputStream(con.getOutputStream())) {
                output.writeBytes(query.toString());
            }

            con.connect();
            int status = con.getResponseCode();
            if (status == HttpURLConnection.HTTP_OK) {
                try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8")) // UTF-8 обязательно
                        ) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                }
            } else { // некая внутренняя ошибка сервера
                try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getErrorStream(), "UTF-8")) // UTF-8 обязательно
                        ) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                        sb.append("\n");
                    }
                }
                Logger log = LoggerFactory.getLogger(PaymentSystemAlphaBank.class);
                log.error("Не удалось совершить оплату", orderId, sb.toString());
                throw new PaymentException((new StringBuilder()).append("Не удалось проверить состояние оплаты ").append(orderId).append(". Внутренняя ошибка.").toString());
            }
        } catch (IOException ex) {
            throw new PaymentException(ex, "Не удалось выполнить запрос к платежному шлюзу Альфа Банк");
        }

        JsonParser parser = new JsonParser();
        JsonElement elem = parser.parse(sb.toString());
        JsonObject obj = elem.getAsJsonObject();

        Integer errorCode = obj.getAsJsonPrimitive("errorCode") == null ? null : obj.getAsJsonPrimitive("errorCode").getAsInt();
        String errorMessage = obj.getAsJsonPrimitive("errorMessage") == null ? null : obj.getAsJsonPrimitive("errorMessage").getAsString();
        String orderNumber = obj.getAsJsonPrimitive("orderNumber") == null ? null : obj.getAsJsonPrimitive("orderNumber").getAsString();
        String orderDescription = obj.getAsJsonPrimitive("orderDescription") == null ? null : obj.getAsJsonPrimitive("orderDescription").getAsString();
        Integer orderStatus = obj.getAsJsonPrimitive("orderStatus") == null ? null : obj.getAsJsonPrimitive("orderStatus").getAsInt();
        Integer actionCode = obj.getAsJsonPrimitive("actionCode") == null ? null : obj.getAsJsonPrimitive("actionCode").getAsInt();
        String actionCodeDescription = obj.getAsJsonPrimitive("actionCodeDescription") == null ? null : obj.getAsJsonPrimitive("actionCodeDescription").getAsString();
        BigDecimal amount = obj.getAsJsonPrimitive("amount") == null ? null : obj.getAsJsonPrimitive("amount").getAsBigDecimal();
        Long date = obj.getAsJsonPrimitive("date") == null ? null : obj.getAsJsonPrimitive("date").getAsLong();
        String currency = obj.getAsJsonPrimitive("currency") == null ? null : obj.getAsJsonPrimitive("currency").getAsString();
        String maskedPan = obj.getAsJsonPrimitive("maskedPan") == null ? null : obj.getAsJsonPrimitive("maskedPan").getAsString();
        String expiration = obj.getAsJsonPrimitive("expiration") == null ? null : obj.getAsJsonPrimitive("expiration").getAsString();
        String cardholderName = obj.getAsJsonPrimitive("cardholderName") == null ? null : obj.getAsJsonPrimitive("cardholderName").getAsString();
        String ip = obj.getAsJsonPrimitive("ip") == null ? null : obj.getAsJsonPrimitive("ip").getAsString();
        String bankName = obj.getAsJsonPrimitive("bankName") == null ? null : obj.getAsJsonPrimitive("bankName").getAsString();
        String bankCountryCode = obj.getAsJsonPrimitive("bankCountryCode") == null ? null : obj.getAsJsonPrimitive("bankCountryCode").getAsString();
        String bankCountryName = obj.getAsJsonPrimitive("bankCountryName") == null ? null : obj.getAsJsonPrimitive("bankCountryName").getAsString();

        if ((errorCode == null) || (errorCode != 0)) {
            throw new PaymentException((new StringBuilder()).append("Не удалось проверить состояние оплаты ").append(orderId).append(". Сообщение шлюза: ").append(errorMessage).toString());
        }

        Timestamp sdate = null;
        if (date != null) {
            sdate = new Timestamp(date);
        }
        PaymentOrderStatus ans = new PaymentOrderStatus(orderId, orderNumber, orderDescription, orderStatus, actionCode, actionCodeDescription, amount, sdate, currency, maskedPan, expiration, cardholderName, ip, bankName, bankCountryCode, bankCountryName, sb.toString());
        return ans;
    }

    /**
     * Проверка состояния оплаты
     *
     * @param language Язык в кодировке ISO 639-1 (ru, ...)
     * @param orderId Идентификатор оплаты в системе банка
     * @return
     * @throws PaymentException
     * @throws net.stemteam.jaxis.err.NotConfiguredException
     */
    public static PaymentOrderStatus checkOrderStatus(String language, String orderId) throws PaymentException, NotConfiguredException {
        checkConfigured();
        return checkOrderStatus(checkOrderStatusUrl, username, password, language, orderId);
    }

    /**
     * Проверка состояния оплаты
     *
     * @param username имя пользователя для авторизации
     * @param password пароль пользователя для авторизации
     * @param language Язык в кодировке ISO 639-1 (ru, ...)
     * @param orderId Идентификатор оплаты в системе банка
     * @return
     * @throws PaymentException
     * @throws net.stemteam.jaxis.err.NotConfiguredException
     */
    public static PaymentOrderStatus checkOrderStatus(String username, String password, String language, String orderId) throws PaymentException, NotConfiguredException {
        checkConfigured();
        return checkOrderStatus(checkOrderStatusUrl, username, password, language, orderId);
    }

    public PaymentSystemAlphaBank(PaymentSystemType type) {
        super(type);
    }

}
