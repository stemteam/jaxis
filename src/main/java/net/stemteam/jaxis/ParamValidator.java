/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis;

import net.stemteam.datatransport.exception.DataSetException;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;
import net.stemteam.datatransport.transport.StringEncoder;
import net.stemteam.jaxis.common.ExceptionHelper;
import net.stemteam.jaxis.common.JsonHelper;
import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.handler.AtomHandler;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.jaxis.common.Version;

/**
 * Коллекция ErrorResponse
 */
public class ParamValidator extends ArrayList<ValidationErrorItem> {

    private final AtomHandler handler;

    public ParamValidator(AtomHandler handler) {
        this.handler = handler;
    }

    /**
     * Преобразовывает к датасету (Message,Exception,ExceptionObject,CallStack)
     *
     * @return
     * @throws DataSetException
     */
    public DataSet toDataSet() throws DataSetException {
        DataSet ds = new DataSet();
        ds.createColumn(FIELD_MESSAGE, DataColumnType.STRING);
        ds.createColumn(FIELD_EXCEPTION, DataColumnType.STRING);
        ds.createColumn(FIELD_EXCEPTION_OBJECT, DataColumnType.STRING);
        ds.createColumn(FIELD_CALL_STACK, DataColumnType.STRING);
        for (ValidationErrorItem item : this) {
            DataRecord row = ds.createRecord();
            row.setString(FIELD_MESSAGE, item.getMessage());
            row.setString(FIELD_EXCEPTION, item.getException());
            row.setString(FIELD_EXCEPTION_OBJECT, item.getExceptionObject());
            row.setString(FIELD_CALL_STACK, item.getCallStack());
        }
        return ds;
    }
    private static final String FIELD_CALL_STACK = "CallStack";
    private static final String FIELD_EXCEPTION_OBJECT = "ExceptionObject";
    private static final String FIELD_EXCEPTION = "Exception";
    private static final String FIELD_MESSAGE = "Message";

    private static final String FIELD_IS_EMPTY = "FieldIsEmpty";
    private static final String FIELD_IS_EMPTY_MSG = "Параметр не может быть пустым";
    private static final String NOT_A_INT = "NotInteger";
    private static final String NOT_A_INT_MSG = "Не соответствует формату Integer";
    private static final String NOT_A_DOUBLE = "NotDouble";
    private static final String NOT_A_DOUBLE_MSG = "Не соответствует формату Double";
    private static final String NOT_A_LONG = "NotInt64";
    private static final String NOT_A_LONG_MSG = "Не соответствует формату Int64";
    private static final String NOT_A_DATETIME = "NotDateTime";
    private static final String NOT_A_DATETIME_MSG = "Не соответствует формату DateTime";
    private static final String NOT_A_TIMESTAMP = "NotTimestamp";
    private static final String NOT_A_TIMESTAMP_MSG = "Не соответствует формату Timestamp";
    private static final String NOT_A_DATASET = "NotDataSet";
    private static final String NOT_A_DATASET_MSG = "Не соответствует формату DataSet";
    private static final String NOT_A_FILE = "NotFile";
    private static final String NOT_A_FILE_MSG = "Не соответствует формату UploadedFile";
    private static final String NOT_A_INT_LIST = "NotIntList";
    private static final String NOT_A_INT_LIST_MSG = "Не является списком идентификаторов с разделителем запятая";
    private static final String NOT_A_NUMBER = "NotNumber";
    private static final String NOT_A_NUMBER_MSG = "Не является числом";
    private static final String NOT_A_VERSION = "NotVersion";
    private static final String NOT_A_VERSION_MSG = "Некорректный номер версии";

    // по HTTP кодам
    public static final String UNAUTHORIZED = "Unauthorized";
    public static final String FORBIDDEN = "Forbidden";
    public static final String INTERNAL = "Internal";
    public static final String NOT_FOUND = "NotFound";
    public static final String BAD_REQUEST = "BadRequest";

    // патерн проверки списков идентификаторов
    static Pattern p = Pattern.compile("^(\\d+,\\s?)*\\d+$");

    /**
     * Чтение параметра из карты параметров
     *
     * @param params карта параметров
     * @param paramName имя параметра
     * @param checkNotNull проверять ли на не нулл
     * @return значение
     */
    public Object getObject(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        return o;
    }

    /**
     * Возвращает параметр типа Int из карты параметров
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     */
    public Integer getInt(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        if (o == null) {
            return null;
        }
        try {
            Integer i = o == null ? null : Integer.parseInt((String) o);
            return i;
        } catch (NumberFormatException ex) {
            add(new ValidationErrorItem(NOT_A_INT, paramName, NOT_A_INT_MSG));
        }
        return null;
    }

    /**
     * Возвращает параметр типа Int из карты параметров
     *
     * @param params
     * @param paramName
     * @return
     */
    public Integer getInt(HashMap params, String paramName) {
        return getInt(params, paramName, false);
    }

    /**
     * Возвращает параметр типа Long из карты параметров
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     */
    public Long getLong(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        if (o == null) {
            return null;
        }
        try {
            Long i = o == null ? null : Long.parseLong((String) o);
            return i;
        } catch (NumberFormatException ex) {
            add(new ValidationErrorItem(NOT_A_LONG, paramName, NOT_A_LONG_MSG));
        }
        return null;
    }

    /**
     * Возвращает параметр типа Long из карты параметров
     *
     * @param params
     * @param paramName
     * @return
     */
    public Long getLong(HashMap params, String paramName) {
        return getLong(params, paramName, false);
    }

    /**
     * Возвращает параметр типа Double из карты параметров
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     */
    public Double getDouble(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        if (o == null) {
            return null;
        }
        try {
            Double i = o == null ? null : Double.parseDouble((String) o);
            if (i.isNaN()) {
                add(new ValidationErrorItem(NOT_A_NUMBER, paramName, NOT_A_NUMBER_MSG));
                return null;
            }
            return i;
        } catch (NumberFormatException ex) {
            add(new ValidationErrorItem(NOT_A_DOUBLE, paramName, NOT_A_DOUBLE_MSG));
        }
        return null;
    }

    /**
     * Возвращает параметр типа Double из карты параметров
     *
     * @param params
     * @param paramName
     * @return
     */
    public Double getDouble(HashMap params, String paramName) {
        return getDouble(params, paramName, false);
    }

    /**
     * Возвращает параметр типа String из карты параметров
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     * @throws IllegalArgumentException
     */
    public String getString(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        return (String) o;
    }

    /**
     * Возвращает строку со списком идентификаторов типа Int (разделитель запятая, с проверкой содержимого на
     * соответствие)
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     */
    public String getIntList(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        if (o == null) {
            return null;
        }
        String s = (String) o;
        // проверяем на содержимое: допускаются только идентификаторы и запятые
        if (!p.matcher(s).matches()) {
            add(new ValidationErrorItem(NOT_A_INT_LIST, paramName, NOT_A_INT_LIST_MSG));
            return null;
        }
        return s;
    }

    /**
     * Возвращает строку со списком идентификаторов типа Int (разделитель запятая, с проверкой содержимого на
     * соответствие)
     *
     * @param params
     * @param paramName
     * @return
     */
    public String getIntList(HashMap params, String paramName) {
        return getIntList(params, paramName, false);
    }

    /**
     * Возвращает параметр типа String из карты параметров
     *
     * @param params
     * @param paramName
     * @return
     */
    public String getString(HashMap params, String paramName) {
        return getString(params, paramName, false);
    }

    /**
     * Возвращает параметр типа Timestamp (YY) из карты параметров
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     */
    @Deprecated
    public java.sql.Timestamp getDateTime(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        if (o == null) {
            return null;
        }
        try {
            if (handler.getContentType() == JaxisContentType.JSON2) {
                Long unixTimestampMs = Long.parseLong((String) o);
                java.sql.Timestamp ts = new Timestamp(unixTimestampMs);
                return ts;
            } else {
                java.sql.Timestamp ts = StringEncoder.decodeDateTime((String) o);
                return ts;
            }
        } catch (ParseException ex) {
            add(new ValidationErrorItem(NOT_A_DATETIME, paramName, NOT_A_DATETIME_MSG));
            return null;
        }
    }

    /**
     * Возвращает параметр типа Timestamp (YY) из карты параметров
     *
     * @param params
     * @param paramName
     * @return
     * @deprecated хочется везде использовать YYYY
     */
    @Deprecated
    public java.sql.Timestamp getDateTime(HashMap params, String paramName) {
        return getDateTime(params, paramName, false);
    }

    /**
     * Возвращает параметр типа DataSet из карты параметров
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     * @throws IllegalArgumentException
     */
    public DataSet getDataSet(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        try {
            if (o == null) {
                return null;
            }
            if (handler.getContentType() == JaxisContentType.JSON) {
                DataSet ds = DataSet.fromJson((String) o);
                return ds;
            } else if (handler.getContentType() == JaxisContentType.JSON2) {
                DataSet ds = DataSet.fromJson2((String) o);
                return ds;
            } else {
                DataSet dataSet = new DataSet();
                dataSet.UnPackData((String) o);
                return dataSet;
            }
        } catch (Exception ex) {
            add(new ValidationErrorItem(NOT_A_DATASET, paramName, NOT_A_DATASET_MSG, ExceptionHelper.getErrorString(ex)));
            return null;
        }
    }

    /**
     * Возвращает параметр типа DataSet из карты параметров
     *
     * @param params
     * @param paramName
     * @return
     */
    public DataSet getDataSet(HashMap params, String paramName) {
        return getDataSet(params, paramName, false);
    }

    /**
     * Возвращает параметр типа Timestamp (YYYY) из карты параметров
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     */
    @Deprecated
    public java.sql.Timestamp getTimeStamp(HashMap params, String paramName, boolean checkNotNull) {
        return getTimestamp(params, paramName, checkNotNull);
    }

    /**
     * Возвращает параметр типа Timestamp из карты параметров
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     */
    public java.sql.Timestamp getTimestamp(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        if (o == null) {
            return null;
        }
        try {
            if (handler.getContentType() == JaxisContentType.JSON2) {
                Long unixTimestampMs = Long.parseLong((String) o);
                java.sql.Timestamp ts = new Timestamp(unixTimestampMs);
                return ts;
            } else {
                // вэберы запарили путать форматы и слать таймстам то с 2-мя, то 
                // с 4-мя цифрами года, поэтому будем читать и так и так, 
                // поскольку разбираться с местами где они не так передали после 
                // очередного их обновления говнокода мне уже порядком надоело.
                try {
                    // 4 цифры года
                    return StringEncoder.decodeTimeStamp((String) o);
                } catch (ParseException e) {
                    // 2 цифры года
                    return StringEncoder.decodeDateTime((String) o);
                }
            }
        } catch (Exception ex) {
            add(new ValidationErrorItem(NOT_A_TIMESTAMP, paramName, NOT_A_TIMESTAMP_MSG));
            return null;
        }
    }

    /**
     * Возвращает параметр типа Timestamp из карты параметров
     *
     * @param params
     * @param paramName
     * @return
     */
    public java.sql.Timestamp getTimestamp(HashMap params, String paramName) {
        return getTimestamp(params, paramName, false);
    }

    /**
     * Возвращает параметр типа Timestamp (YYYY) из карты параметров
     *
     * @param params
     * @param paramName
     * @return
     */
    @Deprecated
    public java.sql.Timestamp getTimeStamp(HashMap params, String paramName) {
        return getTimestamp(params, paramName, false);
    }

    /**
     * Это такой хитрый (но простой) метод, который вызывается после проверки параметров и формирует исключение ежели
     * все плохо. Вызывается руками после проверки параметров, чтобы не продолжать мучения обработки.
     */
    public void postValidate() throws IllegalArgumentException {
        if (!this.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append("-=#");
            for (ValidationErrorItem i : this) {
                if (i.getException() != null) {
                    sb.append(i.getException()).append(" ");
                }
                if (i.getExceptionObject() != null) {
                    sb.append(i.getExceptionObject()).append(" ");
                }
                if (i.getMessage() != null) {
                    sb.append(i.getMessage());
                }
                sb.append("|");
            }
            sb.append("#=-");
            throw new IllegalArgumentException(sb.toString());
        }
    }

    /**
     * Добавляет исключение вида FORBIDDEN с собщением и стеком вызова. Всегда генерирует исключение SecurityException.
     *
     * @param message
     * @throws net.stemteam.jaxis.err.HttpForbiddenException
     */
    public void throwForbidden(String message) throws HttpForbiddenException {
        throwForbidden(FORBIDDEN, message);
    }

    /**
     * Добавляет исключение вида FORBIDDEN с типом, собщением и стеком вызова.
     *
     * @param exception
     * @param message
     * @throws HttpForbiddenException
     */
    public void throwForbidden(String exception, String message) throws HttpForbiddenException {
        ValidationErrorItem item = new ValidationErrorItem(exception, null, message, ExceptionHelper.getCurrentStack(3));
        add(item);
        throw new HttpForbiddenException(ExceptionHelper.prepareMessage(message));
    }

    /**
     * Возвращает параметр типа UploadedFile из карты параметров
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     */
    public UploadedFile getUploadedFile(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        if (!(o instanceof UploadedFile)) {
            add(new ValidationErrorItem(NOT_A_FILE, paramName, NOT_A_FILE_MSG));
            return null;
        } else {
            return (UploadedFile) o;
        }
    }

    /**
     * Возвращает параметр типа UploadedFile из карты параметров
     *
     * @param params
     * @param paramName
     * @return
     */
    public UploadedFile getUploadedFile(HashMap params, String paramName) {
        return getUploadedFile(params, paramName, false);
    }

    /**
     * Чтение параметра с номером версии
     *
     * @param params
     * @param paramName
     * @param checkNotNull
     * @return
     */
    public Version getVersion(HashMap params, String paramName, boolean checkNotNull) {
        Object o = params.get(paramName);
        if ((checkNotNull) && (o == null)) {
            add(new ValidationErrorItem(FIELD_IS_EMPTY, paramName, FIELD_IS_EMPTY_MSG));
            return null;
        }
        String verNum = (String) o;
        try {
            Version.check(verNum);
        } catch (IllegalArgumentException ex) {
            add(new ValidationErrorItem(NOT_A_VERSION, paramName, NOT_A_VERSION_MSG));
        }
        return new Version(verNum);
    }

    /**
     * Чтение параметра с номером версии
     *
     * @param params
     * @param paramName
     * @return
     */
    public Version getVersion(HashMap params, String paramName) {
        return getVersion(params, paramName, false);
    }

}
