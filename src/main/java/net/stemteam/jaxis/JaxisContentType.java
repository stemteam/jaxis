/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis;

/**
 * Виды контента, отдаваймые жахесом
 */
public enum JaxisContentType {
    
    DEFAULT(1, "", "text/plain;charset=utf-8"),
    JSON(2, ".json", "application/json;charset=utf-8"),
    HTML(3, ".html", "text/html;charset=utf-8"),
    BIN(4, ".bin", "application/octet-stream"),
    JSON2(5, ".json2", "application/json;charset=utf-8"),
    JAXIS(6, ".jaxis", "text/plain;charset=utf-8");
    
    private final int id;
    private final String ext;
    private final String contentType;
    
    JaxisContentType(int id, String ext, String contentType) {
        this.id = id;
        this.ext = ext;
        this.contentType = contentType;
    }
    
    public String getExt() {
        return this.ext;
    }
    
    public String getContentType() {
        return this.contentType;
    }
    
}
