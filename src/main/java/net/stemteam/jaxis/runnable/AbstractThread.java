package net.stemteam.jaxis.runnable;

import java.util.Date;

/**
 * Прототип потока обработки
 */
public abstract class AbstractThread extends Thread {

    // флаг проверки необходимости продолжения работы
    private boolean keepRunning = true;

    // таймаут ожидания (милисекунд)
    private int sleepTimeout = 1000;

    // время запуска потока
    private Date startTime;

    // последнее время выполнения
    private Date lastTime;

    /**
     * Конструктор по умолчанию
     * выполняет setDaemon(true)
     */
    public AbstractThread() {
        this.setDaemon(false);
    }

    /**
     * Конструктор
     * @param isDaemon выполнять в виде демона
     */
    public AbstractThread(boolean isDaemon) {
        this.setDaemon(isDaemon);
    }

    /**
     *  Главный метод
     */
    @Override
    public void run() {
        // работаем пока требо
        while (keepRunning) {
            try {
                // обновляем время последнего выполнения
                lastTime = new Date();
                // выполняем основное действо
                runAction();
                // спим
                sleep(sleepTimeout);
            } catch (InterruptedException ex) {}
      }
    }

    /**
     * Само непосредственное десйство в потоке
     */
    public abstract void runAction();

    /**
     * Запрашивает остановку потока
     */
    public synchronized void pleaseStop() {
        keepRunning = false;
    }

    /**
     * Метод запуска
     */
    @Override
    public void start() {
        startTime = new Date();
        super.start();
    }

    /**
     * Возвращает времия запуска потока
     * @return время запуска
     */
    public synchronized Date getStartTime() {
        return this.startTime;
    }

    /**
     * Возвращает время последнего выполнения
     * @return время последнего выполнения
     */
    public synchronized Date getLastTime() {
        return this.lastTime;
    }

    /**
     * Возвращает интервал ожидания потока (милисекунд)
     * @return
     */
    public synchronized int getSleepTimeout() {
        return sleepTimeout;
    }

    /**
     * Устанавливает интервал ожидания потока (милисекунд)
     * @param sleepTimeout
     */
    public synchronized void setSleepTimeout(int sleepTimeout) {
        this.sleepTimeout = sleepTimeout;
    }


}
