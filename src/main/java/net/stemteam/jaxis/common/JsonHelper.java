package net.stemteam.jaxis.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.stemteam.jaxis.err.HttpException;

public class JsonHelper {
    
    public static String exceptionToJson(Exception ex) {
        String json = null;
        String msg = "";
        if (ex.getMessage() == null) {
            if (ex instanceof SecurityException) {
                msg = "Нет прав для выполнения действия";
            }
        } else {
            msg = ExceptionHelper.getErrorString(ex);
        }
        
        // если у нас есть пользовательское сообщение
        if (ex instanceof HttpException) {
            String s = ((HttpException) ex).getUserMessage();
            if (s != null) {
                msg = s;
            }
        }
        
        Error e = new Error(msg);
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting(); // печатать красиво
        builder.serializeNulls(); // не исключать нул-значения
        Gson gson = builder.create();
        json = gson.toJson(e);
        return json;
    }
    
    static class Error {
        private String error;

        public Error(String error) {
            this.error = error;
        }
        
        public String getError() {
            return error;
        }
    }
    
    public static String toJson(Object obj) {
        return toJson(obj, false);
    }
    
    public static String toJson(Object obj, boolean isSerializeNulls) {
        String json = null;
        GsonBuilder builder = new GsonBuilder();
//        builder.setPrettyPrinting(); // печатать красиво
        if (isSerializeNulls) {
            builder.serializeNulls(); // не исключать нул-значения
        }
        //builder.generateNonExecutableJson();
        Gson gson = builder.create();
        json = gson.toJson(obj);
        return json;
    }
        
    /**
     * Парсит текст объекта с ошибкой (error = ) в формате JSON
     * @param jsonError JSON объект с ошибкой
     * @return 
     */
    public static String fromJsonError(String jsonError) {
        GsonBuilder builder = new GsonBuilder();
        builder.serializeNulls();
        Gson gson = builder.create();
        Error e = gson.fromJson(jsonError, Error.class);
        return e.getError();
    }
    
}
