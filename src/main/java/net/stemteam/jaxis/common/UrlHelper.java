package net.stemteam.jaxis.common;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class UrlHelper {

    /**
     * Урл-кодирование значения
     *
     * @param o значение
     * @param encoding кодировка
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String Encode(Object o, String encoding) throws UnsupportedEncodingException {
        if (o == null) {
            return "";
        }
        String str = String.valueOf(o);
        String result = URLEncoder.encode(str, encoding);
        result = result
                .replace("+", "%20")
                .replace("*", "%2A");
//        result = result
//                .replace("~", "%7E")
//                .replace("!", "%21")
//                .replace("@", "%40")
//                .replace("#", "%23")
//                .replace("$", "%24")
//                .replace("&", "%26")
//                .replace("*", "%2A")
//                .replace("(", "%28")
//                .replace(")", "%29")
//                .replace(",", "%2C")
//                .replace("/", "%2F")
//                .replace("?", "%3F")
//                .replace(";", "%3B")
//                .replace(":", "%3A")
//                .replace("=", "%3D")
//                .replace("+", "%2B")
//                .replace("/", "%2F")
//                .replace("'", "%27");
        return result;
    }

    public static String Encode(Object o) throws UnsupportedEncodingException {
        return Encode(o, "utf-8");
    }

    public static String Decode(String str, String encoding) throws UnsupportedEncodingException {
        if (str == null) {
            return "";
        }
        String result = URLDecoder.decode(str, encoding);
        return result;
    }

    public static String Decode(String str) throws UnsupportedEncodingException {
        return Decode(str, "utf-8");
    }

    /**
     * Приводит пространство имен к виду /preved/medved/moped
     *
     * @param namespace
     * @return
     */
    public static String correctNamespace(String namespace) {
        // корректируем пространстов имен под формат
        if ((namespace == null) || (namespace == "/")) {
            namespace = "";
        }
        if ((namespace.length() > 1) && (!namespace.startsWith("/"))) {
            namespace = "/" + namespace;
        }
        if ((namespace.length() > 1) && (namespace.endsWith("/"))) {
            namespace = namespace.substring(0, namespace.length() - 1);
        }
        return namespace;
    }

}
