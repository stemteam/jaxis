/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.common;

import net.stemteam.jaxis.UploadStore;
import net.stemteam.jaxis.UploadedFile;
import static net.stemteam.jaxis.handler.AbstractHandler.CHARSET;
import net.stemteam.jaxis.handler.AtomHandler;
import com.sun.net.httpserver.HttpExchange;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.MultipartStream;
import org.apache.commons.io.FilenameUtils;

/**
 * Куда же без хелпера :)
 */
public class HttpHelper {

    /**
     * Кодирование filename в заголовке Content-Disposition согласно RFC-5987 в кодировке UTF-8
     * @param s
     * @return
     * @throws UnsupportedEncodingException 
     */
    public static String rfc5987_encode(final String s) throws UnsupportedEncodingException {
        final byte[] s_bytes = s.getBytes("UTF-8");
        final int len = s_bytes.length;
        final StringBuilder sb = new StringBuilder(len << 1);
        final char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        final byte[] attr_char = {'!', '#', '$', '&', '+', '-', '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
            'W', 'X', 'Y', 'Z', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
            'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '|', '~'};
        for (int i = 0; i < len; ++i) {
            final byte b = s_bytes[i];
            if (Arrays.binarySearch(attr_char, b) >= 0) {
                sb.append((char) b);
            } else {
                sb.append('%');
                sb.append(digits[0x0f & (b >>> 4)]);
                sb.append(digits[b & 0x0f]);
            }
        }
        return sb.toString();
    }

    /**
     * Парсит название кодировки из заголовка Content-Type. Если не удалось - возвращает UTF-8.
     *
     * @param contentTypeHeader
     * @return
     */
    public static String parseContentEncoding(String contentTypeHeader) {
        if (contentTypeHeader == null) {
            return "UTF-8";
        }
        String[] values = contentTypeHeader.split(";");
        for (String value : values) {
            value = value.trim();
            if (value.toLowerCase().startsWith("charset=")) {
                return value.substring("charset=".length());
            }
        }
        // если ничего не нашли
        return "UTF-8";
    }

    /**
     * Парсит параметры в карту параметров
     *
     * @param exchange
     * @return
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public static HashMap<String, Object> parseParams(HttpExchange exchange) throws UnsupportedEncodingException,
            IOException {
        HashMap<String, Object> params = new HashMap();

        // может быть мультипарт, поэтому ищем теки с контентом и длиной
        String cType = exchange.getRequestHeaders().getFirst("Content-type");
        String cLength = exchange.getRequestHeaders().getFirst("Content-length");

        // парсим кодировку из заголовка Content-Type
        String encoding = parseContentEncoding(cType);

        // мультипарт обрабатывается по особенному
        if ((cType != null) && (cType.contains("multipart/form-data"))) {
            // нужен разделитель частей, boundary
            String boundary = cType.substring(cType.indexOf("boundary") + 9);
            MultipartStream multipartStream = new MultipartStream(exchange.getRequestBody(), boundary.getBytes());
            // устанавливаем кодировку для заголовков мультапарт согласно кодировке всего запроса
            multipartStream.setHeaderEncoding(encoding);
            boolean nextPart = multipartStream.skipPreamble();
            while (nextPart) {
                String header = multipartStream.readHeaders();

                // читаем имя параметра
                int i1 = header.indexOf(" name=");
                String pName = header.substring(i1 + 7);
                int i3 = pName.indexOf("\"");
                pName = pName.substring(0, i3);

                // пытаемся понять, файл это или нет
                int fn = header.indexOf("filename");
                if (fn != -1) {
                    // вытаскиваем имя и расширение
                    String fName = header.substring(fn + 10);
                    int i2 = fName.indexOf("\"");
                    fName = fName.substring(0, i2);
                    String fExt = FilenameUtils.getExtension(fName).toLowerCase();

                    // скачиваем файл во временный каталог (генерим уникальное имя, сохраняем расширение)
                    String uName = UploadStore.prepareFileName(fExt);
                    FileOutputStream fs = new FileOutputStream(UploadStore.getUploadPath() + "/" + uName);
                    try {
                        multipartStream.readBodyData(fs);
                    } finally {
                        fs.close();
                    }

                    // пишем в параметры имя параметра и имя файла
                    params.put(pName, new UploadedFile(uName, UploadStore.getUploadPath() + "/" + uName, fName));
                } else {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    multipartStream.readBodyData(baos);
                    String value = baos.toString(AtomHandler.CHARSET);

                    params.put(pName, value);
                }
                nextPart = multipartStream.readBoundary();
            }

            return params;
        }

        // не мультипарт: application/x-www-form-urlencoded | text/plain
        if ("post".equalsIgnoreCase(exchange.getRequestMethod())) {
            InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), CHARSET);
            BufferedReader br = new BufferedReader(isr);
            String query = br.readLine();
            parseQuery(query, params);
            return params;
        } else if ("get".equalsIgnoreCase(exchange.getRequestMethod())) {
            String query = exchange.getRequestURI().getRawQuery();
            parseQuery(query, params);
            return params;
        } else {
            throw new IllegalArgumentException("Unsupported HTTP method: " + exchange.getRequestMethod());
        }
    }

    /**
     * Парсит параметры HTTP запроса в карту параметров
     *
     * @param query
     * @param parameters
     * @throws UnsupportedEncodingException
     */
    public static void parseQuery(String query, Map<String, Object> parameters)
            throws UnsupportedEncodingException {
        if (query != null) {
            String pairs[] = query.split("[&]");

            for (String pair : pairs) {
                String param[] = pair.split("[=]");

                String key = null;
                String value = null;
                if (param.length > 0) {
                    key = UrlHelper.Decode(param[0], CHARSET);
                }

                if (param.length > 1) {
                    value = UrlHelper.Decode(param[1], CHARSET);
                }

                if (parameters.containsKey(key)) {
                    Object obj = parameters.get(key);
                    if (obj instanceof List<?>) {
                        List<String> values = (List<String>) obj;
                        values.add(value);
                    } else if (obj instanceof String) {
                        List<String> values = new ArrayList<String>();
                        values.add((String) obj);
                        values.add(value);
                        parameters.put(key, values);
                    }
                } else {
                    parameters.put(key, value);
                }
            }
        }
    }

    /**
     * Сжатие бинарного массива методом GZIP DEFLATE
     *
     * @param array
     * @return
     * @throws IOException
     */
    public static byte[] packArray(byte[] array) throws IOException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        GZIPOutputStream gzipStream = new GZIPOutputStream(byteStream);
        gzipStream.write(array);
        gzipStream.flush();
        gzipStream.close();
        return byteStream.toByteArray();
    }

    /**
     * Выполняем запрос получаем насос
     *
     * @param url полный URL запроса вместе с параметрами
     * @param basicAuthUser пользователь basic-авторизации (если необходим)
     * @param basicAuthPassword пароль basic-авторизации (если необходим)
     * @param connectTimeout
     * @param readTimeout
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
    public static WebRequestData requestGet(String url, String basicAuthUser, String basicAuthPassword,
            int connectTimeout, int readTimeout) throws MalformedURLException, IOException {
        WebRequestData data = new WebRequestData();
        long timestart = System.currentTimeMillis();

        URL u = new URL(url);
        HttpURLConnection c = (HttpURLConnection) u.openConnection();
        try {
            c.setRequestMethod("GET");
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(connectTimeout);
            c.setReadTimeout(readTimeout);

            // basic авторизация
            if ((basicAuthUser != null) && (basicAuthPassword != null)) {
                String authString = basicAuthUser + ":" + basicAuthPassword;
                String authStringEnc = Base64.encodeBase64String(authString.getBytes());
                c.setRequestProperty("Authorization", "Basic " + authStringEnc);
            }

            c.connect();
            int status = c.getResponseCode();
            data.setHttpCode(status);
            if (status == HttpURLConnection.HTTP_OK) {
                StringBuilder sb;
                try (BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), "UTF-8"))) {
                    sb = new StringBuilder();
                    boolean first = true;
                    String line;
                    while ((line = br.readLine()) != null) {
                        if (!first) {
                            sb.append("\n");
                        } else {
                            first = false;
                        }
                        sb.append(line);

                    }
                }
                data.setResponceMessage(sb.toString());
            } else {
                StringBuilder sb;
                try (BufferedReader br = new BufferedReader(new InputStreamReader(c.getErrorStream(), "UTF-8"))) {
                    sb = new StringBuilder();
                    boolean first = true;
                    String line;
                    while ((line = br.readLine()) != null) {
                        if (!first) {
                            sb.append("\n");
                        } else {
                            first = false;
                        }
                        sb.append(line);

                    }
                    data.setResponceMessage(sb.toString());
                }
            }
            // время выполнения только после полного получения данных
            data.setExecutionTimeMs(System.currentTimeMillis() - timestart);
        } finally {
            c.disconnect();
        }
        return data;
    }

}
