/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.common;

import java.util.HashMap;

/**
 * Параметры HTTP редиректа
 *
 * @author Andrey Nikolaev <vajadhava@gmail.com>
 */
public class HttpRedirect {

    // HTTP код
    private final int code = 301;

    private String url;
    private HashMap<String, String> headers = new HashMap<>();

    /**
     * Создает описание редиректа на заданный URL. HTTP заголовки можно добавить через addHeader
     *
     * @param url URL назначения
     */
    public HttpRedirect(String url) {
        this.url = url;
    }

    /**
     * HTTP code
     *
     * @return
     */
    public int getCode() {
        return code;
    }

    /**
     * URL назначения
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     * Заголовки
     *
     * @return
     */
    public HashMap<String, String> getHeaders() {
        return headers;
    }

    /**
     * Добавить HTTP заголовок
     *
     * @param name ключ
     * @param value значение
     * @return возвращает список заголовков
     */
    public HashMap<String, String> addHeader(String name, String value) {
        headers.put(url, value);
        return headers;
    }

}
