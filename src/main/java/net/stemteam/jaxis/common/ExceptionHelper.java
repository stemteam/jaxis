package net.stemteam.jaxis.common;

public class ExceptionHelper {

    private static final String MESSAGE_PREFIX = "-=#";
    private static final String MESSAGE_SUFFIX = "#=-";

    /**
     * Это для того чтобы в клиентах вычленить текс сообщения из всего текста
     * ошибки
     *
     * @param message
     * @return
     */
    public static String prepareMessage(String message) {
        return new StringBuilder().append(MESSAGE_PREFIX).append(message).append(MESSAGE_SUFFIX).toString();
    }

    /**
     * Парсит весь текст ошибки, выковыривает текст сообщения, если таковой
     * имеется
     *
     * @param callStack полный текст ошибки со стеком вызова
     * @return
     */
    public static String getHumanMessage(String callStack) {
        String s = null;
        if (callStack == null) {
            return null;
        }
        int i1 = callStack.indexOf(MESSAGE_PREFIX);
        int i2 = callStack.indexOf(MESSAGE_SUFFIX);
        if ((i1 != -1) && (i2 != -1) && (i1 < i2)) {
            s = callStack.substring(i1 + MESSAGE_PREFIX.length(), i2);
        }
        return s;
    }
    
    /**
     * Возвращает строковое представление исключения + стэк вызовов
     *
     * @param ex Исключение
     * @return Строка
     */
    public static String getErrorString(Exception ex) {
        StringBuilder sb = new StringBuilder();
        sb.append(ex.toString());
        sb.append("\n");
        for (StackTraceElement el : ex.getStackTrace()) {
            sb.append(el.toString());
            sb.append("\n");
        }
        return sb.toString();
    }
    
    /**
     * Возвращает текущий стэк
     * @return 
     */
    public static String getCurrentStack(int scipCount) {
        // стэк
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (StackTraceElement el : Thread.currentThread().getStackTrace()) {
            i++;
            if (i <= scipCount) {
                continue;
            }
            sb.append(el.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

}
