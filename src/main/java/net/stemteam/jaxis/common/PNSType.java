package net.stemteam.jaxis.common;

/**
 * Created by warmouse on 16.06.15.
 * Тип службы уведомлений платформы Platform Notification Service Type
 */
public enum PNSType {
    GCM("gcm", "Android PNS"),
    APNS("apns", "Apple PNS"),
    MPNS("mpns", "Windows phone 8"),
    WNS("wns", "Windows store");

    private final String code;
    private final String name;

    PNSType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    /**
     * Доступ к элементу по коду
     * @param code код (sms, email, push)
     * @return
     */
    public static PNSType getByCode(String code) {
        for (PNSType v : values()) {
            if (v.getCode().equals(code)) {
                return v;
            }
        }
        throw new IllegalArgumentException(String.format("Code '%s' not exists", code));
    }
}
