/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.common;

import net.stemteam.datatransport.exception.ColumnNotFoundException;
import net.stemteam.datatransport.transport.DataColumn;
import net.stemteam.datatransport.transport.DataColumnType;
import net.stemteam.datatransport.transport.DataRecord;
import net.stemteam.datatransport.transport.DataSet;

/**
 * Помогалка в формировании html
 *
 * @author Andrey Nikolaev <vajadhava@mail.ru>
 */
public class HtmlHelper {

    public static String getPageHeader(String title) {
        StringBuilder sb = new StringBuilder();
        // заголовок
        sb.append("<!DOCTYPE html>");
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<title>jaxis</title>");
        sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />");
        sb.append("<meta name=\"norobots\" content=\"noindex\" />");
        sb.append("<link href=\"http://idea.navstat.ru/assets/css/bootstrap.min.css\" rel=\"stylesheet\">");

        sb.append("<link href=\"http://idea.navstat.ru/assets/css/bootstrap-responsive.min.css\" rel=\"stylesheet\">");

//        sb.append("<link media=\"all\" type=\"text/css\" href=\"http://code.jquery.com/ui/1.8.19/themes/base/jquery-ui.css\" rel=\"stylesheet\">\n");
//        sb.append("<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"></script>\n");
//        sb.append("<script type=\"text/javascript\" src=\"http://code.jquery.com/ui/1.8.19/jquery-ui.min.js\"></script>\n");
//        sb.append("<script type=\"text/javascript\" src=\"http://jquery-ui.googlecode.com/svn/tags/latest/ui/minified/i18n/jquery-ui-i18n.min.js\"></script>\n");
        // google chart api
//        sb.append("<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>");
        // стили для выравнивания в таблице по правому краю
        sb.append("<style>th.right, td.right {text-align:right;}</style>");
        sb.append("<style>td.nowrap {white-space:nowrap}</style>");
        sb.append("</head>");
        sb.append("<body>");
        sb.append("<div class=\"container-fluid\">");

        sb.append("<div class=\"page-header\"><h1>");
        sb.append(title);
        sb.append("</div>\n\n");

        return sb.toString();
    }

    public static String getPageFooter() {
        StringBuilder sb = new StringBuilder();
        sb.append("</div>");

//        sb.append("<script>\n");
//        sb.append("$(document).ready(function(){\n");        
//        sb.append("$.datepicker.setDefaults( $.datepicker.regional[ \"ru\" ] );");
//        sb.append("$('#datepicker').datepicker({dateFormat: 'yy.mm.dd'});\n");
//        sb.append("});\n");
//        sb.append("</script>\n");
        sb.append("</body>");
        sb.append("</html>");

        return sb.toString();
    }

    /**
     * Формирует html-таблицу из датасета
     *
     * @param ds DataSet
     * @return
     * @throws ColumnNotFoundException
     */
    public static String getDataSetAsHtmlTable(DataSet ds) throws ColumnNotFoundException {
        if (ds == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<table class=\"table table-striped table-bordered table-condensed\">");

        // заголовок
        sb.append("<thead>");

        // название колонок
        sb.append("<tr>");
        for (DataColumn col : ds.getColumns()) {
            // определяем выравнивание
            if (DataColumnType.STRING.equals(DataColumnType.getByName(col.getDataTypeName()))
                    || DataColumnType.DATASET.equals(DataColumnType.getByName(col.getDataTypeName()))
                    || DataColumnType.GEOMETRY.equals(DataColumnType.getByName(col.getDataTypeName()))) {
                sb.append("<th>");
            } else {
                sb.append("<th class=\"right\">");
            }
            sb.append(col.getName());
            sb.append("</th>");
        }
        sb.append("</tr>");

        // типы данных
        sb.append("<tr>");
        for (DataColumn col : ds.getColumns()) {
            // определяем выравнивание
            if (DataColumnType.STRING.equals(DataColumnType.getByName(col.getDataTypeName()))
                    || DataColumnType.DATASET.equals(DataColumnType.getByName(col.getDataTypeName()))
                    || DataColumnType.GEOMETRY.equals(DataColumnType.getByName(col.getDataTypeName()))) {
                sb.append("<th>");
            } else {
                sb.append("<th class=\"right\">");
            }

            sb.append(col.getDataTypeName());
            sb.append("</th>");
        }
        sb.append("</tr>");
        sb.append("</thead>");

        // содержимое
        sb.append("<tbody>");
        int colCount = ds.getColumns().size();

        for (DataRecord row : ds.getRecords()) {
            sb.append("<tr>");
            for (int i = 0; i < colCount; i++) {
                DataColumn col = row.getCell(i).getColumn();
                // определяем выравнивание
                if (DataColumnType.STRING.equals(DataColumnType.getByName(col.getDataTypeName()))
                    || DataColumnType.DATASET.equals(DataColumnType.getByName(col.getDataTypeName()))
                    || DataColumnType.GEOMETRY.equals(DataColumnType.getByName(col.getDataTypeName()))) {
                    sb.append("<td class=\"nowrap\">");
                } else {
                    sb.append("<td class=\"right\">");
                }

                if (DataColumnType.DATASET.equals(DataColumnType.getByName(col.getDataTypeName()))) {
                    sb.append(getDataSetAsHtmlTable(row.getCell(i).getDataSet()));
                } else {
                    sb.append(row.getCell(i).getJsonValue());
                }
                sb.append("</td>");
            }
            sb.append("</tr>");
        }

        sb.append("</tbody>");
        sb.append("</table>");

        return sb.toString();
    }
}
