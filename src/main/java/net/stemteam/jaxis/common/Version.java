/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.common;

import java.util.Objects;

/**
 * Лютый хэлпер по версиям
 *
 * @author vajadhava@gmail.com
 */
public class Version implements Comparable<Version> {

    private final String version;

    /**
     * Возвращает строковое представление
     *
     * @return
     */
    public final String get() {
        return this.version;
    }

    public Version(String version) {
        check(version);
        this.version = version;
    }

    /**
     * Проверка на соответствие формату версии
     *
     * @param version
     * @throws IllegalArgumentException при несоответствии формату
     */
    public static void check(String version) {
        if (version == null) {
            throw new IllegalArgumentException("Version can not be null");
        }
        if (!version.matches("[0-9]+(\\.[0-9]+)*")) {
            throw new IllegalArgumentException("Invalid version format");
        }
    }

    /**
     * Сравнение с версией. Результаты: -1 текущая версия меньше переданной, 0 = равны, 1 = больше указанной
     *
     * @param target
     * @return
     */
    @Override
    public int compareTo(Version target) {
        if (target == null) {
            return 1;
        }
        String[] thisParts = this.get().split("\\.");
        String[] thatParts = target.get().split("\\.");
        int length = Math.max(thisParts.length, thatParts.length);
        for (int i = 0; i < length; i++) {
            int thisPart = i < thisParts.length ? Integer.parseInt(thisParts[i]) : 0;
            int thatPart = i < thatParts.length ? Integer.parseInt(thatParts[i]) : 0;
            if (thisPart < thatPart) {
                return -1;
            }
            if (thisPart > thatPart) {
                return 1;
            }
        }
        return 0;
    }

    /**
     * Проверка на равенство
     *
     * @param version
     * @return
     */
    @Override
    public boolean equals(Object version) {
        if (this == version) {
            return true;
        }
        if (version == null) {
            return false;
        }
        if (this.getClass() != version.getClass()) {
            return false;
        }
        return this.compareTo((Version) version) == 0;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.version);
        return hash;
    }

    @Override
    public String toString() {
        return get();
    }
}
