/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.common;

import net.stemteam.datatransport.transport.DataSet;

/**
 * Данные выполнения вэб-запроса
 */
public class WebRequestData {

    private int httpCode;
    private String responceMessage;
    private long executionTimeMs;

    /**
     * HTTP_CODE
     *
     * @return
     */
    public int getHttpCode() {
        return httpCode;
    }

    /**
     * HTTP_CODE
     *
     * @param httpCode
     */
    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }

    /**
     * Данные ответа вэб-сервера
     *
     * @return
     */
    public String getResponceMessage() {
        return responceMessage;
    }

    /**
     * Данные ответа вэб-сервера
     *
     * @param responceMessage
     */
    public void setResponceMessage(String responceMessage) {
        this.responceMessage = responceMessage;
    }

    /**
     * Время выполнения запроса (милисекунд)
     *
     * @return
     */
    public long getExecutionTimeMs() {
        return executionTimeMs;
    }

    /**
     * Время выполнения запроса (милисекунд)
     *
     * @param executionTimeMs
     */
    public void setExecutionTimeMs(long executionTimeMs) {
        this.executionTimeMs = executionTimeMs;
    }

    /**
     * Распаковка датасета из ответа
     *
     * @return
     * @throws Exception
     */
    public DataSet getDataSet() throws Exception {
        if (responceMessage == null) {
            return null;
        }
        DataSet ds = new DataSet();
        ds.UnPackData(responceMessage);
        return ds;
    }
}
