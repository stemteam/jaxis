/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.stemteam.jaxis.common;

import net.stemteam.datatransport.tool.Md5;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;

/**
 * Класс работы с смс через сервис SMS Aero (http://smsaero.ru/)
 */
public class SmsAero {

    private static String username;
    private static String password;
    private static String signature;

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        SmsAero.password = password;
    }

    public static String getSignature() {
        return signature;
    }

    public static void setSignature(String signature) {
        SmsAero.signature = signature;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        SmsAero.username = username;
    }

    /**
     * Возвращает баланс учетной записи на сервисе
     * @return 
     */
    public static double getBalanse() throws UnsupportedEncodingException, NoSuchAlgorithmException, MalformedURLException, IOException {
        // формируем запрос
        StringBuilder sb = new StringBuilder();
        sb.append("http://gate.smsaero.ru/balance/?answer=json&user=");
        sb.append(UrlHelper.Encode(username));
        sb.append("&password=");
        sb.append(Md5.md5ToString(Md5.getMd5(password.getBytes())));

        URL obj = new URL(sb.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();


        // читаем response
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String response = "";
        String s;
        while ((s = in.readLine()) != null) {
            response += s + "\n";
        }
        con.disconnect();

        // парсим
        response = response.replace("balance", "");
        response = response.replace(":", "");
        response = response.replace("{", "");
        response = response.replace("}", "");
        response = response.replace(" ", "");
        response = response.replace("\"", "");
        response = response.replace("\n", "");
        response = response.replace("\r", "");

        return Double.valueOf(response);
    }

    /**
     * Отправляет SMS (телефон в формате 71234567890)
     * @param phone 
     * @param message
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     * @throws MalformedURLException
     * @throws IOException 
     */
    public static void sendSms(String phone, String message) throws IOException, Exception {
        // формируем запрос
        StringBuilder sb = new StringBuilder();
        sb.append("http://gate.smsaero.ru/send/?to=");
        sb.append(UrlHelper.Encode(phone));
        sb.append("&text=");
        sb.append(UrlHelper.Encode(message));
        sb.append("&user=");
        sb.append(UrlHelper.Encode(username));
        sb.append("&password=");
        sb.append(Md5.md5ToString(Md5.getMd5(password.getBytes())));
        sb.append("&from=");
        sb.append(UrlHelper.Encode(signature));

        URL obj = new URL(sb.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();

        // читаем response
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String response = "";
        String s;
        while ((s = in.readLine()) != null) {
            response += s + "\n";
        }
        con.disconnect();

        if ((response != null) && (response.indexOf("accepted") != -1)) {
            return;
        } else {
            throw new Exception("Не удалось отправить SMS адресату " + phone + ". Причина: " + response);
        }

    }
}
