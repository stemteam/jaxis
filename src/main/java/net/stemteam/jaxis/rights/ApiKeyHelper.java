/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.stemteam.jaxis.rights;

import net.stemteam.jaxis.err.HttpForbiddenException;
import net.stemteam.jaxis.err.NotConfiguredException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * Api Key Helper.
 * 
 * Синглтон для дальнейшей оптимизации по скорости, когда будут не запросы к 
 * базе, а карта ключей (TODO)
 */
public class ApiKeyHelper {
    
    private static final String ALGORITHM_SHA = "SHA";
    private static final String ALGORITHM_SHA256 = "SHA-256";
    private static final String ALGORITHM_SHA512 = "SHA-512";
    
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    /**
     * Преобразовывает бинарный массив в Hex строку
     * @param bytes
     * @return 
     */
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    
    /**
     * Генерирует новый API-KEY, алгоритм SHA-256
     * @return
     * @throws NoSuchAlgorithmException 
     */
    public String generate() throws NoSuchAlgorithmException {
        // генерируем случайную последовательность
        SecureRandom sr = new SecureRandom();
        byte[] pseudoRandom = new byte[100];
        sr.nextBytes(pseudoRandom);
        // считаем SHA-256
        
        MessageDigest md = MessageDigest.getInstance(ALGORITHM_SHA256);
        String s = bytesToHex(md.digest(pseudoRandom));
        
        return s;
    }
    
    /**
     * Проверяет корректность API-ключа
     * @param ds базка, где проверять
     * @param apiKey
     * @throws SQLException 
     * @throws net.stemteam.jaxis.err.HttpForbiddenException 
     * @throws net.stemteam.jaxis.err.NotConfiguredException 
     */
    public void check(DataSource ds, String apiKey) throws SQLException, HttpForbiddenException, NotConfiguredException {
        try (Connection conn = ds.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(" select id_STEM_RIGHT_apikey from STEM_RIGHT_apikey where strdata = ? ");
            ps.setString(1, apiKey);
            try (ResultSet rs = ps.executeQuery()) {
                if (!rs.next()) {
                    throw new HttpForbiddenException("API KEY not found in database");
                }
            }
        }
    }
    
    public static ApiKeyHelper getInstance() {
        return ApiKeyHolder.INSTANCE;
    }
    
    private static class ApiKeyHolder {
        private static final ApiKeyHelper INSTANCE = new ApiKeyHelper();
    }
    
    public static void main(String[] args) throws NoSuchAlgorithmException {
        ApiKeyHelper apikey = new ApiKeyHelper();
        System.out.println(apikey.generate());
    }
    
}
