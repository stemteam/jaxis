Демонизация сервиса

Будем публиковать из-под пользователя stem.

Создаем каталог - расположение (в дальнейшем [WORKING_PATH]), устанавливаем права на каталог - доступ для пользователя stem.

3. Переносим в [WORKING_PATH] следующие файлы:
- содержимое папки сборки (dist)
- start.sh - управляющий файл (для лончера устанавливаем права на выполнение)

4. В /etc/init.d переносим файл jaxis (демонизатор), устанавливаем права на выполнение.

5. В демонизаторе указываем расположение jaxis [WORKING_PATH] в переменной "$WORKING_PATH"

6. Устанавливаем авто-запуск демона при старте системы (и выгрузка при шатдауне)

	# ubuntu 10.04
	update-rc.d jaxis defaults  

	# SLES11
	chkconfig -a --level 2345 jaxis

	# CentOS
	chkconfig jaxis on

7. Удаление из автозапуска (опц.)

	# ubuntu 10.04
	update-rc.d -f jaxis remove 

	# SLES11
	chkconfig jaxis off
	chkconfig -d jaxis


--------------------------------------------
Статус-задание, это задание, проверяющее запущенность сервиса, и запускающее его если он не запущен (он может развалиться в процессе работы).
Состоит из файлов:
	status.sh
	crontab.ct

