#!/bin/bash

# таймату выполнения запроса по HTTP, секунд
JAXIS_HTTP_TIMEOUT=60
# максимальный аптайм, дней
JAXIS_MAX_UPTIME=7
# максимальное количество дескрипторов
JAXIS_MAX_FD=10000

# проверяем допустимость ОС
if [ ! -f /etc/redhat-release ]; then
  echo "Error. Unsupported OS release."
  exit 1
fi

# check if not run - start it
isnr=$(service jaxis status | grep -c 'not running')
if [[ $isnr -eq 1 ]]; then
  date
  service jaxis start
  exit 0
fi

# проверяем аптайм демона, если велик - выполняем перезапуск
jaxispid=$(pgrep jaxis)
t=$(LC_ALL=POSIX ps -o etime= $jaxispid)
uptimeday=0
case $t in *-*) uptimeday=${t%%-*}; t=${t#*-};; esac
if [[ $uptimeday -gt $JAXIS_MAX_UPTIME ]]; then 
  # перезапуск по аптайму только в темное время суток, поэтому проверяем что на улице темно.
  hh=$(date +"%H")
  if [[ $hh -lt 6 ]]; then
    echo "Uptime: $uptimeday. Will be restarted."
    service jaxis restart
    exit 0
  fi
fi


